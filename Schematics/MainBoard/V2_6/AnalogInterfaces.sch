EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 3
Title "EuroPress - Power supplies"
Date "2019-01-06"
Rev "2.2"
Comp "LGR"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	16000 5400 500  5400
Wire Notes Line
	6700 5400 6700 11200
Text Notes 6750 5550 0    60   ~ 0
POWER SUPPLIES
Wire Notes Line
	6700 5600 7600 5600
Wire Notes Line
	7600 5600 7600 5400
$Comp
L Regulator_Switching:TSR_1-24120 U3
U 1 1 5C50B29A
P 8100 8300
AR Path="/5CC2A4AE/5C50B29A" Ref="U3"  Part="1" 
AR Path="/614E87D3/5C50B29A" Ref="U?"  Part="1" 
F 0 "U3" H 8100 8667 50  0000 C CNN
F 1 "TSR_1-2490" H 8100 8576 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 8100 8150 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 8100 8300 50  0001 C CNN
F 4 "DC-DC switching regulator 15-36Vin 9V 1A" H -400 0   50  0001 C CNN "Description"
F 5 "Traco Power" H -400 0   50  0001 C CNN "Manufacturer_Name"
F 6 "TSR 1-2490" H -400 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "495-TSR-1-2490" H -400 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -400 0   50  0001 C CNN "Supplier"
F 9 "495-TSR-1-2490" H -400 0   50  0001 C CNN "SupplierRef"
F 10 "--" H 8100 8300 50  0001 C CNN "LCSC"
	1    8100 8300
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR035
U 1 1 5C50B6C9
P 7500 8100
AR Path="/5CC2A4AE/5C50B6C9" Ref="#PWR035"  Part="1" 
AR Path="/614E87D3/5C50B6C9" Ref="#PWR?"  Part="1" 
F 0 "#PWR035" H 7500 7950 50  0001 C CNN
F 1 "+24V" H 7515 8273 50  0000 C CNN
F 2 "" H 7500 8100 50  0001 C CNN
F 3 "" H 7500 8100 50  0001 C CNN
	1    7500 8100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 5C50B80A
P 8100 8600
AR Path="/5CC2A4AE/5C50B80A" Ref="#PWR036"  Part="1" 
AR Path="/614E87D3/5C50B80A" Ref="#PWR?"  Part="1" 
F 0 "#PWR036" H 8100 8350 50  0001 C CNN
F 1 "GND" H 8105 8427 50  0000 C CNN
F 2 "" H 8100 8600 50  0001 C CNN
F 3 "" H 8100 8600 50  0001 C CNN
	1    8100 8600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 8200 7500 8200
Wire Wire Line
	7500 8200 7500 8100
Wire Wire Line
	8100 8500 8100 8600
Wire Wire Line
	8500 8200 8700 8200
Wire Wire Line
	8700 8200 8700 8100
Text Notes 6950 10750 0    50   ~ 0
±12V/±83mA isolated switching regulator (ADC/DAC/OpAmps supply)
Text Notes 7300 8950 0    50   ~ 0
9V/1A switching regulator (Arduino Due supply)
Text Notes 11650 6850 0    50   ~ 0
3V3/3A linear regulator
Wire Notes Line
	12600 500  12600 5400
Text Notes 12650 650  0    60   ~ 0
CONNECTORS
Wire Notes Line
	12600 700  13350 700 
Wire Notes Line
	13350 700  13350 500 
$Comp
L power:+12VA #PWR041
U 1 1 5F70136B
P 9100 9550
AR Path="/5CC2A4AE/5F70136B" Ref="#PWR041"  Part="1" 
AR Path="/614E87D3/5F70136B" Ref="#PWR?"  Part="1" 
F 0 "#PWR041" H 9100 9400 50  0001 C CNN
F 1 "+12VA" H 9115 9723 50  0000 C CNN
F 2 "" H 9100 9550 50  0001 C CNN
F 3 "" H 9100 9550 50  0001 C CNN
	1    9100 9550
	1    0    0    -1  
$EndComp
$Comp
L EuroPress:TEC_2-2422 U4
U 1 1 5F722FD2
P 8400 9900
AR Path="/5CC2A4AE/5F722FD2" Ref="U4"  Part="1" 
AR Path="/614E87D3/5F722FD2" Ref="U?"  Part="1" 
F 0 "U4" H 8400 10367 50  0000 C CNN
F 1 "TEC_2-2422" H 8400 10276 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TMR-xxxx_THT" H 8400 9550 50  0001 C CNN
F 3 "https://assets.tracopower.com/20190124103907/TEC2/documents/tec2-datasheet.pdf" H 8400 9400 50  0001 C CNN
F 4 "DC-DC isolated regulator 2W 18-36Vin +/-12V +/-83mA SMD" H 100 0   50  0001 C CNN "Description"
F 5 "Traco Power" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "TEC 2-2422" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "495-TEC2-2422" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 100 0   50  0001 C CNN "Supplier"
F 9 "495-TEC2-2422" H 100 0   50  0001 C CNN "SupplierRef"
F 10 "--" H 8400 9900 50  0001 C CNN "LCSC"
	1    8400 9900
	1    0    0    -1  
$EndComp
$Comp
L power:-12VA #PWR044
U 1 1 5F72405D
P 9450 10200
AR Path="/5CC2A4AE/5F72405D" Ref="#PWR044"  Part="1" 
AR Path="/614E87D3/5F72405D" Ref="#PWR?"  Part="1" 
F 0 "#PWR044" H 9450 10050 50  0001 C CNN
F 1 "-12VA" H 9465 10373 50  0000 C CNN
F 2 "" H 9450 10200 50  0001 C CNN
F 3 "" H 9450 10200 50  0001 C CNN
	1    9450 10200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5F7241B7
P 7200 10200
AR Path="/5CC2A4AE/5F7241B7" Ref="#PWR034"  Part="1" 
AR Path="/614E87D3/5F7241B7" Ref="#PWR?"  Part="1" 
F 0 "#PWR034" H 7200 9950 50  0001 C CNN
F 1 "GND" H 7205 10027 50  0000 C CNN
F 2 "" H 7200 10200 50  0001 C CNN
F 3 "" H 7200 10200 50  0001 C CNN
	1    7200 10200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 9700 9100 9700
Wire Wire Line
	9100 9700 9100 9600
Wire Wire Line
	9450 10100 9450 10200
$Comp
L power:+24V #PWR033
U 1 1 5F77FDAA
P 7200 9600
AR Path="/5CC2A4AE/5F77FDAA" Ref="#PWR033"  Part="1" 
AR Path="/614E87D3/5F77FDAA" Ref="#PWR?"  Part="1" 
F 0 "#PWR033" H 7200 9450 50  0001 C CNN
F 1 "+24V" H 7215 9773 50  0000 C CNN
F 2 "" H 7200 9600 50  0001 C CNN
F 3 "" H 7200 9600 50  0001 C CNN
	1    7200 9600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 9600 7200 9700
Wire Wire Line
	7200 9700 7600 9700
Wire Wire Line
	7600 10100 7200 10100
Wire Wire Line
	7200 10100 7200 10200
$Comp
L Device:CP_Small C7
U 1 1 5F7E11F1
P 7200 9900
AR Path="/5CC2A4AE/5F7E11F1" Ref="C7"  Part="1" 
AR Path="/614E87D3/5F7E11F1" Ref="C?"  Part="1" 
F 0 "C7" H 7288 9946 50  0000 L CNN
F 1 "220uF" H 7288 9855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_8x10" H 7200 9900 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 7200 9900 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 220uF 35V 7000h 105°C" H -300 0   50  0001 C CNN "Description"
F 5 "Lelon" H -300 0   50  0001 C CNN "Manufacturer_Name"
F 6 "VZH221M1ETR-0810" H -300 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H -300 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -300 0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V221MNL1GS" H -300 0   50  0001 C CNN "SupplierRef"
F 10 "C134818" H 7200 9900 50  0001 C CNN "LCSC"
F 11 "0;0;180" H 7200 9900 50  0001 C CNN "JLCPCB_CORRECTION"
	1    7200 9900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 9800 7200 9700
Connection ~ 7200 9700
Wire Wire Line
	7200 10000 7200 10100
Connection ~ 7200 10100
$Comp
L Device:C_Small C12
U 1 1 5F842168
P 9600 9750
AR Path="/5CC2A4AE/5F842168" Ref="C12"  Part="1" 
AR Path="/614E87D3/5F842168" Ref="C?"  Part="1" 
F 0 "C12" H 9688 9796 50  0000 L CNN
F 1 "22uF" H 9688 9705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9600 9750 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/212/KEM_C1002_X7R_SMD-1102033.pdf" H 9600 9750 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25volts 22uF X5R 15%" H 100 0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "CL21A226MAQNNNE" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 100 0   50  0001 C CNN "Supplier"
F 9 "80-C2220C226K3R" H 100 0   50  0001 C CNN "SupplierRef"
F 10 "C45783" H 9600 9750 50  0001 C CNN "LCSC"
	1    9600 9750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5F842224
P 9000 10250
AR Path="/5CC2A4AE/5F842224" Ref="C10"  Part="1" 
AR Path="/614E87D3/5F842224" Ref="C?"  Part="1" 
F 0 "C10" H 8912 10204 50  0000 R CNN
F 1 "22uF" H 8912 10295 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9000 10250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/212/KEM_C1002_X7R_SMD-1102033.pdf" H 9000 10250 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25volts 22uF X5R 15%" H 100 0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "CL21A226MAQNNNE" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 100 0   50  0001 C CNN "Supplier"
F 9 "80-C2220C226K3R" H 100 0   50  0001 C CNN "SupplierRef"
F 10 "C45783" H 9000 10250 50  0001 C CNN "LCSC"
	1    9000 10250
	-1   0    0    1   
$EndComp
Wire Wire Line
	8900 9900 9600 9900
Wire Wire Line
	9600 9900 9600 9950
Wire Wire Line
	9600 9850 9600 9900
Connection ~ 9600 9900
Wire Wire Line
	9600 9650 9600 9600
Wire Wire Line
	9600 9600 9100 9600
Connection ~ 9100 9600
Wire Wire Line
	9100 9600 9100 9550
Wire Wire Line
	9000 10350 9000 10400
Wire Wire Line
	9000 10150 9000 10100
Wire Wire Line
	9450 10100 9000 10100
Connection ~ 9000 10100
Wire Wire Line
	9000 10100 8900 10100
$Comp
L power:+3V3 #PWR060
U 1 1 5CF5C408
P 15100 8400
AR Path="/5CC2A4AE/5CF5C408" Ref="#PWR060"  Part="1" 
AR Path="/614E87D3/5CF5C408" Ref="#PWR?"  Part="1" 
F 0 "#PWR060" H 15100 8250 50  0001 C CNN
F 1 "+3V3" H 15115 8573 50  0000 C CNN
F 2 "" H 15100 8400 50  0001 C CNN
F 3 "" H 15100 8400 50  0001 C CNN
	1    15100 8400
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR046
U 1 1 5D65E046
P 11100 7100
AR Path="/5CC2A4AE/5D65E046" Ref="#PWR046"  Part="1" 
AR Path="/614E87D3/5D65E046" Ref="#PWR?"  Part="1" 
F 0 "#PWR046" H 11100 6950 50  0001 C CNN
F 1 "+24V" H 11115 7273 50  0000 C CNN
F 2 "" H 11100 7100 50  0001 C CNN
F 3 "" H 11100 7100 50  0001 C CNN
	1    11100 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 6550 7500 6550
Wire Wire Line
	7500 6450 7600 6450
Wire Wire Line
	7600 6450 7600 6100
Wire Wire Line
	8500 6100 8700 6100
Wire Wire Line
	7600 6550 7600 6950
$Comp
L Device:CP_Small C8
U 1 1 5C999785
P 8700 6250
AR Path="/5CC2A4AE/5C999785" Ref="C8"  Part="1" 
AR Path="/614E87D3/5C999785" Ref="C?"  Part="1" 
F 0 "C8" H 8788 6296 50  0000 L CNN
F 1 "330uF" H 8788 6205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 8700 6250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 8700 6250 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 330uF 35V 5000h 105°C" H -200 0   50  0001 C CNN "Description"
F 5 "Lelon" H -200 0   50  0001 C CNN "Manufacturer_Name"
F 6 "VZH331M1VTR-1010K" H -200 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H -200 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -200 0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V331MNL1GS" H -200 0   50  0001 C CNN "SupplierRef"
F 10 "C134522" H 8700 6250 50  0001 C CNN "LCSC"
F 11 "0;0;180" H 8700 6250 50  0001 C CNN "JLCPCB_CORRECTION"
	1    8700 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C9
U 1 1 5C99987B
P 8700 7100
AR Path="/5CC2A4AE/5C99987B" Ref="C9"  Part="1" 
AR Path="/614E87D3/5C99987B" Ref="C?"  Part="1" 
F 0 "C9" H 8788 7146 50  0000 L CNN
F 1 "330uF" H 8788 7055 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 8700 7100 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 8700 7100 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 330uF 35V 5000h 105°C" H -200 -50 50  0001 C CNN "Description"
F 5 "Lelon" H -200 -50 50  0001 C CNN "Manufacturer_Name"
F 6 "VZH331M1VTR-1010K" H -200 -50 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H -200 -50 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -200 -50 50  0001 C CNN "Supplier"
F 9 "647-UCW1V331MNL1GS" H -200 -50 50  0001 C CNN "SupplierRef"
F 10 "C134522" H 8700 7100 50  0001 C CNN "LCSC"
F 11 "0;0;180" H 8700 7100 50  0001 C CNN "JLCPCB_CORRECTION"
	1    8700 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C99996B
P 8700 6400
AR Path="/5C99996B" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5C99996B" Ref="#PWR037"  Part="1" 
AR Path="/614E87D3/5C99996B" Ref="#PWR?"  Part="1" 
F 0 "#PWR037" H 8700 6150 50  0001 C CNN
F 1 "GND" H 8705 6227 50  0000 C CNN
F 2 "" H 8700 6400 50  0001 C CNN
F 3 "" H 8700 6400 50  0001 C CNN
	1    8700 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C999A30
P 8700 7250
AR Path="/5C999A30" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5C999A30" Ref="#PWR038"  Part="1" 
AR Path="/614E87D3/5C999A30" Ref="#PWR?"  Part="1" 
F 0 "#PWR038" H 8700 7000 50  0001 C CNN
F 1 "GND" H 8705 7077 50  0000 C CNN
F 2 "" H 8700 7250 50  0001 C CNN
F 3 "" H 8700 7250 50  0001 C CNN
	1    8700 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 7000 8700 6950
Wire Wire Line
	8700 6950 9150 6950
Wire Wire Line
	8700 7200 8700 7250
Wire Wire Line
	8700 6150 8700 6100
Wire Wire Line
	8700 6350 8700 6400
$Comp
L power:+24V #PWR042
U 1 1 5CADE914
P 9200 6050
AR Path="/5CC2A4AE/5CADE914" Ref="#PWR042"  Part="1" 
AR Path="/614E87D3/5CADE914" Ref="#PWR?"  Part="1" 
F 0 "#PWR042" H 9200 5900 50  0001 C CNN
F 1 "+24V" H 9215 6223 50  0000 C CNN
F 2 "" H 9200 6050 50  0001 C CNN
F 3 "" H 9200 6050 50  0001 C CNN
	1    9200 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse_Small F1
U 1 1 5CB16DB0
P 7950 6100
AR Path="/5CC2A4AE/5CB16DB0" Ref="F1"  Part="1" 
AR Path="/614E87D3/5CB16DB0" Ref="F?"  Part="1" 
F 0 "F1" H 7950 6285 50  0000 C CNN
F 1 "Fuse_Small" H 7950 6194 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" H 7950 6100 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/358/typ_OGN-14523.pdf" H 7950 6100 50  0001 C CNN
F 4 "Fuse holder 5x20mm" H 0   50  50  0001 C CNN "Description"
F 5 "Schurter" H 0   50  50  0001 C CNN "Manufacturer_Name"
F 6 "0031.8201" H 0   50  50  0001 C CNN "Manufacturer_Part_Number"
F 7 "693-0031.8201" H 0   50  50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   50  50  0001 C CNN "Supplier"
F 9 "693-0031.8201" H 0   50  50  0001 C CNN "SupplierRef"
F 10 "--" H 7950 6100 50  0001 C CNN "LCSC"
	1    7950 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse_Small F2
U 1 1 5CB173E0
P 7950 6950
AR Path="/5CC2A4AE/5CB173E0" Ref="F2"  Part="1" 
AR Path="/614E87D3/5CB173E0" Ref="F?"  Part="1" 
F 0 "F2" H 7950 7135 50  0000 C CNN
F 1 "Fuse_Small" H 7950 7044 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" H 7950 6950 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/358/typ_OGN-14523.pdf" H 7950 6950 50  0001 C CNN
F 4 "Fuse holder 5x20mm" H 0   0   50  0001 C CNN "Description"
F 5 "Schurter" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "0031.8201" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "693-0031.8201" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "693-0031.8201" H 0   0   50  0001 C CNN "SupplierRef"
F 10 "--" H 7950 6950 50  0001 C CNN "LCSC"
	1    7950 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 6100 7850 6100
Wire Wire Line
	8050 6100 8300 6100
Wire Wire Line
	7600 6950 7850 6950
$Comp
L Device:CP_Small C11
U 1 1 5CE602CE
P 9200 6250
AR Path="/5CC2A4AE/5CE602CE" Ref="C11"  Part="1" 
AR Path="/614E87D3/5CE602CE" Ref="C?"  Part="1" 
F 0 "C11" H 9288 6296 50  0000 L CNN
F 1 "330uF" H 9288 6205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 9200 6250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 9200 6250 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 330uF 35V 5000h 105°C" H -200 0   50  0001 C CNN "Description"
F 5 "Lelon" H -200 0   50  0001 C CNN "Manufacturer_Name"
F 6 "VZH331M1VTR-1010K" H -200 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H -200 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -200 0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V331MNL1GS" H -200 0   50  0001 C CNN "SupplierRef"
F 10 "C134522" H 9200 6250 50  0001 C CNN "LCSC"
F 11 "0;0;180" H 9200 6250 50  0001 C CNN "JLCPCB_CORRECTION"
	1    9200 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CE602D4
P 9200 6400
AR Path="/5CE602D4" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5CE602D4" Ref="#PWR043"  Part="1" 
AR Path="/614E87D3/5CE602D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR043" H 9200 6150 50  0001 C CNN
F 1 "GND" H 9205 6227 50  0000 C CNN
F 2 "" H 9200 6400 50  0001 C CNN
F 3 "" H 9200 6400 50  0001 C CNN
	1    9200 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 6350 9200 6400
$Comp
L Device:L_Small L1
U 1 1 5CE982EA
P 8950 6100
AR Path="/5CC2A4AE/5CE982EA" Ref="L1"  Part="1" 
AR Path="/614E87D3/5CE982EA" Ref="L?"  Part="1" 
F 0 "L1" V 9135 6100 50  0000 C CNN
F 1 "10uH" V 9044 6100 50  0000 C CNN
F 2 "EuroPress:SWPA6040S" H 8950 6100 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/lcsc/1809291611_Sunlord-SWPA6040S100MT_C87982.pdf" H 8950 6100 50  0001 C CNN
F 4 "Shielded Power Inductor 10uH 20% SMD" H -200 0   50  0001 C CNN "Description"
F 5 "Sunlord" H -200 0   50  0001 C CNN "Manufacturer_Name"
F 6 "SWPA6040S100MT" H -200 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H -200 0   50  0001 C CNN "Mouser Part Number"
F 8 "" H -200 0   50  0001 C CNN "Supplier"
F 9 "" H -200 0   50  0001 C CNN "SupplierRef"
F 10 "C87982" H 8950 6100 50  0001 C CNN "LCSC"
	1    8950 6100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9200 6050 9200 6100
Wire Wire Line
	8700 6100 8850 6100
Connection ~ 8700 6100
Wire Wire Line
	9050 6100 9200 6100
Connection ~ 9200 6100
Wire Wire Line
	9200 6100 9200 6150
Text GLabel 9150 6950 2    50   Output ~ 0
24V_OUT
$Comp
L power:+9V #PWR039
U 1 1 5CBFABF7
P 8700 8100
AR Path="/5CC2A4AE/5CBFABF7" Ref="#PWR039"  Part="1" 
AR Path="/614E87D3/5CBFABF7" Ref="#PWR?"  Part="1" 
F 0 "#PWR039" H 8700 7950 50  0001 C CNN
F 1 "+9V" H 8715 8273 50  0000 C CNN
F 2 "" H 8700 8100 50  0001 C CNN
F 3 "" H 8700 8100 50  0001 C CNN
	1    8700 8100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 5F4CBCC1
P 15400 7500
AR Path="/5CC2A4AE/5F4CBCC1" Ref="J3"  Part="1" 
AR Path="/614E87D3/5F4CBCC1" Ref="J?"  Part="1" 
F 0 "J3" H 15427 7476 50  0000 L CNN
F 1 "3V3_Ext" H 15427 7385 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 15400 7500 50  0001 C CNN
F 3 "~" H 15400 7500 50  0001 C CNN
F 4 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Sample"
F 8 "N/A" H 0   0   50  0001 C CNN "Supplier"
F 9 "N/A" H 0   0   50  0001 C CNN "SupplierRef"
F 10 "--" H 15400 7500 50  0001 C CNN "LCSC"
	1    15400 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR058
U 1 1 5F4CCCB5
P 15100 7400
AR Path="/5CC2A4AE/5F4CCCB5" Ref="#PWR058"  Part="1" 
AR Path="/614E87D3/5F4CCCB5" Ref="#PWR?"  Part="1" 
F 0 "#PWR058" H 15100 7250 50  0001 C CNN
F 1 "+3V3" H 15115 7573 50  0000 C CNN
F 2 "" H 15100 7400 50  0001 C CNN
F 3 "" H 15100 7400 50  0001 C CNN
	1    15100 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	15100 7700 15100 7600
Wire Wire Line
	15100 7600 15200 7600
Wire Wire Line
	15200 7500 15100 7500
Wire Wire Line
	15100 7500 15100 7400
Text Notes 16850 11250 0    50   ~ 0
+12VA:\n        U9 - TS922AIDT\n        U10 - LT1490A\n        U11 - LT1490A\n        U12 - AD7610BSTZ\n    \n        * LT1490A :\n         - 50µA quiescent current per amplifier\n         - Can drive 20mA per output\n             Sortie 1 : EV proportionnelle\n                 Impedance : 1MOhms => I = 10µA\n             Sortie 2 : Générateur ultrason\n                 Impedance : 15.4kOhms => I = 0.65mA\n    \n        * TS922AIDT :\n         - Total supply current for 12V : ~~2.6mA (Fig.4 p. 7)\n\n        * AD7610BSTZ :\n         - Typical operating current : 1.4mA (VCC = 15V, with internal reference buffer)\n\n    Total : 50µA + 10µA + 0.65mA + 2.6mA + 1.4mA = ~~5mA\n\n-12VA:\n        U12 - AD7610BSTZ\n\n        * AD7610BSTZ\n         - Typical operating current : 0.7mA (VEE = -15V)
Text Notes 16650 9000 0    50   ~ 0
Power supplies sizing :
Text Notes 10500 9900 0    60   ~ 0
POWER STATUS
Wire Notes Line
	10450 9950 11250 9950
Wire Notes Line
	11250 9950 11250 9750
$Comp
L Device:R_Small R?
U 1 1 5E28130B
P 11300 10700
AR Path="/5E28130B" Ref="R?"  Part="1" 
AR Path="/5CC2A4AE/5E28130B" Ref="R9"  Part="1" 
AR Path="/614E87D3/5E28130B" Ref="R?"  Part="1" 
F 0 "R9" H 11359 10746 50  0000 L CNN
F 1 "470" H 11359 10655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 11300 10700 50  0001 C CNN
F 3 "~" H 11300 10700 50  0001 C CNN
F 4 "Resistor 470 1% 62.5mW 0402" H 6000 2750 50  0001 C CNN "Description"
F 5 "UNI-ROYAL(Uniroyal Elec)" H 6000 2750 50  0001 C CNN "Manufacturer_Name"
F 6 "0402WGF4700TCE" H 6000 2750 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "" H 6000 2750 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6000 2750 50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB331V" H 6000 2750 50  0001 C CNN "SupplierRef"
F 10 "C25117" H 11300 10700 50  0001 C CNN "LCSC"
	1    11300 10700
	1    0    0    -1  
$EndComp
Wire Wire Line
	11300 10550 11300 10600
$Comp
L power:GND #PWR?
U 1 1 5E281325
P 11300 10850
AR Path="/5E281325" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E281325" Ref="#PWR049"  Part="1" 
AR Path="/614E87D3/5E281325" Ref="#PWR?"  Part="1" 
F 0 "#PWR049" H 11300 10600 50  0001 C CNN
F 1 "GND" H 11305 10677 50  0000 C CNN
F 2 "" H 11300 10850 50  0001 C CNN
F 3 "" H 11300 10850 50  0001 C CNN
	1    11300 10850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5E28132B
P 11300 10300
AR Path="/5E28132B" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E28132B" Ref="#PWR048"  Part="1" 
AR Path="/614E87D3/5E28132B" Ref="#PWR?"  Part="1" 
F 0 "#PWR048" H 11300 10150 50  0001 C CNN
F 1 "+3V3" H 11315 10473 50  0000 C CNN
F 2 "" H 11300 10300 50  0001 C CNN
F 3 "" H 11300 10300 50  0001 C CNN
	1    11300 10300
	1    0    0    -1  
$EndComp
Wire Wire Line
	11300 10800 11300 10850
Wire Wire Line
	11300 10350 11300 10300
Wire Notes Line
	10450 11200 10450 9750
Wire Notes Line
	10470 9750 11820 9750
$Comp
L Device:LED_Small D?
U 1 1 5E281343
P 11300 10450
AR Path="/5E281343" Ref="D?"  Part="1" 
AR Path="/5CC2A4AE/5E281343" Ref="D4"  Part="1" 
AR Path="/614E87D3/5E281343" Ref="D?"  Part="1" 
F 0 "D4" V 11300 10382 50  0000 R CNN
F 1 "3V3_ON" V 11255 10382 50  0001 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 11300 10450 50  0001 C CNN
F 3 "http://optoelectronics.liteon.com/upload/download/DS22-2000-118/LTST-C171KGKT.pdf" V 11300 10450 50  0001 C CNN
F 4 "Everlight Elec" H 6400 2250 50  0001 C CNN "Manufacturer_Name"
F 5 "19-217/GHC-YR1S2/3T" H 6400 2250 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "" H 6400 2250 50  0001 C CNN "Mouser Part Number"
F 7 "Mouser" H 6400 2250 50  0001 C CNN "Supplier"
F 8 "859-LTST-C171KGKT" H 6400 2250 50  0001 C CNN "SupplierRef"
F 9 "Green 520~535nm - 0603 LED" H 6400 2250 50  0001 C CNN "Description"
F 10 "C72043" H 11300 10450 50  0001 C CNN "LCSC"
	1    11300 10450
	0    -1   -1   0   
$EndComp
Wire Notes Line
	11810 9950 11810 9740
Text GLabel 15300 1800 0    50   Input ~ 0
SPI_SCK
Text GLabel 15300 1700 0    50   Input ~ 0
SPI_MOSI
Text GLabel 15300 1600 0    50   Input ~ 0
SPI_MISO
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5F65E2BD
P 15600 2650
AR Path="/5F65E2BD" Ref="J?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2BD" Ref="J5"  Part="1" 
AR Path="/614E87D3/5F65E2BD" Ref="J?"  Part="1" 
F 0 "J5" H 15680 2646 50  0000 L CNN
F 1 "Conn_01x03" H 15680 2601 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 15600 2650 50  0001 C CNN
F 3 "~" H 15600 2650 50  0001 C CNN
F 4 "N/A" H 400 -2050 50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 400 -2050 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 400 -2050 50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 400 -2050 50  0001 C CNN "Sample"
F 8 "N/A" H 400 -2050 50  0001 C CNN "Supplier"
F 9 "N/A" H 400 -2050 50  0001 C CNN "SupplierRef"
F 10 "--" H 15600 2650 50  0001 C CNN "LCSC"
	1    15600 2650
	1    0    0    1   
$EndComp
Wire Wire Line
	15300 2550 15400 2550
Wire Wire Line
	15300 2650 15400 2650
$Comp
L power:GND #PWR?
U 1 1 5F65E2C5
P 15300 2800
AR Path="/5F65E2C5" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2C5" Ref="#PWR062"  Part="1" 
AR Path="/614E87D3/5F65E2C5" Ref="#PWR?"  Part="1" 
F 0 "#PWR062" H 15300 2550 50  0001 C CNN
F 1 "GND" H 15305 2627 50  0000 C CNN
F 2 "" H 15300 2800 50  0001 C CNN
F 3 "" H 15300 2800 50  0001 C CNN
	1    15300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	15400 2750 15300 2750
Wire Wire Line
	15300 2750 15300 2800
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F65E2CF
P 15600 1700
AR Path="/5F65E2CF" Ref="J?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2CF" Ref="J4"  Part="1" 
AR Path="/614E87D3/5F65E2CF" Ref="J?"  Part="1" 
F 0 "J4" H 15680 1692 50  0000 L CNN
F 1 "SPI" H 15680 1601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 15600 1700 50  0001 C CNN
F 3 "~" H 15600 1700 50  0001 C CNN
F 4 "N/A" H 1850 -2950 50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 1850 -2950 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 1850 -2950 50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 1850 -2950 50  0001 C CNN "Sample"
F 8 "N/A" H 1850 -2950 50  0001 C CNN "Supplier"
F 9 "N/A" H 1850 -2950 50  0001 C CNN "SupplierRef"
F 10 "--" H 15600 1700 50  0001 C CNN "LCSC"
	1    15600 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F65E2D5
P 15300 1950
AR Path="/5F65E2D5" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2D5" Ref="#PWR061"  Part="1" 
AR Path="/614E87D3/5F65E2D5" Ref="#PWR?"  Part="1" 
F 0 "#PWR061" H 15300 1700 50  0001 C CNN
F 1 "GND" H 15305 1777 50  0000 C CNN
F 2 "" H 15300 1950 50  0001 C CNN
F 3 "" H 15300 1950 50  0001 C CNN
	1    15300 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	15300 1600 15400 1600
Wire Wire Line
	15400 1700 15300 1700
Wire Wire Line
	15300 1800 15400 1800
Wire Wire Line
	15400 1900 15300 1900
Wire Wire Line
	15300 1900 15300 1950
Text GLabel 15300 2550 0    50   Input ~ 0
CYCLE_TIME_MON1
Text GLabel 15300 2650 0    50   Input ~ 0
CYCLE_TIME_MON2
$Comp
L Connector_Generic:Conn_02x02_Counter_Clockwise J2
U 1 1 5E350F13
P 7300 6450
AR Path="/5CC2A4AE/5E350F13" Ref="J2"  Part="1" 
AR Path="/614E87D3/5E350F13" Ref="J?"  Part="1" 
F 0 "J2" H 7350 6667 50  0000 C CNN
F 1 "24V Power" H 7350 6576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 7300 6450 50  0001 C CNN
F 3 "~" H 7300 6450 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 2 positions" H 100 0   50  0001 C CNN "Description"
F 5 "Samtec" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-102-01-F-D-K" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 100 0   50  0001 C CNN "Supplier"
F 9 "IPL1-102-01-F-D-K" H 100 0   50  0001 C CNN "SupplierRef"
F 10 "--" H 7300 6450 50  0001 C CNN "LCSC"
	1    7300 6450
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E380EE6
P 6950 6650
AR Path="/5E380EE6" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E380EE6" Ref="#PWR032"  Part="1" 
AR Path="/614E87D3/5E380EE6" Ref="#PWR?"  Part="1" 
F 0 "#PWR032" H 6950 6400 50  0001 C CNN
F 1 "GND" H 6955 6477 50  0000 C CNN
F 2 "" H 6950 6650 50  0001 C CNN
F 3 "" H 6950 6650 50  0001 C CNN
	1    6950 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 6650 6950 6550
Wire Wire Line
	6950 6450 7000 6450
Wire Wire Line
	7000 6550 6950 6550
Connection ~ 6950 6550
Wire Wire Line
	6950 6550 6950 6450
NoConn ~ 7600 9900
$Comp
L power:PWR_FLAG #FLG04
U 1 1 61586CE7
P 15100 8400
F 0 "#FLG04" H 15100 8475 50  0001 C CNN
F 1 "PWR_FLAG" V 15100 8528 50  0000 L CNN
F 2 "" H 15100 8400 50  0001 C CNN
F 3 "~" H 15100 8400 50  0001 C CNN
	1    15100 8400
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 6158789D
P 7500 6450
F 0 "#FLG02" H 7500 6525 50  0001 C CNN
F 1 "PWR_FLAG" H 7500 6623 50  0000 C CNN
F 2 "" H 7500 6450 50  0001 C CNN
F 3 "~" H 7500 6450 50  0001 C CNN
	1    7500 6450
	1    0    0    -1  
$EndComp
Connection ~ 7500 6450
$Comp
L power:PWR_FLAG #FLG03
U 1 1 61588752
P 7500 6550
F 0 "#FLG03" H 7500 6625 50  0001 C CNN
F 1 "PWR_FLAG" H 7500 6723 50  0000 C CNN
F 2 "" H 7500 6550 50  0001 C CNN
F 3 "~" H 7500 6550 50  0001 C CNN
	1    7500 6550
	-1   0    0    1   
$EndComp
Connection ~ 7500 6550
$Comp
L power:GNDA #PWR045
U 1 1 614EF6E2
P 9600 9950
F 0 "#PWR045" H 9600 9700 50  0001 C CNN
F 1 "GNDA" H 9605 9777 50  0000 C CNN
F 2 "" H 9600 9950 50  0001 C CNN
F 3 "" H 9600 9950 50  0001 C CNN
	1    9600 9950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR040
U 1 1 614EFDDC
P 9000 10400
F 0 "#PWR040" H 9000 10150 50  0001 C CNN
F 1 "GNDA" H 9005 10227 50  0000 C CNN
F 2 "" H 9000 10400 50  0001 C CNN
F 3 "" H 9000 10400 50  0001 C CNN
	1    9000 10400
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Switching:TPS5430DDA U5
U 1 1 615056F3
P 12100 7450
F 0 "U5" H 12100 7917 50  0000 C CNN
F 1 "TPS5430DDAR" H 12100 7826 50  0000 C CNN
F 2 "Package_SO:TI_SO-PowerPAD-8_ThermalVias" H 12150 7100 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/tps5430.pdf" H 12100 7450 50  0001 C CNN
F 4 "C9864" H 12100 7450 50  0001 C CNN "LCSC"
F 5 "Texas Instruments" H 12100 7450 50  0001 C CNN "Manufacturer_Name"
F 6 "TPS5430DDAR" H 12100 7450 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "3A 36V DC-DC Converter" H 12100 7450 50  0001 C CNN "Description"
F 8 "0;0;-90" H 12100 7450 50  0001 C CNN "JLCPCB_CORRECTION"
	1    12100 7450
	1    0    0    -1  
$EndComp
NoConn ~ 11600 7650
$Comp
L power:GND #PWR059
U 1 1 5F4CCBE4
P 15100 7700
AR Path="/5CC2A4AE/5F4CCBE4" Ref="#PWR059"  Part="1" 
AR Path="/614E87D3/5F4CCBE4" Ref="#PWR?"  Part="1" 
F 0 "#PWR059" H 15100 7450 50  0001 C CNN
F 1 "GND" H 15105 7527 50  0000 C CNN
F 2 "" H 15100 7700 50  0001 C CNN
F 3 "" H 15100 7700 50  0001 C CNN
	1    15100 7700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR050
U 1 1 6150E49C
P 12050 7900
AR Path="/5CC2A4AE/6150E49C" Ref="#PWR050"  Part="1" 
AR Path="/614E87D3/6150E49C" Ref="#PWR?"  Part="1" 
F 0 "#PWR050" H 12050 7650 50  0001 C CNN
F 1 "GND" H 12055 7727 50  0000 C CNN
F 2 "" H 12050 7900 50  0001 C CNN
F 3 "" H 12050 7900 50  0001 C CNN
	1    12050 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	12000 7850 12000 7900
Wire Wire Line
	12000 7900 12050 7900
Wire Wire Line
	12100 7850 12100 7900
Wire Wire Line
	12100 7900 12050 7900
Connection ~ 12050 7900
Text GLabel 12600 7650 2    50   Input ~ 0
BUCK_FB
$Comp
L Device:C_Small C14
U 1 1 6151379B
P 12700 7350
F 0 "C14" H 12792 7396 50  0000 L CNN
F 1 "10n" H 12792 7305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 12700 7350 50  0001 C CNN
F 3 "~" H 12700 7350 50  0001 C CNN
F 4 "C15195" H 12700 7350 50  0001 C CNN "LCSC"
F 5 "X7R ±10% 50V 10NF 0402 MULTILAYER CERAMIC CAPACITORS MLCC" H 12700 7350 50  0001 C CNN "Description"
F 6 "Samsung Electro-Mechanics" H 12700 7350 50  0001 C CNN "Manufacturer_Name"
F 7 "CL05B103KB5NNNC" H 12700 7350 50  0001 C CNN "Manufacturer_Part_Number"
	1    12700 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	12700 7250 12600 7250
Wire Wire Line
	12600 7450 12700 7450
$Comp
L Device:D_Schottky_Small D5
U 1 1 6151887D
P 13100 7600
F 0 "D5" V 13054 7670 50  0000 L CNN
F 1 "SS34" V 13145 7670 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" V 13100 7600 50  0001 C CNN
F 3 "~" V 13100 7600 50  0001 C CNN
F 4 "C8678" H 13100 7600 50  0001 C CNN "LCSC"
F 5 "40V 3A 550mV @ 3A SMA(DO-214AC) Schottky Barrier Diode" H 13100 7600 50  0001 C CNN "Description"
F 6 "MDD（Microdiode Electronics）" H 13100 7600 50  0001 C CNN "Manufacturer_Name"
F 7 "SS34" H 13100 7600 50  0001 C CNN "Manufacturer_Part_Number"
	1    13100 7600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR053
U 1 1 615191E8
P 13100 7700
AR Path="/5CC2A4AE/615191E8" Ref="#PWR053"  Part="1" 
AR Path="/614E87D3/615191E8" Ref="#PWR?"  Part="1" 
F 0 "#PWR053" H 13100 7450 50  0001 C CNN
F 1 "GND" H 13105 7527 50  0000 C CNN
F 2 "" H 13100 7700 50  0001 C CNN
F 3 "" H 13100 7700 50  0001 C CNN
	1    13100 7700
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L2
U 1 1 6151A525
P 13300 7450
F 0 "L2" V 13485 7450 50  0000 C CNN
F 1 "22u" V 13394 7450 50  0000 C CNN
F 2 "EuroPress:SWPA6040S" H 13300 7450 50  0001 C CNN
F 3 "~" H 13300 7450 50  0001 C CNN
F 4 "Shielded Power Inductor 22uH 20% SMD" H 13300 7450 50  0001 C CNN "Description"
F 5 "C83454" H 13300 7450 50  0001 C CNN "LCSC"
F 6 "Sunlord" H 13300 7450 50  0001 C CNN "Manufacturer_Name"
F 7 "SWPA6045S220MT" H 13300 7450 50  0001 C CNN "Manufacturer_Part_Number"
	1    13300 7450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	13200 7450 13100 7450
Connection ~ 12700 7450
Wire Wire Line
	13100 7500 13100 7450
Connection ~ 13100 7450
Wire Wire Line
	13100 7450 12700 7450
$Comp
L Device:CP_Small C15
U 1 1 6151FAA7
P 13500 7600
F 0 "C15" H 13588 7646 50  0000 L CNN
F 1 "150u" H 13588 7555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 13500 7600 50  0001 C CNN
F 3 "~" H 13500 7600 50  0001 C CNN
F 4 "6.3V ±20% 35mΩ 300kHz -55℃~+105℃ 150uF CASE-B_3528 Tantalum Capacitor" H 13500 7600 50  0001 C CNN "Description"
F 5 "C178384" H 13500 7600 50  0001 C CNN "LCSC"
F 6 "Panasonic" H 13500 7600 50  0001 C CNN "Manufacturer_Name"
F 7 "6TPG150MZG" H 13500 7600 50  0001 C CNN "Manufacturer_Part_Number"
F 8 "0;0;180" H 13500 7600 50  0001 C CNN "JLCPCB_CORRECTION"
	1    13500 7600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR054
U 1 1 6152042E
P 13500 7700
AR Path="/5CC2A4AE/6152042E" Ref="#PWR054"  Part="1" 
AR Path="/614E87D3/6152042E" Ref="#PWR?"  Part="1" 
F 0 "#PWR054" H 13500 7450 50  0001 C CNN
F 1 "GND" H 13505 7527 50  0000 C CNN
F 2 "" H 13500 7700 50  0001 C CNN
F 3 "" H 13500 7700 50  0001 C CNN
	1    13500 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	13500 7450 13500 7500
$Comp
L Device:C_Small C13
U 1 1 6152469B
P 11300 7350
F 0 "C13" H 11392 7396 50  0000 L CNN
F 1 "10u" H 11392 7305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 11300 7350 50  0001 C CNN
F 3 "~" H 11300 7350 50  0001 C CNN
F 4 "X7R ±10% 50V 10uF 1210 Multilayer Ceramic Capacitors MLCC" H 11300 7350 50  0001 C CNN "Description"
F 5 "C77102" H 11300 7350 50  0001 C CNN "LCSC"
F 6 "Murata" H 11300 7350 50  0001 C CNN "Manufacturer_Name"
F 7 "GRM32ER71H106KA12L" H 11300 7350 50  0001 C CNN "Manufacturer_Part_Number"
	1    11300 7350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR047
U 1 1 615255EB
P 11300 7450
AR Path="/5CC2A4AE/615255EB" Ref="#PWR047"  Part="1" 
AR Path="/614E87D3/615255EB" Ref="#PWR?"  Part="1" 
F 0 "#PWR047" H 11300 7200 50  0001 C CNN
F 1 "GND" H 11305 7277 50  0000 C CNN
F 2 "" H 11300 7450 50  0001 C CNN
F 3 "" H 11300 7450 50  0001 C CNN
	1    11300 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 61529096
P 13050 8500
F 0 "R10" H 13109 8546 50  0000 L CNN
F 1 "10k" H 13109 8455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 13050 8500 50  0001 C CNN
F 3 "~" H 13050 8500 50  0001 C CNN
F 4 "C25744" H 13050 8500 50  0001 C CNN "LCSC"
F 5 "UNI-ROYAL(Uniroyal Elec)" H 13050 8500 50  0001 C CNN "Manufacturer_Name"
F 6 "0402WGF1002TCE" H 13050 8500 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "Resistor 10k 1% 62.5mW 0402" H 13050 8500 50  0001 C CNN "Description"
	1    13050 8500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R11
U 1 1 615296B2
P 13050 8800
F 0 "R11" H 13109 8846 50  0000 L CNN
F 1 "3.9k" H 13109 8755 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 13050 8800 50  0001 C CNN
F 3 "~" H 13050 8800 50  0001 C CNN
F 4 "Resistor 3.9k 1% 62.5mW 0402" H 13050 8800 50  0001 C CNN "Description"
F 5 "C51721" H 13050 8800 50  0001 C CNN "LCSC"
F 6 "UNI-ROYAL(Uniroyal Elec)" H 13050 8800 50  0001 C CNN "Manufacturer_Name"
F 7 "0402WGF3901TCE" H 13050 8800 50  0001 C CNN "Manufacturer_Part_Number"
	1    13050 8800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R12
U 1 1 61529992
P 13050 9000
F 0 "R12" H 13109 9046 50  0000 L CNN
F 1 "2k" H 13109 8955 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 13050 9000 50  0001 C CNN
F 3 "~" H 13050 9000 50  0001 C CNN
F 4 "Resistor 2k 1% 62.5mW 0402" H 13050 9000 50  0001 C CNN "Description"
F 5 "C4109" H 13050 9000 50  0001 C CNN "LCSC"
F 6 "UNI-ROYAL(Uniroyal Elec)" H 13050 9000 50  0001 C CNN "Manufacturer_Name"
F 7 "0402WGF2001TCE" H 13050 9000 50  0001 C CNN "Manufacturer_Part_Number"
	1    13050 9000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 61529D9C
P 13050 9100
AR Path="/5CC2A4AE/61529D9C" Ref="#PWR052"  Part="1" 
AR Path="/614E87D3/61529D9C" Ref="#PWR?"  Part="1" 
F 0 "#PWR052" H 13050 8850 50  0001 C CNN
F 1 "GND" H 13055 8927 50  0000 C CNN
F 2 "" H 13050 9100 50  0001 C CNN
F 3 "" H 13050 9100 50  0001 C CNN
	1    13050 9100
	1    0    0    -1  
$EndComp
Text GLabel 12950 8650 0    50   Input ~ 0
BUCK_FB
Wire Wire Line
	13050 8600 13050 8650
Wire Wire Line
	12950 8650 13050 8650
Connection ~ 13050 8650
Wire Wire Line
	13050 8650 13050 8700
Text Notes 12850 8850 2    39   ~ 0
Voltage ref nominal: 1.221V\n=> Vout = V ±3.35%
Text Notes 11650 7850 2    39   ~ 0
ENA pin has an internal\npull-up current source
$Comp
L Device:CP_Small C16
U 1 1 61536A5C
P 13900 7600
F 0 "C16" H 13988 7646 50  0000 L CNN
F 1 "150u" H 13988 7555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 13900 7600 50  0001 C CNN
F 3 "~" H 13900 7600 50  0001 C CNN
F 4 "6.3V ±20% 35mΩ 300kHz -55℃~+105℃ 150uF CASE-B_3528 Tantalum Capacitor" H 13900 7600 50  0001 C CNN "Description"
F 5 "C178384" H 13900 7600 50  0001 C CNN "LCSC"
F 6 "Panasonic" H 13900 7600 50  0001 C CNN "Manufacturer_Name"
F 7 "6TPG150MZG" H 13900 7600 50  0001 C CNN "Manufacturer_Part_Number"
F 8 "0;0;180" H 13900 7600 50  0001 C CNN "JLCPCB_CORRECTION"
	1    13900 7600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 61536EEA
P 13900 7700
AR Path="/5CC2A4AE/61536EEA" Ref="#PWR055"  Part="1" 
AR Path="/614E87D3/61536EEA" Ref="#PWR?"  Part="1" 
F 0 "#PWR055" H 13900 7450 50  0001 C CNN
F 1 "GND" H 13905 7527 50  0000 C CNN
F 2 "" H 13900 7700 50  0001 C CNN
F 3 "" H 13900 7700 50  0001 C CNN
	1    13900 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	13900 7450 13900 7500
$Comp
L Device:CP_Small C17
U 1 1 615399E6
P 14300 7600
F 0 "C17" H 14388 7646 50  0000 L CNN
F 1 "150u" H 14388 7555 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 14300 7600 50  0001 C CNN
F 3 "~" H 14300 7600 50  0001 C CNN
F 4 "6.3V ±20% 35mΩ 300kHz -55℃~+105℃ 150uF CASE-B_3528 Tantalum Capacitor" H 14300 7600 50  0001 C CNN "Description"
F 5 "C178384" H 14300 7600 50  0001 C CNN "LCSC"
F 6 "Panasonic" H 14300 7600 50  0001 C CNN "Manufacturer_Name"
F 7 "6TPG150MZG" H 14300 7600 50  0001 C CNN "Manufacturer_Part_Number"
F 8 "0;0;180" H 14300 7600 50  0001 C CNN "JLCPCB_CORRECTION"
	1    14300 7600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR056
U 1 1 61539E96
P 14300 7700
AR Path="/5CC2A4AE/61539E96" Ref="#PWR056"  Part="1" 
AR Path="/614E87D3/61539E96" Ref="#PWR?"  Part="1" 
F 0 "#PWR056" H 14300 7450 50  0001 C CNN
F 1 "GND" H 14305 7527 50  0000 C CNN
F 2 "" H 14300 7700 50  0001 C CNN
F 3 "" H 14300 7700 50  0001 C CNN
	1    14300 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	14300 7450 14300 7500
$Comp
L power:+3V3 #PWR057
U 1 1 6153CD7D
P 14450 7400
AR Path="/5CC2A4AE/6153CD7D" Ref="#PWR057"  Part="1" 
AR Path="/614E87D3/6153CD7D" Ref="#PWR?"  Part="1" 
F 0 "#PWR057" H 14450 7250 50  0001 C CNN
F 1 "+3V3" H 14465 7573 50  0000 C CNN
F 2 "" H 14450 7400 50  0001 C CNN
F 3 "" H 14450 7400 50  0001 C CNN
	1    14450 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	14450 7400 14450 7450
Wire Wire Line
	14450 7450 14300 7450
Connection ~ 13500 7450
Wire Wire Line
	13500 7450 13400 7450
Connection ~ 13900 7450
Wire Wire Line
	13900 7450 13500 7450
Connection ~ 14300 7450
Wire Wire Line
	14300 7450 13900 7450
Wire Wire Line
	11100 7250 11300 7250
Wire Wire Line
	11100 7100 11100 7250
Connection ~ 11300 7250
Wire Wire Line
	11300 7250 11600 7250
$Comp
L Device:D_Schottky_Small D2
U 1 1 615CA608
P 8400 6100
F 0 "D2" H 8400 5893 50  0000 C CNN
F 1 "SS54" H 8400 5984 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" V 8400 6100 50  0001 C CNN
F 3 "~" V 8400 6100 50  0001 C CNN
F 4 "40V 5A 550mV @ 5A SMA(DO-214AC) Schottky Barrier Diode" H 8400 6100 50  0001 C CNN "Description"
F 5 "C22452" H 8400 6100 50  0001 C CNN "LCSC"
F 6 "MDD（Microdiode Electronics）" H 8400 6100 50  0001 C CNN "Manufacturer_Name"
F 7 "SS54" H 8400 6100 50  0001 C CNN "Manufacturer_Part_Number"
	1    8400 6100
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Schottky_Small D3
U 1 1 615D6008
P 8400 6950
F 0 "D3" H 8400 6743 50  0000 C CNN
F 1 "SS54" H 8400 6834 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" V 8400 6950 50  0001 C CNN
F 3 "~" V 8400 6950 50  0001 C CNN
F 4 "40V 5A 550mV @ 5A SMA(DO-214AC) Schottky Barrier Diode" H 8400 6950 50  0001 C CNN "Description"
F 5 "C22452" H 8400 6950 50  0001 C CNN "LCSC"
F 6 "MDD（Microdiode Electronics）" H 8400 6950 50  0001 C CNN "Manufacturer_Name"
F 7 "SS54" H 8400 6950 50  0001 C CNN "Manufacturer_Part_Number"
	1    8400 6950
	-1   0    0    1   
$EndComp
Wire Wire Line
	8050 6950 8300 6950
Wire Wire Line
	8500 6950 8700 6950
Connection ~ 8700 6950
$Comp
L power:+3V3 #PWR051
U 1 1 6161FB82
P 13050 8400
AR Path="/5CC2A4AE/6161FB82" Ref="#PWR051"  Part="1" 
AR Path="/614E87D3/6161FB82" Ref="#PWR?"  Part="1" 
F 0 "#PWR051" H 13050 8250 50  0001 C CNN
F 1 "+3V3" H 13065 8573 50  0000 C CNN
F 2 "" H 13050 8400 50  0001 C CNN
F 3 "" H 13050 8400 50  0001 C CNN
	1    13050 8400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
