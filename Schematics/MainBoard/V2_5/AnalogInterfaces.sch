EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 2
Title "EuroPress - Analog interfaces / Power supplies"
Date "2019-01-06"
Rev "2.2"
Comp "LGR"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	16000 5400 500  5400
Wire Notes Line
	6700 5400 6700 11200
Text Notes 6750 5550 0    60   ~ 0
POWER SUPPLIES
Wire Notes Line
	6700 5600 7600 5600
Wire Notes Line
	7600 5600 7600 5400
$Comp
L Regulator_Switching:TSR_1-24120 U3
U 1 1 5C50B29A
P 8100 8300
F 0 "U3" H 8100 8667 50  0000 C CNN
F 1 "TSR_1-2490" H 8100 8576 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 8100 8150 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 8100 8300 50  0001 C CNN
F 4 "DC-DC switching regulator 15-36Vin 9V 1A" H -400 0   50  0001 C CNN "Description"
F 5 "Traco Power" H -400 0   50  0001 C CNN "Manufacturer_Name"
F 6 "TSR 1-2490" H -400 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "495-TSR-1-2490" H -400 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -400 0   50  0001 C CNN "Supplier"
F 9 "495-TSR-1-2490" H -400 0   50  0001 C CNN "SupplierRef"
	1    8100 8300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C8
U 1 1 5C50B3BD
P 7400 8400
F 0 "C8" H 7492 8446 50  0000 L CNN
F 1 "22uF" H 7492 8355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-7343-31_Kemet-D_Pad2.25x2.55mm_HandSolder" H 7400 8400 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/427/tr3-239731.pdf" H 7400 8400 50  0001 C CNN
F 4 "Condensateur au tantale - CMS Solide 22uF 35V 10% 2917" H -400 0   50  0001 C CNN "Description"
F 5 "Vishay/Sprague" H -400 0   50  0001 C CNN "Manufacturer_Name"
F 6 "TR3D226K035C0300" H -400 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "74-TR3D226K035C0300" H -400 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -400 0   50  0001 C CNN "Supplier"
F 9 "74-TR3D226K035C0300" H -400 0   50  0001 C CNN "SupplierRef"
	1    7400 8400
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR042
U 1 1 5C50B6C9
P 7400 8100
F 0 "#PWR042" H 7400 7950 50  0001 C CNN
F 1 "+24V" H 7415 8273 50  0000 C CNN
F 2 "" H 7400 8100 50  0001 C CNN
F 3 "" H 7400 8100 50  0001 C CNN
	1    7400 8100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5C50B774
P 7400 8600
F 0 "#PWR043" H 7400 8350 50  0001 C CNN
F 1 "GND" H 7405 8427 50  0000 C CNN
F 2 "" H 7400 8600 50  0001 C CNN
F 3 "" H 7400 8600 50  0001 C CNN
	1    7400 8600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR044
U 1 1 5C50B80A
P 8100 8600
F 0 "#PWR044" H 8100 8350 50  0001 C CNN
F 1 "GND" H 8105 8427 50  0000 C CNN
F 2 "" H 8100 8600 50  0001 C CNN
F 3 "" H 8100 8600 50  0001 C CNN
	1    8100 8600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 8200 7400 8200
Wire Wire Line
	7400 8200 7400 8100
Wire Wire Line
	7400 8300 7400 8200
Wire Wire Line
	7400 8500 7400 8600
Wire Wire Line
	8100 8500 8100 8600
Wire Wire Line
	8500 8200 8700 8200
Wire Wire Line
	8700 8200 8700 8100
Text Notes 6950 10750 0    50   ~ 0
±12V/±83mA isolated switching regulator (ADC/DAC/OpAmps supply)
Text Notes 7300 8950 0    50   ~ 0
9V/1A switching regulator (Arduino Due supply)
Text Notes 11200 7400 0    50   ~ 0
5V/1.5A linear regulator
Text Notes 11200 9500 0    50   ~ 0
3V3/1.5A linear regulator
Wire Notes Line
	12600 500  12600 5400
Text Notes 12650 650  0    60   ~ 0
CONNECTORS
Wire Notes Line
	12600 700  13350 700 
Wire Notes Line
	13350 700  13350 500 
$Comp
L power:+12VA #PWR049
U 1 1 5F70136B
P 9100 9550
F 0 "#PWR049" H 9100 9400 50  0001 C CNN
F 1 "+12VA" H 9115 9723 50  0000 C CNN
F 2 "" H 9100 9550 50  0001 C CNN
F 3 "" H 9100 9550 50  0001 C CNN
	1    9100 9550
	1    0    0    -1  
$EndComp
$Comp
L EuroPress:TEC_2-2422 U4
U 1 1 5F722FD2
P 8400 9900
F 0 "U4" H 8400 10367 50  0000 C CNN
F 1 "TEC_2-2422" H 8400 10276 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TMR-xxxx_THT" H 8400 9550 50  0001 C CNN
F 3 "https://assets.tracopower.com/20190124103907/TEC2/documents/tec2-datasheet.pdf" H 8400 9400 50  0001 C CNN
F 4 "DC-DC isolated regulator 2W 18-36Vin +/-12V +/-83mA SMD" H 100 0   50  0001 C CNN "Description"
F 5 "Traco Power" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "TEC 2-2422" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "495-TEC2-2422" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 100 0   50  0001 C CNN "Supplier"
F 9 "495-TEC2-2422" H 100 0   50  0001 C CNN "SupplierRef"
	1    8400 9900
	1    0    0    -1  
$EndComp
$Comp
L power:-12VA #PWR052
U 1 1 5F72405D
P 9450 10200
F 0 "#PWR052" H 9450 10050 50  0001 C CNN
F 1 "-12VA" H 9465 10373 50  0000 C CNN
F 2 "" H 9450 10200 50  0001 C CNN
F 3 "" H 9450 10200 50  0001 C CNN
	1    9450 10200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR041
U 1 1 5F7241B7
P 7200 10200
F 0 "#PWR041" H 7200 9950 50  0001 C CNN
F 1 "GND" H 7205 10027 50  0000 C CNN
F 2 "" H 7200 10200 50  0001 C CNN
F 3 "" H 7200 10200 50  0001 C CNN
	1    7200 10200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 9700 9100 9700
Wire Wire Line
	9100 9700 9100 9600
Wire Wire Line
	9450 10100 9450 10200
$Comp
L power:+24V #PWR040
U 1 1 5F77FDAA
P 7200 9600
F 0 "#PWR040" H 7200 9450 50  0001 C CNN
F 1 "+24V" H 7215 9773 50  0000 C CNN
F 2 "" H 7200 9600 50  0001 C CNN
F 3 "" H 7200 9600 50  0001 C CNN
	1    7200 9600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 9600 7200 9700
Wire Wire Line
	7200 9700 7600 9700
Wire Wire Line
	7600 10100 7200 10100
Wire Wire Line
	7200 10100 7200 10200
$Comp
L Device:CP_Small C7
U 1 1 5F7E11F1
P 7200 9900
F 0 "C7" H 7288 9946 50  0000 L CNN
F 1 "220uF" H 7288 9855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_8x10" H 7200 9900 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 7200 9900 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 220uF 35V 7000h 105°C" H -300 0   50  0001 C CNN "Description"
F 5 "Nichicon" H -300 0   50  0001 C CNN "Manufacturer_Name"
F 6 "UCW1V221MNL1GS" H -300 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "647-UCW1V221MNL1GS" H -300 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -300 0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V221MNL1GS" H -300 0   50  0001 C CNN "SupplierRef"
	1    7200 9900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 9800 7200 9700
Connection ~ 7200 9700
Wire Wire Line
	7200 10000 7200 10100
Connection ~ 7200 10100
$Comp
L Device:C_Small C13
U 1 1 5F842168
P 9600 9750
F 0 "C13" H 9688 9796 50  0000 L CNN
F 1 "22uF" H 9688 9705 50  0000 L CNN
F 2 "Capacitor_SMD:C_2220_5650Metric_Pad1.97x5.40mm_HandSolder" H 9600 9750 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/212/KEM_C1002_X7R_SMD-1102033.pdf" H 9600 9750 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25volts 22uF X7R 10%" H 100 0   50  0001 C CNN "Description"
F 5 "Kemet" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "C2220C226K3RACTU" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C2220C226K3R" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 100 0   50  0001 C CNN "Supplier"
F 9 "80-C2220C226K3R" H 100 0   50  0001 C CNN "SupplierRef"
	1    9600 9750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 5F842224
P 9000 10250
F 0 "C11" H 8912 10204 50  0000 R CNN
F 1 "22uF" H 8912 10295 50  0000 R CNN
F 2 "Capacitor_SMD:C_2220_5650Metric_Pad1.97x5.40mm_HandSolder" H 9000 10250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/212/KEM_C1002_X7R_SMD-1102033.pdf" H 9000 10250 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25volts 22uF X7R 10%" H 100 0   50  0001 C CNN "Description"
F 5 "Kemet" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "C2220C226K3RACTU" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C2220C226K3R" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 100 0   50  0001 C CNN "Supplier"
F 9 "80-C2220C226K3R" H 100 0   50  0001 C CNN "SupplierRef"
	1    9000 10250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR048
U 1 1 5F84244C
P 9000 10400
F 0 "#PWR048" H 9000 10150 50  0001 C CNN
F 1 "GND" H 9005 10227 50  0000 C CNN
F 2 "" H 9000 10400 50  0001 C CNN
F 3 "" H 9000 10400 50  0001 C CNN
	1    9000 10400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR053
U 1 1 5F8424DF
P 9600 9950
F 0 "#PWR053" H 9600 9700 50  0001 C CNN
F 1 "GND" H 9605 9777 50  0000 C CNN
F 2 "" H 9600 9950 50  0001 C CNN
F 3 "" H 9600 9950 50  0001 C CNN
	1    9600 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 9900 9600 9900
Wire Wire Line
	9600 9900 9600 9950
Wire Wire Line
	9600 9850 9600 9900
Connection ~ 9600 9900
Wire Wire Line
	9600 9650 9600 9600
Wire Wire Line
	9600 9600 9100 9600
Connection ~ 9100 9600
Wire Wire Line
	9100 9600 9100 9550
Wire Wire Line
	9000 10350 9000 10400
Wire Wire Line
	9000 10150 9000 10100
Wire Wire Line
	9450 10100 9000 10100
Connection ~ 9000 10100
Wire Wire Line
	9000 10100 8900 10100
$Comp
L Device:EMI_Filter_LCL FL1
U 1 1 5C7FF49C
P 14200 6500
F 0 "FL1" H 14200 6817 50  0000 C CNN
F 1 "ACH3218-103-TD01" H 14200 6726 50  0000 C CNN
F 2 "EuroPress:ACH3212" V 14200 6500 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/400/tf_commercial_power_ach3218_en-837204.pdf" V 14200 6500 50  0001 C CNN
F 4 "Large Current EMI Filter" H 0   0   50  0001 C CNN "Description"
F 5 "TDK" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ACH3218-103-TD01" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "810-ACH3218-103-TD01" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "810-ACH3218-103-TD01" H 0   0   50  0001 C CNN "SupplierRef"
	1    14200 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:EMI_Filter_LCL FL2
U 1 1 5C7FF656
P 14200 8600
F 0 "FL2" H 14200 8917 50  0000 C CNN
F 1 "ACH3218-103-TD01" H 14200 8826 50  0000 C CNN
F 2 "EuroPress:ACH3212" V 14200 8600 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/400/tf_commercial_power_ach3218_en-837204.pdf" V 14200 8600 50  0001 C CNN
F 4 "Large Current EMI Filter" H 0   0   50  0001 C CNN "Description"
F 5 "TDK" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ACH3218-103-TD01" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "810-ACH3218-103-TD01" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "810-ACH3218-103-TD01" H 0   0   50  0001 C CNN "SupplierRef"
	1    14200 8600
	1    0    0    -1  
$EndComp
$Comp
L EuroPress:MPM3515 U5
U 1 1 5C8241DF
P 12400 6500
F 0 "U5" H 12400 7265 50  0000 C CNN
F 1 "MPM3515" H 12400 7174 50  0000 C CNN
F 2 "Europress:CONV_MPM3515GQV-AEC1-Z" H 12450 5700 50  0001 L BNN
F 3 "https://www.monolithicpower.com/pub/media/document/MPM3515_r1.0.pdf" H 12450 5800 50  0001 L BNN
F 4 "4-36V input, 1.5A output Synchronous, Step-Down Converter with an Integrated Inductor" H 0   0   50  0001 C CNN "Description"
F 5 "Monolithic Power Systems" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "MPM3515GQV-Z" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "946-MPM3515GQV-Z" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "946-MPM3515GQV-Z" H 0   0   50  0001 C CNN "SupplierRef"
	1    12400 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5C824443
P 13300 6600
F 0 "R7" H 13359 6646 50  0000 L CNN
F 1 "75k" H 13359 6555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13300 6600 50  0001 C CNN
F 3 "~" H 13300 6600 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 75Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB753V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB753V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB753V" H 0   0   50  0001 C CNN "SupplierRef"
	1    13300 6600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5C82456B
P 13300 7000
F 0 "R8" H 13359 7046 50  0000 L CNN
F 1 "14.3k" H 13359 6955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13300 7000 50  0001 C CNN
F 3 "~" H 13300 7000 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 14.3Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB1432V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB1432V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB1432V" H 0   0   50  0001 C CNN "SupplierRef"
	1    13300 7000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5C8249C1
P 13300 8700
F 0 "R9" H 13359 8746 50  0000 L CNN
F 1 "75k" H 13359 8655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13300 8700 50  0001 C CNN
F 3 "~" H 13300 8700 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 75Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB753V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB753V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB753V" H 0   0   50  0001 C CNN "SupplierRef"
	1    13300 8700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 5C8249C7
P 13300 9100
F 0 "R10" H 13359 9146 50  0000 L CNN
F 1 "24.3k" H 13359 9055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13300 9100 50  0001 C CNN
F 3 "~" H 13300 9100 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 24.3Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB2432V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB2432V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB2432V" H 0   0   50  0001 C CNN "SupplierRef"
	1    13300 9100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 5C846F9D
P 11400 6250
F 0 "R5" H 11459 6296 50  0000 L CNN
F 1 "100k" H 11459 6205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11400 6250 50  0001 C CNN
F 3 "~" H 11400 6250 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 100Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB104V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB104V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB104V" H 0   0   50  0001 C CNN "SupplierRef"
	1    11400 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR066
U 1 1 5C88D71C
P 13300 7200
F 0 "#PWR066" H 13300 6950 50  0001 C CNN
F 1 "GND" H 13305 7027 50  0000 C CNN
F 2 "" H 13300 7200 50  0001 C CNN
F 3 "" H 13300 7200 50  0001 C CNN
	1    13300 7200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR067
U 1 1 5C88D7D8
P 13300 9300
F 0 "#PWR067" H 13300 9050 50  0001 C CNN
F 1 "GND" H 13305 9127 50  0000 C CNN
F 2 "" H 13300 9300 50  0001 C CNN
F 3 "" H 13300 9300 50  0001 C CNN
	1    13300 9300
	1    0    0    -1  
$EndComp
Wire Wire Line
	13200 8500 13300 8500
Wire Wire Line
	13300 8500 13300 8600
Wire Wire Line
	13300 8800 13300 8900
Wire Wire Line
	13300 9200 13300 9300
Wire Wire Line
	13200 8900 13300 8900
Connection ~ 13300 8900
Wire Wire Line
	13300 8900 13300 9000
Wire Wire Line
	13200 6400 13300 6400
Wire Wire Line
	13300 6400 13300 6500
Wire Wire Line
	13300 6700 13300 6800
Wire Wire Line
	13300 7100 13300 7200
Wire Wire Line
	13200 6800 13300 6800
Connection ~ 13300 6800
Wire Wire Line
	13300 6800 13300 6900
$Comp
L Device:R_Small R6
U 1 1 5C9A8265
P 11400 8350
F 0 "R6" H 11459 8396 50  0000 L CNN
F 1 "100k" H 11459 8305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11400 8350 50  0001 C CNN
F 3 "~" H 11400 8350 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 100Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB104V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB104V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB104V" H 0   0   50  0001 C CNN "SupplierRef"
	1    11400 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	11400 6350 11400 6400
Wire Wire Line
	11400 6400 11600 6400
Wire Wire Line
	11400 8450 11400 8500
Wire Wire Line
	11400 8500 11600 8500
Wire Wire Line
	11400 8250 11400 8200
Connection ~ 11400 8200
Wire Wire Line
	11400 8200 11600 8200
Wire Wire Line
	11400 6150 11400 6100
Connection ~ 11400 6100
Wire Wire Line
	11400 6100 11600 6100
$Comp
L Device:C_Small C14
U 1 1 5CC892CF
P 10600 6250
F 0 "C14" H 10692 6296 50  0000 L CNN
F 1 "4.7uF" H 10692 6205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1812_4532Metric_Pad1.30x3.40mm_HandSolder" H 10600 6250 50  0001 C CNN
F 3 "~" H 10600 6250 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 1812 50Vdc 4.7uF X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C1812F475K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C1812F475K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C1812F475K5R" H 0   0   50  0001 C CNN "SupplierRef"
	1    10600 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C16
U 1 1 5CC894DB
P 11000 6250
F 0 "C16" H 11092 6296 50  0000 L CNN
F 1 "0.1uF" H 11092 6205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11000 6250 50  0001 C CNN
F 3 "~" H 11000 6250 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "SupplierRef"
	1    11000 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5CC897B6
P 10600 6400
F 0 "#PWR055" H 10600 6150 50  0001 C CNN
F 1 "GND" H 10605 6227 50  0000 C CNN
F 2 "" H 10600 6400 50  0001 C CNN
F 3 "" H 10600 6400 50  0001 C CNN
	1    10600 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR060
U 1 1 5CC89865
P 11000 6400
F 0 "#PWR060" H 11000 6150 50  0001 C CNN
F 1 "GND" H 11005 6227 50  0000 C CNN
F 2 "" H 11000 6400 50  0001 C CNN
F 3 "" H 11000 6400 50  0001 C CNN
	1    11000 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 6350 10600 6400
Wire Wire Line
	11000 6350 11000 6400
$Comp
L Device:C_Small C15
U 1 1 5CD03CD6
P 10600 8350
F 0 "C15" H 10692 8396 50  0000 L CNN
F 1 "4.7uF" H 10692 8305 50  0000 L CNN
F 2 "Capacitor_SMD:C_1812_4532Metric_Pad1.30x3.40mm_HandSolder" H 10600 8350 50  0001 C CNN
F 3 "~" H 10600 8350 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 1812 50Vdc 4.7uF X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C1812F475K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C1812F475K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C1812F475K5R" H 0   0   50  0001 C CNN "SupplierRef"
	1    10600 8350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C17
U 1 1 5CD03CDC
P 11000 8350
F 0 "C17" H 11092 8396 50  0000 L CNN
F 1 "0.1uF" H 11092 8305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11000 8350 50  0001 C CNN
F 3 "~" H 11000 8350 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "SupplierRef"
	1    11000 8350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR057
U 1 1 5CD03CE2
P 10600 8500
F 0 "#PWR057" H 10600 8250 50  0001 C CNN
F 1 "GND" H 10605 8327 50  0000 C CNN
F 2 "" H 10600 8500 50  0001 C CNN
F 3 "" H 10600 8500 50  0001 C CNN
	1    10600 8500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR061
U 1 1 5CD03CE8
P 11000 8500
F 0 "#PWR061" H 11000 8250 50  0001 C CNN
F 1 "GND" H 11005 8327 50  0000 C CNN
F 2 "" H 11000 8500 50  0001 C CNN
F 3 "" H 11000 8500 50  0001 C CNN
	1    11000 8500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 8450 10600 8500
Wire Wire Line
	11000 8450 11000 8500
Wire Wire Line
	10600 8250 10600 8200
Wire Wire Line
	10600 8200 11000 8200
Wire Wire Line
	11000 8250 11000 8200
Connection ~ 11000 8200
Wire Wire Line
	11000 8200 11400 8200
Wire Wire Line
	10600 6150 10600 6100
Connection ~ 10600 6100
Wire Wire Line
	10600 6100 11000 6100
Wire Wire Line
	11000 6150 11000 6100
Connection ~ 11000 6100
Wire Wire Line
	11000 6100 11400 6100
$Comp
L Device:C_Small C18
U 1 1 5CE2B4F8
P 13700 6550
F 0 "C18" H 13792 6596 50  0000 L CNN
F 1 "47uF" H 13792 6505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 13700 6550 50  0001 C CNN
F 3 "~" H 13700 6550 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 1210 6.3Vdc 47uF X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Murata Electronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "GCJ32ER70J476KE01L" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "81-GCJ32ER70J476KE1L" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "81-GCJ32ER70J476KE1L" H 0   0   50  0001 C CNN "SupplierRef"
	1    13700 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR068
U 1 1 5CE2B4FE
P 13700 6700
F 0 "#PWR068" H 13700 6450 50  0001 C CNN
F 1 "GND" H 13705 6527 50  0000 C CNN
F 2 "" H 13700 6700 50  0001 C CNN
F 3 "" H 13700 6700 50  0001 C CNN
	1    13700 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	13700 6650 13700 6700
$Comp
L Device:C_Small C19
U 1 1 5CE56720
P 13700 8650
F 0 "C19" H 13792 8696 50  0000 L CNN
F 1 "47uF" H 13792 8605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 13700 8650 50  0001 C CNN
F 3 "~" H 13700 8650 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 1210 6.3Vdc 47uF X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Murata Electronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "GCJ32ER70J476KE01L" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "81-GCJ32ER70J476KE1L" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "81-GCJ32ER70J476KE1L" H 0   0   50  0001 C CNN "SupplierRef"
	1    13700 8650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR069
U 1 1 5CE56726
P 13700 8800
F 0 "#PWR069" H 13700 8550 50  0001 C CNN
F 1 "GND" H 13705 8627 50  0000 C CNN
F 2 "" H 13700 8800 50  0001 C CNN
F 3 "" H 13700 8800 50  0001 C CNN
	1    13700 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	13700 8750 13700 8800
Connection ~ 13300 6400
Wire Wire Line
	13700 6450 13700 6400
Wire Wire Line
	13300 6400 13700 6400
Connection ~ 13300 8500
Wire Wire Line
	13700 8550 13700 8500
Wire Wire Line
	13700 8500 13300 8500
$Comp
L power:+3V3 #PWR080
U 1 1 5CF5C408
P 15100 8400
F 0 "#PWR080" H 15100 8250 50  0001 C CNN
F 1 "+3V3" H 15115 8573 50  0000 C CNN
F 2 "" H 15100 8400 50  0001 C CNN
F 3 "" H 15100 8400 50  0001 C CNN
	1    15100 8400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR076
U 1 1 5CF5C4E4
P 15100 6300
F 0 "#PWR076" H 15100 6150 50  0001 C CNN
F 1 "+5V" H 15115 6473 50  0000 C CNN
F 2 "" H 15100 6300 50  0001 C CNN
F 3 "" H 15100 6300 50  0001 C CNN
	1    15100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR064
U 1 1 5D095077
P 12400 7400
F 0 "#PWR064" H 12400 7150 50  0001 C CNN
F 1 "GND" H 12405 7227 50  0000 C CNN
F 2 "" H 12400 7400 50  0001 C CNN
F 3 "" H 12400 7400 50  0001 C CNN
	1    12400 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR065
U 1 1 5D095145
P 12400 9500
F 0 "#PWR065" H 12400 9250 50  0001 C CNN
F 1 "GND" H 12405 9327 50  0000 C CNN
F 2 "" H 12400 9500 50  0001 C CNN
F 3 "" H 12400 9500 50  0001 C CNN
	1    12400 9500
	1    0    0    -1  
$EndComp
Wire Wire Line
	12300 9400 12300 9450
Wire Wire Line
	12300 9450 12400 9450
Wire Wire Line
	12500 9450 12500 9400
Wire Wire Line
	12400 9500 12400 9450
Connection ~ 12400 9450
Wire Wire Line
	12400 9450 12500 9450
Wire Wire Line
	12300 7300 12300 7350
Wire Wire Line
	12300 7350 12400 7350
Wire Wire Line
	12500 7350 12500 7300
Wire Wire Line
	12400 7400 12400 7350
Connection ~ 12400 7350
Wire Wire Line
	12400 7350 12500 7350
Connection ~ 13700 6400
Connection ~ 13700 8500
$Comp
L Device:C_Small C22
U 1 1 5D1D847C
P 15100 6550
F 0 "C22" H 15192 6596 50  0000 L CNN
F 1 "0.1uF" H 15192 6505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 15100 6550 50  0001 C CNN
F 3 "~" H 15100 6550 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "SupplierRef"
	1    15100 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR077
U 1 1 5D1D8482
P 15100 6700
F 0 "#PWR077" H 15100 6450 50  0001 C CNN
F 1 "GND" H 15105 6527 50  0000 C CNN
F 2 "" H 15100 6700 50  0001 C CNN
F 3 "" H 15100 6700 50  0001 C CNN
	1    15100 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	15100 6650 15100 6700
$Comp
L Device:C_Small C23
U 1 1 5D20729D
P 15100 8650
F 0 "C23" H 15192 8696 50  0000 L CNN
F 1 "0.1uF" H 15192 8605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 15100 8650 50  0001 C CNN
F 3 "~" H 15100 8650 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "SupplierRef"
	1    15100 8650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR081
U 1 1 5D2072A3
P 15100 8800
F 0 "#PWR081" H 15100 8550 50  0001 C CNN
F 1 "GND" H 15105 8627 50  0000 C CNN
F 2 "" H 15100 8800 50  0001 C CNN
F 3 "" H 15100 8800 50  0001 C CNN
	1    15100 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	15100 8750 15100 8800
Wire Wire Line
	15100 8500 15100 8400
Wire Wire Line
	15100 8550 15100 8500
Connection ~ 15100 8500
Wire Wire Line
	15100 6400 15100 6300
Wire Wire Line
	15100 6450 15100 6400
Connection ~ 15100 6400
$Comp
L power:GND #PWR070
U 1 1 5D2F4FEA
P 14200 6700
F 0 "#PWR070" H 14200 6450 50  0001 C CNN
F 1 "GND" H 14205 6527 50  0000 C CNN
F 2 "" H 14200 6700 50  0001 C CNN
F 3 "" H 14200 6700 50  0001 C CNN
	1    14200 6700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR073
U 1 1 5D2F50A5
P 14200 8800
F 0 "#PWR073" H 14200 8550 50  0001 C CNN
F 1 "GND" H 14205 8627 50  0000 C CNN
F 2 "" H 14200 8800 50  0001 C CNN
F 3 "" H 14200 8800 50  0001 C CNN
	1    14200 8800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C20
U 1 1 5D3552A5
P 14700 6550
F 0 "C20" H 14788 6596 50  0000 L CNN
F 1 "10uF" H 14788 6505 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 14700 6550 50  0001 C CNN
F 3 "~" H 14700 6550 50  0001 C CNN
F 4 "Vishay/Sprague" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 5 "TR3A106K010C0900" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 6 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Mouser Part Number"
F 7 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 8 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "SupplierRef"
F 9 "Condensateur au tantale CMS 10uF 10V 10% case A" H 0   0   50  0001 C CNN "Description"
	1    14700 6550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C21
U 1 1 5D3554D7
P 14700 8650
F 0 "C21" H 14788 8696 50  0000 L CNN
F 1 "10uF" H 14788 8605 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 14700 8650 50  0001 C CNN
F 3 "~" H 14700 8650 50  0001 C CNN
F 4 "Vishay/Sprague" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 5 "TR3A106K010C0900" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 6 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Mouser Part Number"
F 7 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 8 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "SupplierRef"
F 9 "Condensateur au tantale CMS 10uF 10V 10% case A" H 0   0   50  0001 C CNN "Description"
	1    14700 8650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR074
U 1 1 5D3555E9
P 14700 6700
F 0 "#PWR074" H 14700 6450 50  0001 C CNN
F 1 "GND" H 14705 6527 50  0000 C CNN
F 2 "" H 14700 6700 50  0001 C CNN
F 3 "" H 14700 6700 50  0001 C CNN
	1    14700 6700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR075
U 1 1 5D3556A8
P 14700 8800
F 0 "#PWR075" H 14700 8550 50  0001 C CNN
F 1 "GND" H 14705 8627 50  0000 C CNN
F 2 "" H 14700 8800 50  0001 C CNN
F 3 "" H 14700 8800 50  0001 C CNN
	1    14700 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	14200 8800 14200 8700
Wire Wire Line
	14700 8550 14700 8500
Connection ~ 14700 8500
Wire Wire Line
	14700 8500 15100 8500
Wire Wire Line
	14700 8750 14700 8800
Wire Wire Line
	14700 6700 14700 6650
Wire Wire Line
	14700 6450 14700 6400
Connection ~ 14700 6400
Wire Wire Line
	14700 6400 15100 6400
Wire Wire Line
	14200 6700 14200 6600
$Comp
L power:+24V #PWR054
U 1 1 5D542DF9
P 10600 6050
F 0 "#PWR054" H 10600 5900 50  0001 C CNN
F 1 "+24V" H 10615 6223 50  0000 C CNN
F 2 "" H 10600 6050 50  0001 C CNN
F 3 "" H 10600 6050 50  0001 C CNN
	1    10600 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 6050 10600 6100
Connection ~ 7400 8200
$Comp
L power:+24V #PWR056
U 1 1 5D65E046
P 10600 8150
F 0 "#PWR056" H 10600 8000 50  0001 C CNN
F 1 "+24V" H 10615 8323 50  0000 C CNN
F 2 "" H 10600 8150 50  0001 C CNN
F 3 "" H 10600 8150 50  0001 C CNN
	1    10600 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 8150 10600 8200
Connection ~ 10600 8200
Wire Wire Line
	7600 6550 7500 6550
$Comp
L EuroPress:12CWQ03FN D?
U 1 1 5C794BB3
P 8400 6100
AR Path="/5C794BB3" Ref="D?"  Part="1" 
AR Path="/5CC2A4AE/5C794BB3" Ref="D2"  Part="1" 
F 0 "D2" V 8700 5900 50  0000 L CNN
F 1 "12CWQ03FN" V 8600 5550 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin4" H 8350 6100 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/427/vs-12cwq03f-890365.pdf" H 8350 6100 50  0001 C CNN
F 4 "High Performance Schottky Rectifier, 2 x 6 A" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "VS-12CWQ03FN-M3" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "78-VS-12CWQ03FN-M3" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "78-VS-12CWQ03FN-M3" H 0   0   50  0001 C CNN "SupplierRef"
	1    8400 6100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 6450 8200 6450
Wire Wire Line
	8200 5750 8400 5750
Wire Wire Line
	7500 6450 7600 6450
Wire Wire Line
	7600 6450 7600 6050
Wire Wire Line
	8550 6050 8600 6050
Wire Wire Line
	8600 6050 8600 6100
Wire Wire Line
	8600 6150 8550 6150
Wire Wire Line
	8600 6100 8900 6100
Connection ~ 8600 6100
Wire Wire Line
	8600 6100 8600 6150
$Comp
L EuroPress:12CWQ03FN D?
U 1 1 5C88D503
P 8400 7000
AR Path="/5C88D503" Ref="D?"  Part="1" 
AR Path="/5CC2A4AE/5C88D503" Ref="D3"  Part="1" 
F 0 "D3" V 8700 6800 50  0000 L CNN
F 1 "12CWQ03FN" V 8600 6450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin4" H 8350 7000 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/427/vs-12cwq03f-890365.pdf" H 8350 7000 50  0001 C CNN
F 4 "High Performance Schottky Rectifier, 2 x 6 A" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "VS-12CWQ03FN-M3" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "78-VS-12CWQ03FN-M3" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "78-VS-12CWQ03FN-M3" H 0   0   50  0001 C CNN "SupplierRef"
	1    8400 7000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 7350 8200 7350
Wire Wire Line
	8200 6650 8400 6650
Wire Wire Line
	8550 6950 8600 6950
Wire Wire Line
	8600 6950 8600 7000
Wire Wire Line
	8600 7050 8550 7050
Wire Wire Line
	8600 7000 8900 7000
Connection ~ 8600 7000
Wire Wire Line
	8600 7000 8600 7050
Wire Wire Line
	7600 6550 7600 6950
$Comp
L Device:CP_Small C9
U 1 1 5C999785
P 8900 6250
F 0 "C9" H 8988 6296 50  0000 L CNN
F 1 "330uF" H 8988 6205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 8900 6250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 8900 6250 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 330uF 35V 7000h 105°C" H 0   0   50  0001 C CNN "Description"
F 5 "Nichicon" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "UCW1V331MNL1GS" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "647-UCW1V331MNL1GS" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V331MNL1GS" H 0   0   50  0001 C CNN "SupplierRef"
	1    8900 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C10
U 1 1 5C99987B
P 8900 7150
F 0 "C10" H 8988 7196 50  0000 L CNN
F 1 "330uF" H 8988 7105 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 8900 7150 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 8900 7150 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 330uF 35V 7000h 105°C" H 0   0   50  0001 C CNN "Description"
F 5 "Nichicon" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "UCW1V331MNL1GS" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "647-UCW1V331MNL1GS" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V331MNL1GS" H 0   0   50  0001 C CNN "SupplierRef"
	1    8900 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C99996B
P 8900 6400
AR Path="/5C99996B" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5C99996B" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 8900 6150 50  0001 C CNN
F 1 "GND" H 8905 6227 50  0000 C CNN
F 2 "" H 8900 6400 50  0001 C CNN
F 3 "" H 8900 6400 50  0001 C CNN
	1    8900 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C999A30
P 8900 7300
AR Path="/5C999A30" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5C999A30" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 8900 7050 50  0001 C CNN
F 1 "GND" H 8905 7127 50  0000 C CNN
F 2 "" H 8900 7300 50  0001 C CNN
F 3 "" H 8900 7300 50  0001 C CNN
	1    8900 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 7050 8900 7000
Connection ~ 8900 7000
Wire Wire Line
	8900 7000 9350 7000
Wire Wire Line
	8900 7250 8900 7300
Wire Wire Line
	8900 6150 8900 6100
Wire Wire Line
	8900 6350 8900 6400
$Comp
L power:+24V #PWR050
U 1 1 5CADE914
P 9400 6050
F 0 "#PWR050" H 9400 5900 50  0001 C CNN
F 1 "+24V" H 9415 6223 50  0000 C CNN
F 2 "" H 9400 6050 50  0001 C CNN
F 3 "" H 9400 6050 50  0001 C CNN
	1    9400 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse_Small F1
U 1 1 5CB16DB0
P 7950 6050
F 0 "F1" H 7950 6235 50  0000 C CNN
F 1 "Fuse_Small" H 7950 6144 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" H 7950 6050 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/358/typ_OGN-14523.pdf" H 7950 6050 50  0001 C CNN
F 4 "Fuse holder 5x20mm" H 0   0   50  0001 C CNN "Description"
F 5 "Schurter" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "0031.8201" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "693-0031.8201" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "693-0031.8201" H 0   0   50  0001 C CNN "SupplierRef"
	1    7950 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5750 8200 6050
Wire Wire Line
	8200 6650 8200 6950
$Comp
L Device:Fuse_Small F2
U 1 1 5CB173E0
P 7950 6950
F 0 "F2" H 7950 7135 50  0000 C CNN
F 1 "Fuse_Small" H 7950 7044 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_0031_8201_Horizontal_Open" H 7950 6950 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/358/typ_OGN-14523.pdf" H 7950 6950 50  0001 C CNN
F 4 "Fuse holder 5x20mm" H 0   0   50  0001 C CNN "Description"
F 5 "Schurter" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "0031.8201" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "693-0031.8201" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "693-0031.8201" H 0   0   50  0001 C CNN "SupplierRef"
	1    7950 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 6050 7850 6050
Wire Wire Line
	8050 6050 8200 6050
Connection ~ 8200 6050
Wire Wire Line
	8200 6050 8200 6450
Wire Wire Line
	7600 6950 7850 6950
Wire Wire Line
	8050 6950 8200 6950
Connection ~ 8200 6950
Wire Wire Line
	8200 6950 8200 7350
$Comp
L Device:CP_Small C12
U 1 1 5CE602CE
P 9400 6250
F 0 "C12" H 9488 6296 50  0000 L CNN
F 1 "330uF" H 9488 6205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 9400 6250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/293/e-cw-17369.pdf" H 9400 6250 50  0001 C CNN
F 4 "Condensateur électrolytique en aluminium CMS 330uF 35V 7000h 105°C" H 0   0   50  0001 C CNN "Description"
F 5 "Nichicon" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "UCW1V331MNL1GS" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "647-UCW1V331MNL1GS" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "647-UCW1V331MNL1GS" H 0   0   50  0001 C CNN "SupplierRef"
	1    9400 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CE602D4
P 9400 6400
AR Path="/5CE602D4" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5CE602D4" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 9400 6150 50  0001 C CNN
F 1 "GND" H 9405 6227 50  0000 C CNN
F 2 "" H 9400 6400 50  0001 C CNN
F 3 "" H 9400 6400 50  0001 C CNN
	1    9400 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 6350 9400 6400
$Comp
L Device:L_Small L1
U 1 1 5CE982EA
P 9150 6100
F 0 "L1" V 9335 6100 50  0000 C CNN
F 1 "10uH" V 9244 6100 50  0000 C CNN
F 2 "Inductor_SMD:L_Bourns-SRR1005" H 9150 6100 50  0001 C CNN
F 3 "https://www.bourns.com/docs/product-datasheets/srr1005.pdf" H 9150 6100 50  0001 C CNN
F 4 "Shielded Power Inductor 10uH 20% SMD 1005" H 0   0   50  0001 C CNN "Description"
F 5 "Bourns" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "SRR1005-100M" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "652-SRR1005-100M" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "652-SRR1005-100M" H 0   0   50  0001 C CNN "SupplierRef"
	1    9150 6100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 6050 9400 6100
Wire Wire Line
	8900 6100 9050 6100
Connection ~ 8900 6100
Wire Wire Line
	9250 6100 9400 6100
Connection ~ 9400 6100
Wire Wire Line
	9400 6100 9400 6150
Text GLabel 9350 7000 2    50   Output ~ 0
24V_OUT
$Comp
L power:+9V #PWR045
U 1 1 5CBFABF7
P 8700 8100
F 0 "#PWR045" H 8700 7950 50  0001 C CNN
F 1 "+9V" H 8715 8273 50  0000 C CNN
F 2 "" H 8700 8100 50  0001 C CNN
F 3 "" H 8700 8100 50  0001 C CNN
	1    8700 8100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 5F4CBABD
P 14500 7500
F 0 "J3" H 14528 7476 50  0000 L CNN
F 1 "5V_Ext" H 14528 7385 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 14500 7500 50  0001 C CNN
F 3 "~" H 14500 7500 50  0001 C CNN
F 4 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Sample"
F 8 "N/A" H 0   0   50  0001 C CNN "Supplier"
F 9 "N/A" H 0   0   50  0001 C CNN "SupplierRef"
	1    14500 7500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J4
U 1 1 5F4CBCC1
P 15400 7500
F 0 "J4" H 15427 7476 50  0000 L CNN
F 1 "3V3_Ext" H 15427 7385 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 15400 7500 50  0001 C CNN
F 3 "~" H 15400 7500 50  0001 C CNN
F 4 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Sample"
F 8 "N/A" H 0   0   50  0001 C CNN "Supplier"
F 9 "N/A" H 0   0   50  0001 C CNN "SupplierRef"
	1    15400 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR071
U 1 1 5F4CC7CE
P 14200 7400
F 0 "#PWR071" H 14200 7250 50  0001 C CNN
F 1 "+5V" H 14215 7573 50  0000 C CNN
F 2 "" H 14200 7400 50  0001 C CNN
F 3 "" H 14200 7400 50  0001 C CNN
	1    14200 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR072
U 1 1 5F4CC9D4
P 14200 7700
F 0 "#PWR072" H 14200 7450 50  0001 C CNN
F 1 "GND" H 14205 7527 50  0000 C CNN
F 2 "" H 14200 7700 50  0001 C CNN
F 3 "" H 14200 7700 50  0001 C CNN
	1    14200 7700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR079
U 1 1 5F4CCBE4
P 15100 7700
F 0 "#PWR079" H 15100 7450 50  0001 C CNN
F 1 "GND" H 15105 7527 50  0000 C CNN
F 2 "" H 15100 7700 50  0001 C CNN
F 3 "" H 15100 7700 50  0001 C CNN
	1    15100 7700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR078
U 1 1 5F4CCCB5
P 15100 7400
F 0 "#PWR078" H 15100 7250 50  0001 C CNN
F 1 "+3V3" H 15115 7573 50  0000 C CNN
F 2 "" H 15100 7400 50  0001 C CNN
F 3 "" H 15100 7400 50  0001 C CNN
	1    15100 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	14200 7400 14200 7500
Wire Wire Line
	14200 7500 14300 7500
Wire Wire Line
	14300 7600 14200 7600
Wire Wire Line
	14200 7600 14200 7700
Wire Wire Line
	15100 7700 15100 7600
Wire Wire Line
	15100 7600 15200 7600
Wire Wire Line
	15200 7500 15100 7500
Wire Wire Line
	15100 7500 15100 7400
Text Notes 16850 11250 0    50   ~ 0
+12VA:\n        U9 - TS922AIDT\n        U10 - LT1490A\n        U11 - LT1490A\n        U12 - AD7610BSTZ\n    \n        * LT1490A :\n         - 50µA quiescent current per amplifier\n         - Can drive 20mA per output\n             Sortie 1 : EV proportionnelle\n                 Impedance : 1MOhms => I = 10µA\n             Sortie 2 : Générateur ultrason\n                 Impedance : 15.4kOhms => I = 0.65mA\n    \n        * TS922AIDT :\n         - Total supply current for 12V : ~~2.6mA (Fig.4 p. 7)\n\n        * AD7610BSTZ :\n         - Typical operating current : 1.4mA (VCC = 15V, with internal reference buffer)\n\n    Total : 50µA + 10µA + 0.65mA + 2.6mA + 1.4mA = ~~5mA\n\n-12VA:\n        U12 - AD7610BSTZ\n\n        * AD7610BSTZ\n         - Typical operating current : 0.7mA (VEE = -15V)
Text Notes 16650 9000 0    50   ~ 0
Power supplies sizing :
Text Notes 10500 9900 0    60   ~ 0
POWER STATUS
Wire Notes Line
	10450 9950 11250 9950
Wire Notes Line
	11250 9950 11250 9750
$Comp
L Device:R_Small R?
U 1 1 5E2812FF
P 10900 10700
AR Path="/5E2812FF" Ref="R?"  Part="1" 
AR Path="/5CC2A4AE/5E2812FF" Ref="R3"  Part="1" 
F 0 "R3" H 10959 10746 50  0000 L CNN
F 1 "330R" H 10959 10655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10900 10700 50  0001 C CNN
F 3 "~" H 10900 10700 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 330ohms 0.1% 25ppm" H 6000 2750 50  0001 C CNN "Description"
F 5 "Panasonic" H 6000 2750 50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB331V" H 6000 2750 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB331V" H 6000 2750 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6000 2750 50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB331V" H 6000 2750 50  0001 C CNN "SupplierRef"
	1    10900 10700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E28130B
P 11300 10700
AR Path="/5E28130B" Ref="R?"  Part="1" 
AR Path="/5CC2A4AE/5E28130B" Ref="R4"  Part="1" 
F 0 "R4" H 11359 10746 50  0000 L CNN
F 1 "330R" H 11359 10655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11300 10700 50  0001 C CNN
F 3 "~" H 11300 10700 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 330ohms 0.1% 25ppm" H 6000 2750 50  0001 C CNN "Description"
F 5 "Panasonic" H 6000 2750 50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB331V" H 6000 2750 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB331V" H 6000 2750 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6000 2750 50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB331V" H 6000 2750 50  0001 C CNN "SupplierRef"
	1    11300 10700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D?
U 1 1 5E281317
P 10900 10450
AR Path="/5E281317" Ref="D?"  Part="1" 
AR Path="/5CC2A4AE/5E281317" Ref="D4"  Part="1" 
F 0 "D4" V 10900 10382 50  0000 R CNN
F 1 "5V_ON" V 10855 10382 50  0001 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10900 10450 50  0001 C CNN
F 3 "http://optoelectronics.liteon.com/upload/download/DS22-2000-118/LTST-C171KGKT.pdf" V 10900 10450 50  0001 C CNN
F 4 "Lite-On" H 6000 2250 50  0001 C CNN "Manufacturer_Name"
F 5 "LTST-C171KGKT" H 6000 2250 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "859-LTST-C171KGKT" H 6000 2250 50  0001 C CNN "Mouser Part Number"
F 7 "Mouser" H 6000 2250 50  0001 C CNN "Supplier"
F 8 "859-LTST-C171KGKT" H 6000 2250 50  0001 C CNN "SupplierRef"
F 9 "DEL standard CMS 0805 green clear 571nm 35mcd Vf=2V If=20mA" H 6000 2250 50  0001 C CNN "Description"
	1    10900 10450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10900 10550 10900 10600
Wire Wire Line
	11300 10550 11300 10600
$Comp
L power:GND #PWR?
U 1 1 5E28131F
P 10900 10850
AR Path="/5E28131F" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E28131F" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 10900 10600 50  0001 C CNN
F 1 "GND" H 10905 10677 50  0000 C CNN
F 2 "" H 10900 10850 50  0001 C CNN
F 3 "" H 10900 10850 50  0001 C CNN
	1    10900 10850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E281325
P 11300 10850
AR Path="/5E281325" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E281325" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 11300 10600 50  0001 C CNN
F 1 "GND" H 11305 10677 50  0000 C CNN
F 2 "" H 11300 10850 50  0001 C CNN
F 3 "" H 11300 10850 50  0001 C CNN
	1    11300 10850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5E28132B
P 11300 10300
AR Path="/5E28132B" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E28132B" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 11300 10150 50  0001 C CNN
F 1 "+3V3" H 11315 10473 50  0000 C CNN
F 2 "" H 11300 10300 50  0001 C CNN
F 3 "" H 11300 10300 50  0001 C CNN
	1    11300 10300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E281331
P 10900 10300
AR Path="/5E281331" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E281331" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 10900 10150 50  0001 C CNN
F 1 "+5V" H 10915 10473 50  0000 C CNN
F 2 "" H 10900 10300 50  0001 C CNN
F 3 "" H 10900 10300 50  0001 C CNN
	1    10900 10300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 10800 10900 10850
Wire Wire Line
	11300 10800 11300 10850
Wire Wire Line
	11300 10350 11300 10300
Wire Wire Line
	10900 10300 10900 10350
Wire Notes Line
	10450 11200 10450 9750
Wire Notes Line
	10470 9750 11820 9750
$Comp
L Device:LED_Small D?
U 1 1 5E281343
P 11300 10450
AR Path="/5E281343" Ref="D?"  Part="1" 
AR Path="/5CC2A4AE/5E281343" Ref="D5"  Part="1" 
F 0 "D5" V 11300 10382 50  0000 R CNN
F 1 "3V3_ON" V 11255 10382 50  0001 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 11300 10450 50  0001 C CNN
F 3 "http://optoelectronics.liteon.com/upload/download/DS22-2000-118/LTST-C171KGKT.pdf" V 11300 10450 50  0001 C CNN
F 4 "Lite-On" H 6400 2250 50  0001 C CNN "Manufacturer_Name"
F 5 "LTST-C171KGKT" H 6400 2250 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "859-LTST-C171KGKT" H 6400 2250 50  0001 C CNN "Mouser Part Number"
F 7 "Mouser" H 6400 2250 50  0001 C CNN "Supplier"
F 8 "859-LTST-C171KGKT" H 6400 2250 50  0001 C CNN "SupplierRef"
F 9 "DEL standard CMS 0805 green clear 571nm 35mcd Vf=2V If=20mA" H 6400 2250 50  0001 C CNN "Description"
	1    11300 10450
	0    -1   -1   0   
$EndComp
Wire Notes Line
	11810 9950 11810 9740
Text GLabel 15300 1800 0    50   Input ~ 0
SPI_SCK
Text GLabel 15300 1700 0    50   Input ~ 0
SPI_MOSI
Text GLabel 15300 1600 0    50   Input ~ 0
SPI_MISO
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5F65E2BD
P 15600 2650
AR Path="/5F65E2BD" Ref="J?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2BD" Ref="J6"  Part="1" 
F 0 "J6" H 15680 2646 50  0000 L CNN
F 1 "Conn_01x03" H 15680 2601 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 15600 2650 50  0001 C CNN
F 3 "~" H 15600 2650 50  0001 C CNN
F 4 "N/A" H 400 -2050 50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 400 -2050 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 400 -2050 50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 400 -2050 50  0001 C CNN "Sample"
F 8 "N/A" H 400 -2050 50  0001 C CNN "Supplier"
F 9 "N/A" H 400 -2050 50  0001 C CNN "SupplierRef"
	1    15600 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	15300 2550 15400 2550
Wire Wire Line
	15300 2650 15400 2650
$Comp
L power:GND #PWR?
U 1 1 5F65E2C5
P 15300 2800
AR Path="/5F65E2C5" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2C5" Ref="#PWR083"  Part="1" 
F 0 "#PWR083" H 15300 2550 50  0001 C CNN
F 1 "GND" H 15305 2627 50  0000 C CNN
F 2 "" H 15300 2800 50  0001 C CNN
F 3 "" H 15300 2800 50  0001 C CNN
	1    15300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	15400 2750 15300 2750
Wire Wire Line
	15300 2750 15300 2800
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5F65E2CF
P 15600 1700
AR Path="/5F65E2CF" Ref="J?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2CF" Ref="J5"  Part="1" 
F 0 "J5" H 15680 1692 50  0000 L CNN
F 1 "SPI" H 15680 1601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 15600 1700 50  0001 C CNN
F 3 "~" H 15600 1700 50  0001 C CNN
F 4 "N/A" H 1850 -2950 50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 1850 -2950 50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 1850 -2950 50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 1850 -2950 50  0001 C CNN "Sample"
F 8 "N/A" H 1850 -2950 50  0001 C CNN "Supplier"
F 9 "N/A" H 1850 -2950 50  0001 C CNN "SupplierRef"
	1    15600 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F65E2D5
P 15300 1950
AR Path="/5F65E2D5" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5F65E2D5" Ref="#PWR082"  Part="1" 
F 0 "#PWR082" H 15300 1700 50  0001 C CNN
F 1 "GND" H 15305 1777 50  0000 C CNN
F 2 "" H 15300 1950 50  0001 C CNN
F 3 "" H 15300 1950 50  0001 C CNN
	1    15300 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	15300 1600 15400 1600
Wire Wire Line
	15400 1700 15300 1700
Wire Wire Line
	15300 1800 15400 1800
Wire Wire Line
	15400 1900 15300 1900
Wire Wire Line
	15300 1900 15300 1950
Text GLabel 15300 2550 0    50   Input ~ 0
CYCLE_TIME_MON1
Text GLabel 15300 2650 0    50   Input ~ 0
CYCLE_TIME_MON2
$Comp
L EuroPress:MPM3515 U6
U 1 1 5C824307
P 12400 8600
F 0 "U6" H 12400 9365 50  0000 C CNN
F 1 "MPM3515" H 12400 9274 50  0000 C CNN
F 2 "Europress:CONV_MPM3515GQV-AEC1-Z" H 12450 7800 50  0001 L BNN
F 3 "https://www.monolithicpower.com/pub/media/document/MPM3515_r1.0.pdf" H 12450 7900 50  0001 L BNN
F 4 "4-36V input, 1.5A output Synchronous, Step-Down Converter with an Integrated Inductor" H 0   0   50  0001 C CNN "Description"
F 5 "Monolithic Power Systems" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "MPM3515GQV-Z" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "946-MPM3515GQV-Z" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "946-MPM3515GQV-Z" H 0   0   50  0001 C CNN "SupplierRef"
	1    12400 8600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Counter_Clockwise J2
U 1 1 5E350F13
P 7300 6450
F 0 "J2" H 7350 6667 50  0000 C CNN
F 1 "24V Power" H 7350 6576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 7300 6450 50  0001 C CNN
F 3 "~" H 7300 6450 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 2 positions" H 100 0   50  0001 C CNN "Description"
F 5 "Samtec" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-102-01-F-D-K" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 100 0   50  0001 C CNN "Supplier"
F 9 "IPL1-102-01-F-D-K" H 100 0   50  0001 C CNN "SupplierRef"
	1    7300 6450
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E380EE6
P 6950 6650
AR Path="/5E380EE6" Ref="#PWR?"  Part="1" 
AR Path="/5CC2A4AE/5E380EE6" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 6950 6400 50  0001 C CNN
F 1 "GND" H 6955 6477 50  0000 C CNN
F 2 "" H 6950 6650 50  0001 C CNN
F 3 "" H 6950 6650 50  0001 C CNN
	1    6950 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 6650 6950 6550
Wire Wire Line
	6950 6450 7000 6450
Wire Wire Line
	7000 6550 6950 6550
Connection ~ 6950 6550
Wire Wire Line
	6950 6550 6950 6450
NoConn ~ 11600 6650
NoConn ~ 11600 7000
NoConn ~ 13200 6200
NoConn ~ 13200 6000
NoConn ~ 13200 8100
NoConn ~ 13200 8300
NoConn ~ 11600 8750
NoConn ~ 11600 9100
NoConn ~ 7600 9900
Wire Wire Line
	13700 8500 13900 8500
Wire Wire Line
	14500 8500 14700 8500
Wire Wire Line
	13700 6400 13900 6400
Wire Wire Line
	14500 6400 14700 6400
$EndSCHEMATC
