EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DB37_Female_MountingHoles J9
U 1 1 5D7CFDC1
P 10300 3700
F 0 "J9" H 10480 3702 50  0000 L CNN
F 1 "Press body" H 10480 3611 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-37_Female_Vertical_P2.77x2.84mm_MountingHoles" H 10300 3700 50  0001 C CNN
F 3 " ~" H 10300 3700 50  0001 C CNN
F 4 "Sub-D 37 female" H 0   0   50  0001 C CNN "Description"
F 5 "Amphenol" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "L77TSCH37SOL2RM5" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "523-L77TSCH37SOL2RM5" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "523-L77TSCH37SOL2RM5" H 0   0   50  0001 C CNN "SupplierRef"
	1    10300 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB25_Female_MountingHoles J8
U 1 1 5D7D214A
P 7050 4500
F 0 "J8" H 7230 4502 50  0000 L CNN
F 1 "US Generator" H 7230 4411 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 7050 4500 50  0001 C CNN
F 3 " ~" H 7050 4500 50  0001 C CNN
F 4 "Sub-D 25 female" H 0   0   50  0001 C CNN "Description"
F 5 "Amphenol" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "D25S24A4GL00LF" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "649-D25S24A4GL00LF" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "649-D25S24A4GL00LF" H 0   0   50  0001 C CNN "SupplierRef"
	1    7050 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB9_Female_MountingHoles J7
U 1 1 5D7D40E3
P 7000 1750
F 0 "J7" H 7180 1752 50  0000 L CNN
F 1 "Press base" H 7180 1661 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Female_Vertical_P2.77x2.84mm_MountingHoles" H 7000 1750 50  0001 C CNN
F 3 " ~" H 7000 1750 50  0001 C CNN
F 4 "Sub-D 09 female" H 0   0   50  0001 C CNN "Description"
F 5 "Amphenol" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "D09S24A4GL00LF" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "649-D09S24A4GL00LF" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "649-D09S24A4GL00LF" H 0   0   50  0001 C CNN "SupplierRef"
	1    7000 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5D7DA737
P 6550 2350
F 0 "#PWR010" H 6550 2100 50  0001 C CNN
F 1 "GND" H 6555 2177 50  0000 C CNN
F 2 "" H 6550 2350 50  0001 C CNN
F 3 "" H 6550 2350 50  0001 C CNN
	1    6550 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1550 6550 1550
Wire Wire Line
	6550 1550 6550 1950
Wire Wire Line
	6700 1950 6550 1950
Connection ~ 6550 1950
Wire Wire Line
	6550 1950 6550 2350
Wire Wire Line
	6000 1750 6700 1750
Text Label 6000 1350 0    50   ~ 0
24V_outputs
Text Label 6000 1750 0    50   ~ 0
24V_main
Wire Wire Line
	6000 2050 6700 2050
Wire Wire Line
	6000 1850 6700 1850
Wire Wire Line
	6000 2150 6700 2150
Text Label 6000 2050 0    50   ~ 0
DIG_IN_CH1
Text Label 6000 1850 0    50   ~ 0
DIG_IN_CH2
Text Label 6000 2150 0    50   ~ 0
DIG_IN_CH3
$Comp
L Connector_Generic:Conn_02x16_Odd_Even J1
U 1 1 5D7DEB66
P 2300 1900
F 0 "J1" H 2350 2817 50  0000 C CNN
F 1 "Digital inputs" H 2350 2726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x16_P2.54mm_Vertical" H 2300 1900 50  0001 C CNN
F 3 "~" H 2300 1900 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 16 positions" H 0   0   50  0001 C CNN "Description"
F 5 "Samtec" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-116-01-F-D-K" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 0   0   50  0001 C CNN "Supplier"
F 9 "IPL1-116-01-F-D-K" H 0   0   50  0001 C CNN "SupplierRef"
	1    2300 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x12_Odd_Even J2
U 1 1 5D7E11BF
P 2300 3750
F 0 "J2" H 2350 4467 50  0000 C CNN
F 1 "Digital outputs" H 2350 4376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x12_P2.54mm_Vertical" H 2300 3750 50  0001 C CNN
F 3 "~" H 2300 3750 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 12 positions" H 0   0   50  0001 C CNN "Description"
F 5 "Samtec" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-112-01-F-D-K" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 0   0   50  0001 C CNN "Supplier"
F 9 "IPL1-112-01-F-D-K" H 0   0   50  0001 C CNN "SupplierRef"
	1    2300 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J3
U 1 1 5D7E2ECE
P 2300 5150
F 0 "J3" H 2350 5567 50  0000 C CNN
F 1 "Analog in/out" H 2350 5476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 2300 5150 50  0001 C CNN
F 3 "~" H 2300 5150 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 6 positions" H 0   0   50  0001 C CNN "Description"
F 5 "Samtec" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-106-01-F-D-K" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 0   0   50  0001 C CNN "Supplier"
F 9 "IPL1-106-01-F-D-K" H 0   0   50  0001 C CNN "SupplierRef"
	1    2300 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J4
U 1 1 5D7E449A
P 2300 6150
F 0 "J4" H 2350 6467 50  0000 C CNN
F 1 "Encoder" H 2350 6376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 2300 6150 50  0001 C CNN
F 3 "~" H 2300 6150 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 4 positions" H 0   0   50  0001 C CNN "Description"
F 5 "Samtec" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-104-01-F-D-K" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 0   0   50  0001 C CNN "Supplier"
F 9 "IPL1-104-01-F-D-K" H 0   0   50  0001 C CNN "SupplierRef"
	1    2300 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1200 2600 1200
Wire Wire Line
	3250 1300 2600 1300
Wire Wire Line
	3250 1400 2600 1400
Wire Wire Line
	3250 1500 2600 1500
Wire Wire Line
	3250 1600 2600 1600
Wire Wire Line
	3250 1700 2600 1700
Wire Wire Line
	3250 1800 2600 1800
Wire Wire Line
	2600 1900 3250 1900
Wire Wire Line
	3250 2000 2600 2000
Wire Wire Line
	3250 2100 2600 2100
Wire Wire Line
	3250 2200 2600 2200
Wire Wire Line
	3250 2300 2600 2300
Wire Wire Line
	3250 2400 2600 2400
Text Label 3250 1200 2    50   ~ 0
DIG_IN_CH1
Text Label 3250 1300 2    50   ~ 0
DIG_IN_CH2
Text Label 3250 1400 2    50   ~ 0
DIG_IN_CH3
Text Label 3250 1500 2    50   ~ 0
DIG_IN_CH4
Text Label 3250 1600 2    50   ~ 0
DIG_IN_CH5
Wire Wire Line
	6000 4300 6750 4300
Wire Wire Line
	6000 4500 6750 4500
Text Label 6000 4300 0    50   ~ 0
DIG_IN_CH4
Text Label 6000 4500 0    50   ~ 0
DIG_IN_CH5
Text Label 3250 1700 2    50   ~ 0
DIG_IN_CH6
Text Label 3250 1800 2    50   ~ 0
DIG_IN_CH7
Wire Wire Line
	9200 2000 10000 2000
Wire Wire Line
	9200 2300 10000 2300
Text Label 9200 2000 0    50   ~ 0
DIG_IN_CH6
Text Label 9200 2300 0    50   ~ 0
DIG_IN_CH7
Wire Wire Line
	2100 1900 2000 1900
Wire Wire Line
	2000 1900 2000 1800
Wire Wire Line
	2000 1200 2100 1200
Wire Wire Line
	2100 1300 2000 1300
Connection ~ 2000 1300
Wire Wire Line
	2000 1300 2000 1200
Wire Wire Line
	2100 1400 2000 1400
Connection ~ 2000 1400
Wire Wire Line
	2000 1400 2000 1300
Wire Wire Line
	2100 1500 2000 1500
Connection ~ 2000 1500
Wire Wire Line
	2000 1500 2000 1400
Wire Wire Line
	2100 1600 2000 1600
Connection ~ 2000 1600
Wire Wire Line
	2000 1600 2000 1500
Wire Wire Line
	2100 1700 2000 1700
Connection ~ 2000 1700
Wire Wire Line
	2000 1700 2000 1600
Wire Wire Line
	2100 1800 2000 1800
Connection ~ 2000 1800
Wire Wire Line
	2000 1800 2000 1700
Wire Wire Line
	2100 2000 2000 2000
Wire Wire Line
	2000 2000 2000 2100
Wire Wire Line
	2000 2700 2100 2700
Wire Wire Line
	2100 2600 2000 2600
Connection ~ 2000 2600
Wire Wire Line
	2000 2600 2000 2700
Wire Wire Line
	2100 2500 2000 2500
Connection ~ 2000 2500
Wire Wire Line
	2000 2500 2000 2600
Wire Wire Line
	2100 2400 2000 2400
Connection ~ 2000 2400
Wire Wire Line
	2000 2400 2000 2500
Wire Wire Line
	2100 2300 2000 2300
Connection ~ 2000 2300
Wire Wire Line
	2000 2300 2000 2400
Wire Wire Line
	2100 2200 2000 2200
Connection ~ 2000 2200
Wire Wire Line
	2000 2200 2000 2300
Wire Wire Line
	2100 2100 2000 2100
Connection ~ 2000 2100
Wire Wire Line
	2000 2100 2000 2200
Text Label 3250 1900 2    50   ~ 0
DIG_IN_CH8
Text Label 3250 2000 2    50   ~ 0
DIG_IN_CH9
Text Label 3250 2100 2    50   ~ 0
DIG_IN_CH10
Text Label 3250 2200 2    50   ~ 0
DIG_IN_CH11
Text Label 3250 2300 2    50   ~ 0
DIG_IN_CH12
Text Label 3250 2400 2    50   ~ 0
DIG_IN_CH13
Wire Wire Line
	9200 4600 10000 4600
Wire Wire Line
	9200 4300 10000 4300
Wire Wire Line
	9200 5400 10000 5400
Wire Wire Line
	9200 5300 10000 5300
Wire Wire Line
	9200 5200 10000 5200
Wire Wire Line
	9200 5100 10000 5100
Text Label 9200 5100 0    50   ~ 0
DIG_IN_CH10
Text Label 9200 5200 0    50   ~ 0
DIG_IN_CH11
Text Label 9200 5300 0    50   ~ 0
DIG_IN_CH12
Text Label 9200 5400 0    50   ~ 0
DIG_IN_CH13
Text Label 9200 4300 0    50   ~ 0
DIG_IN_CH8
Text Label 9200 4600 0    50   ~ 0
DIG_IN_CH9
Wire Wire Line
	10000 2100 8650 2100
Wire Wire Line
	8650 2100 8650 2400
Wire Wire Line
	8650 2400 10000 2400
Wire Wire Line
	10000 4700 8650 4700
Wire Wire Line
	8650 4700 8650 4400
Connection ~ 8650 2400
Wire Wire Line
	10000 5500 8650 5500
Wire Wire Line
	8650 5500 8650 4700
Connection ~ 8650 4700
Wire Wire Line
	10000 4400 8650 4400
Connection ~ 8650 4400
Wire Wire Line
	8650 4400 8650 3400
Wire Wire Line
	10000 2200 9850 2200
Wire Wire Line
	9850 2200 9850 1900
Wire Wire Line
	9850 1900 10000 1900
Wire Wire Line
	10000 4500 9850 4500
Wire Wire Line
	9850 4500 9850 4200
Connection ~ 9850 2200
Wire Wire Line
	10000 4200 9850 4200
Connection ~ 9850 4200
Wire Wire Line
	9850 4200 9850 3300
Wire Wire Line
	9200 2600 10000 2600
Wire Wire Line
	9200 3200 10000 3200
Text Label 9200 2600 0    50   ~ 0
DIG_OUT_CH2
Text Label 9200 3200 0    50   ~ 0
DIG_OUT_CH3
Wire Wire Line
	1450 3350 2100 3350
Wire Wire Line
	1450 3450 2100 3450
Text Label 1450 3350 0    50   ~ 0
DIG_OUT_CH2
Text Label 1450 3450 0    50   ~ 0
DIG_OUT_CH3
Wire Wire Line
	2700 4350 2600 4350
Wire Wire Line
	2600 4250 2700 4250
Connection ~ 2700 4250
Wire Wire Line
	2700 4250 2700 4350
Wire Wire Line
	2600 4150 2700 4150
Wire Wire Line
	2700 4150 2700 4250
Wire Wire Line
	1450 3250 2100 3250
Text Label 3150 3250 2    50   ~ 0
RELAY_COM
Text Label 1450 3250 0    50   ~ 0
RELAY_NO
Wire Wire Line
	6000 5700 6750 5700
Text Label 6000 3300 0    50   ~ 0
RELAY_COM
Text Label 6000 5700 0    50   ~ 0
RELAY_NO
Wire Wire Line
	1450 4950 2100 4950
Wire Wire Line
	1450 5050 2100 5050
Wire Wire Line
	1450 5250 2100 5250
Wire Wire Line
	1450 5350 2100 5350
Text Label 1450 5050 0    50   ~ 0
ANA_IN_CH1
Text Label 1450 4950 0    50   ~ 0
ANA_IN_CH2
Text Label 1450 5350 0    50   ~ 0
ANA_OUT_CH1
Text Label 1450 5250 0    50   ~ 0
ANA_OUT_CH2
Wire Wire Line
	2600 5150 2700 5150
Wire Wire Line
	2700 5150 2700 5050
Wire Wire Line
	2700 4950 2600 4950
Wire Wire Line
	2600 5050 2700 5050
Connection ~ 2700 5050
Wire Wire Line
	2700 5050 2700 4950
Wire Wire Line
	2600 5250 2700 5250
Wire Wire Line
	2700 5250 2700 5350
Wire Wire Line
	2700 5450 2600 5450
Wire Wire Line
	2600 5350 2700 5350
Connection ~ 2700 5350
Wire Wire Line
	9200 3000 10000 3000
Text Label 9200 3000 0    50   ~ 0
ANA_IN_CH1
Wire Wire Line
	6000 3500 6750 3500
Text Label 6000 3500 0    50   ~ 0
ANA_IN_CH2
Wire Wire Line
	6000 3700 6750 3700
Wire Wire Line
	9200 2900 10000 2900
Text Label 6000 3700 0    50   ~ 0
ANA_OUT_CH1
Text Label 9200 2900 0    50   ~ 0
ANA_OUT_CH2
Wire Wire Line
	1450 6050 2100 6050
Wire Wire Line
	1450 6150 2100 6150
Wire Wire Line
	1450 6250 2100 6250
Wire Wire Line
	1450 6350 2100 6350
Text Label 1450 6150 0    50   ~ 0
ENCODER_A
Text Label 1450 6350 0    50   ~ 0
ENCODER_B
Text Label 1450 6050 0    50   ~ 0
~ENCODER_A
Text Label 1450 6250 0    50   ~ 0
~ENCODER_B
Wire Wire Line
	9200 3500 10000 3500
Wire Wire Line
	9200 3600 10000 3600
Wire Wire Line
	9200 3800 10000 3800
Wire Wire Line
	9200 3700 10000 3700
Text Label 9200 3700 0    50   ~ 0
ENCODER_A
Text Label 9200 3800 0    50   ~ 0
~ENCODER_A
Text Label 9200 3500 0    50   ~ 0
ENCODER_B
Text Label 9200 3600 0    50   ~ 0
~ENCODER_B
Wire Wire Line
	2600 6250 2700 6250
Wire Wire Line
	2700 6250 2700 6350
Wire Wire Line
	2700 6350 2600 6350
Wire Wire Line
	2600 6150 2700 6150
Wire Wire Line
	2700 6150 2700 6050
Wire Wire Line
	2700 6050 2600 6050
$Comp
L power:GND #PWR02
U 1 1 5D9742A4
P 2000 2750
F 0 "#PWR02" H 2000 2500 50  0001 C CNN
F 1 "GND" H 2005 2577 50  0000 C CNN
F 2 "" H 2000 2750 50  0001 C CNN
F 3 "" H 2000 2750 50  0001 C CNN
	1    2000 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5D974E53
P 9850 5600
F 0 "#PWR015" H 9850 5350 50  0001 C CNN
F 1 "GND" H 9855 5427 50  0000 C CNN
F 2 "" H 9850 5600 50  0001 C CNN
F 3 "" H 9850 5600 50  0001 C CNN
	1    9850 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 4500 9850 5600
Connection ~ 9850 4500
Wire Wire Line
	2000 2750 2000 2700
Connection ~ 2000 2700
$Comp
L power:GND #PWR05
U 1 1 5D985693
P 2700 5500
F 0 "#PWR05" H 2700 5250 50  0001 C CNN
F 1 "GND" H 2705 5327 50  0000 C CNN
F 2 "" H 2700 5500 50  0001 C CNN
F 3 "" H 2700 5500 50  0001 C CNN
	1    2700 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5D985C81
P 2700 6400
F 0 "#PWR07" H 2700 6150 50  0001 C CNN
F 1 "GND" H 2705 6227 50  0000 C CNN
F 2 "" H 2700 6400 50  0001 C CNN
F 3 "" H 2700 6400 50  0001 C CNN
	1    2700 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 5350 2700 5450
Connection ~ 2700 5450
Wire Wire Line
	2700 5450 2700 5500
Wire Wire Line
	2700 6350 2700 6400
Connection ~ 2700 6350
Wire Wire Line
	10000 3300 9850 3300
Connection ~ 9850 3300
Wire Wire Line
	9850 3300 9850 3100
$Comp
L power:GND #PWR03
U 1 1 5D9EF66C
P 2700 4400
F 0 "#PWR03" H 2700 4150 50  0001 C CNN
F 1 "GND" H 2705 4227 50  0000 C CNN
F 2 "" H 2700 4400 50  0001 C CNN
F 3 "" H 2700 4400 50  0001 C CNN
	1    2700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4400 2700 4350
Connection ~ 2700 4350
Wire Wire Line
	10000 3100 9850 3100
Connection ~ 9850 3100
Wire Wire Line
	9850 3100 9850 2700
Wire Wire Line
	10000 2500 9850 2500
Connection ~ 9850 2500
Wire Wire Line
	9850 2500 9850 2200
Wire Wire Line
	10000 3400 8650 3400
Connection ~ 8650 3400
Wire Wire Line
	8650 3400 8650 2800
Wire Wire Line
	10000 2800 8650 2800
Connection ~ 8650 2800
Wire Wire Line
	8650 2800 8650 2400
Wire Wire Line
	10000 2700 9850 2700
Connection ~ 9850 2700
Wire Wire Line
	9850 2700 9850 2500
NoConn ~ 2600 2500
NoConn ~ 2600 2600
NoConn ~ 2600 2700
NoConn ~ 2100 3550
NoConn ~ 2100 3650
NoConn ~ 2100 3750
NoConn ~ 2100 3850
NoConn ~ 2100 3950
NoConn ~ 2100 5150
NoConn ~ 2100 5450
NoConn ~ 6700 1650
NoConn ~ 6700 1450
NoConn ~ 6750 3400
NoConn ~ 6750 3600
NoConn ~ 6750 3800
NoConn ~ 6750 4000
NoConn ~ 6750 5600
NoConn ~ 6750 5500
NoConn ~ 6750 5400
NoConn ~ 6750 5300
NoConn ~ 6750 5200
NoConn ~ 6750 5100
NoConn ~ 6750 5000
NoConn ~ 6750 4900
NoConn ~ 6750 4800
NoConn ~ 6750 4700
NoConn ~ 6750 4600
NoConn ~ 6750 4400
NoConn ~ 6750 4200
NoConn ~ 10000 5000
NoConn ~ 10000 4900
NoConn ~ 10000 4800
NoConn ~ 10000 4100
NoConn ~ 10000 4000
NoConn ~ 10000 3900
$Comp
L power:GND #PWR012
U 1 1 5DB53826
P 7000 2350
F 0 "#PWR012" H 7000 2100 50  0001 C CNN
F 1 "GND" H 7005 2177 50  0000 C CNN
F 2 "" H 7000 2350 50  0001 C CNN
F 3 "" H 7000 2350 50  0001 C CNN
	1    7000 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5DB53AF2
P 7050 5900
F 0 "#PWR013" H 7050 5650 50  0001 C CNN
F 1 "GND" H 7055 5727 50  0000 C CNN
F 2 "" H 7050 5900 50  0001 C CNN
F 3 "" H 7050 5900 50  0001 C CNN
	1    7050 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5DB54251
P 10300 5700
F 0 "#PWR016" H 10300 5450 50  0001 C CNN
F 1 "GND" H 10305 5527 50  0000 C CNN
F 2 "" H 10300 5700 50  0001 C CNN
F 3 "" H 10300 5700 50  0001 C CNN
	1    10300 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1350 6700 1350
Text Label 5400 2200 2    50   ~ 0
24V_outputs
Text Label 5400 2100 2    50   ~ 0
24V_main
$Comp
L power:+24V #PWR01
U 1 1 5DBA28E0
P 2000 1100
F 0 "#PWR01" H 2000 950 50  0001 C CNN
F 1 "+24V" H 2015 1273 50  0000 C CNN
F 2 "" H 2000 1100 50  0001 C CNN
F 3 "" H 2000 1100 50  0001 C CNN
	1    2000 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR04
U 1 1 5DBA2F68
P 2700 4900
F 0 "#PWR04" H 2700 4750 50  0001 C CNN
F 1 "+24V" H 2715 5073 50  0000 C CNN
F 2 "" H 2700 4900 50  0001 C CNN
F 3 "" H 2700 4900 50  0001 C CNN
	1    2700 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR011
U 1 1 5DBA3B8F
P 6550 4100
F 0 "#PWR011" H 6550 3950 50  0001 C CNN
F 1 "+24V" H 6565 4273 50  0000 C CNN
F 2 "" H 6550 4100 50  0001 C CNN
F 3 "" H 6550 4100 50  0001 C CNN
	1    6550 4100
	0    -1   -1   0   
$EndComp
$Comp
L power:+24V #PWR014
U 1 1 5DBA43C3
P 8650 2000
F 0 "#PWR014" H 8650 1850 50  0001 C CNN
F 1 "+24V" H 8665 2173 50  0000 C CNN
F 2 "" H 8650 2000 50  0001 C CNN
F 3 "" H 8650 2000 50  0001 C CNN
	1    8650 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1100 2000 1200
Connection ~ 2000 1200
Wire Wire Line
	2700 4900 2700 4950
Connection ~ 2700 4950
Wire Wire Line
	8650 2000 8650 2100
Connection ~ 8650 2100
Wire Wire Line
	6550 4100 6750 4100
$Comp
L power:+24V #PWR06
U 1 1 5D81CFE0
P 2700 6000
F 0 "#PWR06" H 2700 5850 50  0001 C CNN
F 1 "+24V" H 2715 6173 50  0000 C CNN
F 2 "" H 2700 6000 50  0001 C CNN
F 3 "" H 2700 6000 50  0001 C CNN
	1    2700 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 6000 2700 6050
Connection ~ 2700 6050
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5D8082B3
P 4650 2950
F 0 "J6" H 4730 2942 50  0000 L CNN
F 1 "HMI Power" H 4730 2851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4650 2950 50  0001 C CNN
F 3 "~" H 4650 2950 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, single row, 2 positions" H 0   0   50  0001 C CNN "Description"
F 5 "Samtec" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-102-01-F-S-K" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 0   0   50  0001 C CNN "Supplier"
F 9 "IPL1-102-01-F-S-K" H 0   0   50  0001 C CNN "SupplierRef"
	1    4650 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5D808E39
P 4350 3100
F 0 "#PWR09" H 4350 2850 50  0001 C CNN
F 1 "GND" H 4355 2927 50  0000 C CNN
F 2 "" H 4350 3100 50  0001 C CNN
F 3 "" H 4350 3100 50  0001 C CNN
	1    4350 3100
	1    0    0    -1  
$EndComp
Text Label 4000 2950 0    50   ~ 0
24V_main
Wire Wire Line
	4000 2950 4450 2950
Wire Wire Line
	4350 3100 4350 3050
Wire Wire Line
	4350 3050 4450 3050
$Comp
L power:GND #PWR0101
U 1 1 5DF9FDE9
P 6500 3900
F 0 "#PWR0101" H 6500 3650 50  0001 C CNN
F 1 "GND" V 6505 3772 50  0000 R CNN
F 2 "" H 6500 3900 50  0001 C CNN
F 3 "" H 6500 3900 50  0001 C CNN
	1    6500 3900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5DFE7938
P 5800 3400
F 0 "#PWR0102" H 5800 3150 50  0001 C CNN
F 1 "GND" H 5805 3227 50  0000 C CNN
F 2 "" H 5800 3400 50  0001 C CNN
F 3 "" H 5800 3400 50  0001 C CNN
	1    5800 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3400 5800 3300
Wire Wire Line
	5800 3300 6750 3300
Wire Wire Line
	6500 3900 6750 3900
$Comp
L power:GND #PWR0103
U 1 1 5E312B53
P 4300 2300
F 0 "#PWR0103" H 4300 2050 50  0001 C CNN
F 1 "GND" H 4305 2127 50  0000 C CNN
F 2 "" H 4300 2300 50  0001 C CNN
F 3 "" H 4300 2300 50  0001 C CNN
	1    4300 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Counter_Clockwise J5
U 1 1 5E327041
P 4650 2100
F 0 "J5" H 4700 2317 50  0000 C CNN
F 1 "Main board power" H 4700 2226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 4650 2100 50  0001 C CNN
F 3 "~" H 4650 2100 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 2 positions" H 100 0   50  0001 C CNN "Description"
F 5 "Samtec" H 100 0   50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-102-01-F-D-K" H 100 0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 100 0   50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 100 0   50  0001 C CNN "Supplier"
F 9 "IPL1-102-01-F-D-K" H 100 0   50  0001 C CNN "SupplierRef"
	1    4650 2100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5400 2100 4850 2100
Wire Wire Line
	5400 2200 4850 2200
Wire Wire Line
	4350 2100 4300 2100
Wire Wire Line
	4300 2100 4300 2200
Wire Wire Line
	4350 2200 4300 2200
Connection ~ 4300 2200
Wire Wire Line
	4300 2200 4300 2300
$Comp
L power:GND #PWR0104
U 1 1 5E3634F1
P 2000 4400
F 0 "#PWR0104" H 2000 4150 50  0001 C CNN
F 1 "GND" H 2005 4227 50  0000 C CNN
F 2 "" H 2000 4400 50  0001 C CNN
F 3 "" H 2000 4400 50  0001 C CNN
	1    2000 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4400 2000 4350
Wire Wire Line
	2000 4050 2100 4050
Wire Wire Line
	2100 4150 2000 4150
Connection ~ 2000 4150
Wire Wire Line
	2000 4150 2000 4050
Wire Wire Line
	2100 4250 2000 4250
Connection ~ 2000 4250
Wire Wire Line
	2000 4250 2000 4150
Wire Wire Line
	2100 4350 2000 4350
Connection ~ 2000 4350
Wire Wire Line
	2000 4350 2000 4250
Wire Wire Line
	3150 3250 2600 3250
NoConn ~ 2600 3350
NoConn ~ 2600 3450
NoConn ~ 2600 3550
NoConn ~ 2600 3650
NoConn ~ 2600 3750
NoConn ~ 2600 3850
NoConn ~ 2600 3950
NoConn ~ 2600 4050
Text Notes 1950 3550 2    50   ~ 0
DIG_OUT_CH4
Text Notes 1950 3650 2    50   ~ 0
DIG_OUT_CH5
Text Notes 1950 3750 2    50   ~ 0
DIG_OUT_CH6
Text Notes 1950 3850 2    50   ~ 0
DIG_OUT_CH7
Text Notes 1950 3950 2    50   ~ 0
DIG_OUT_CH8
Text Notes 2750 3350 0    50   ~ 0
DIG_OUT_CH9
Text Notes 2750 3450 0    50   ~ 0
DIG_OUT_CH10
Text Notes 2750 3550 0    50   ~ 0
DIG_OUT_CH11
Text Notes 2750 3650 0    50   ~ 0
DIG_OUT_CH12
Text Notes 2750 3750 0    50   ~ 0
DIG_OUT_CH13
Text Notes 2750 3850 0    50   ~ 0
DIG_OUT_CH14
Text Notes 2750 3950 0    50   ~ 0
DIG_OUT_CH15
Text Notes 2750 4050 0    50   ~ 0
DIG_OUT_CH16
$EndSCHEMATC
