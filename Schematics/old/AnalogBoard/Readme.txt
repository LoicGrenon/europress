Pour utiliser GerberPanelizer (https://github.com/ThisIsNotRocketScience/GerberTools) :
- Cocher "Use Protel filename extensions" (Plot -> General Options)
- Cocher "Use auxiliary axis as origin" (Plot -> Gerber Options
- Cocher "PTH and NPTH in single file" (Plot->Generate Drill Files->Drill File Format)
- Définir l'axe auxiliaire dans Pcbnew
- Changer l'extension du fichier Edge_Cuts.gm1 en *.gko
