EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "EuroPress - Analog board"
Date ""
Rev ""
Comp "LGR"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L EuroPress:AD7610BSTZ U2
U 1 1 5D52D1CE
P 6250 2500
F 0 "U2" H 7900 3150 50  0000 L CNN
F 1 "AD7610BSTZ" H 7900 3050 50  0000 L CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 7900 3100 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ad7610.pdf" H 7900 3000 50  0001 L CNN
F 4 "AD7610BSTZ, 16 bit ADC Parallel, SPI, Microwire, 48-Pin LQFP" H 7900 2900 50  0001 L CNN "Description"
F 5 "584-AD7610BSTZ" H 7900 2700 50  0001 L CNN "Mouser Part Number"
F 6 "Analog Devices" H 7900 2600 50  0001 L CNN "Manufacturer_Name"
F 7 "AD7610BSTZ" H 7900 2500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "Yes" H 0   0   50  0001 C CNN "Sample"
F 9 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 10 "584-AD7610BSTZ" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    6250 2500
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:TS922AIDT U1
U 1 1 5D530209
P 4000 6200
F 0 "U1" H 4000 6565 50  0000 C CNN
F 1 "TS922AIDT" H 4000 6474 50  0000 C CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 4150 6250 50  0001 L CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 4150 6150 50  0001 L CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 4150 6050 50  0001 L CNN "Description"
F 5 "1.75" H 4150 5950 50  0001 L CNN "Height"
F 6 "511-TS922AIDT" H 4150 5850 50  0001 L CNN "Mouser Part Number"
F 7 "STMicroelectronics" H 4150 5750 50  0001 L CNN "Manufacturer_Name"
F 8 "TS922AIDT" H 4150 5650 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 10 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    4000 6200
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:TS922AIDT U1
U 2 1 5D531748
P 4000 4400
F 0 "U1" H 4000 4765 50  0000 C CNN
F 1 "TS922AIDT" H 4000 4674 50  0000 C CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 4150 4450 50  0001 L CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 4150 4350 50  0001 L CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 4150 4250 50  0001 L CNN "Description"
F 5 "1.75" H 4150 4150 50  0001 L CNN "Height"
F 6 "511-TS922AIDT" H 4150 4050 50  0001 L CNN "Mouser Part Number"
F 7 "STMicroelectronics" H 4150 3950 50  0001 L CNN "Manufacturer_Name"
F 8 "TS922AIDT" H 4150 3850 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 10 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	2    4000 4400
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:TS922AIDT U1
U 3 1 5D532569
P 2350 2700
F 0 "U1" H 2398 2746 50  0000 L CNN
F 1 "TS922AIDT" H 2398 2655 50  0000 L CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 2500 2750 50  0001 L CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 2500 2650 50  0001 L CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 2500 2550 50  0001 L CNN "Description"
F 5 "1.75" H 2500 2450 50  0001 L CNN "Height"
F 6 "511-TS922AIDT" H 2500 2350 50  0001 L CNN "Mouser Part Number"
F 7 "STMicroelectronics" H 2500 2250 50  0001 L CNN "Manufacturer_Name"
F 8 "TS922AIDT" H 2500 2150 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 10 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	3    2350 2700
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:LT1490A U3
U 1 1 5D533230
P 12200 2550
F 0 "U3" H 12200 2917 50  0000 C CNN
F 1 "TS922AIDT" H 12200 2826 50  0000 C CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 12200 2550 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 12200 2550 50  0001 C CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TS922AIDT" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "511-TS922AIDT" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    12200 2550
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:LT1490A U3
U 2 1 5D533B33
P 13100 2650
F 0 "U3" H 13100 3017 50  0000 C CNN
F 1 "TS922AIDT" H 13100 2926 50  0000 C CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 13100 2650 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 13100 2650 50  0001 C CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TS922AIDT" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "511-TS922AIDT" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	2    13100 2650
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:LT1490A U3
U 3 1 5D5343C8
P 14100 4000
F 0 "U3" H 14058 4046 50  0000 L CNN
F 1 "TS922AIDT" H 14058 3955 50  0000 L CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 14100 4000 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 14100 4000 50  0001 C CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TS922AIDT" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "511-TS922AIDT" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	3    14100 4000
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:LT1490A U4
U 3 1 5D536753
P 15100 4000
F 0 "U4" H 15058 4046 50  0000 L CNN
F 1 "TS922AIDT" H 15058 3955 50  0000 L CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 15100 4000 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 15100 4000 50  0001 C CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TS922AIDT" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "511-TS922AIDT" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	3    15100 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR041
U 1 1 5D53D51F
P 7550 1500
F 0 "#PWR041" H 7550 1350 50  0001 C CNN
F 1 "+3V3" H 7565 1673 50  0000 C CNN
F 2 "" H 7550 1500 50  0001 C CNN
F 3 "" H 7550 1500 50  0001 C CNN
	1    7550 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR018
U 1 1 5D53E1A5
P 4550 1100
F 0 "#PWR018" H 4550 950 50  0001 C CNN
F 1 "+3V3" H 4565 1273 50  0000 C CNN
F 2 "" H 4550 1100 50  0001 C CNN
F 3 "" H 4550 1100 50  0001 C CNN
	1    4550 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR036
U 1 1 5D53F972
P 6650 1500
F 0 "#PWR036" H 6650 1350 50  0001 C CNN
F 1 "+12VA" H 6665 1673 50  0000 C CNN
F 2 "" H 6650 1500 50  0001 C CNN
F 3 "" H 6650 1500 50  0001 C CNN
	1    6650 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR01
U 1 1 5D540375
P 1700 1100
F 0 "#PWR01" H 1700 950 50  0001 C CNN
F 1 "+12VA" H 1715 1273 50  0000 C CNN
F 2 "" H 1700 1100 50  0001 C CNN
F 3 "" H 1700 1100 50  0001 C CNN
	1    1700 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR06
U 1 1 5D540FBA
P 2350 2300
F 0 "#PWR06" H 2350 2150 50  0001 C CNN
F 1 "+12VA" H 2365 2473 50  0000 C CNN
F 2 "" H 2350 2300 50  0001 C CNN
F 3 "" H 2350 2300 50  0001 C CNN
	1    2350 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR09
U 1 1 5D541725
P 2700 1100
F 0 "#PWR09" H 2700 950 50  0001 C CNN
F 1 "+5V" H 2715 1273 50  0000 C CNN
F 2 "" H 2700 1100 50  0001 C CNN
F 3 "" H 2700 1100 50  0001 C CNN
	1    2700 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR038
U 1 1 5D541F1B
P 6950 1500
F 0 "#PWR038" H 6950 1350 50  0001 C CNN
F 1 "+5VA" H 6965 1673 50  0000 C CNN
F 2 "" H 6950 1500 50  0001 C CNN
F 3 "" H 6950 1500 50  0001 C CNN
	1    6950 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VD #PWR040
U 1 1 5D542AE7
P 7250 1500
F 0 "#PWR040" H 7250 1350 50  0001 C CNN
F 1 "+5VD" H 7265 1673 50  0000 C CNN
F 2 "" H 7250 1500 50  0001 C CNN
F 3 "" H 7250 1500 50  0001 C CNN
	1    7250 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+5VA #PWR011
U 1 1 5D543A7A
P 3100 1100
F 0 "#PWR011" H 3100 950 50  0001 C CNN
F 1 "+5VA" H 3115 1273 50  0000 C CNN
F 2 "" H 3100 1100 50  0001 C CNN
F 3 "" H 3100 1100 50  0001 C CNN
	1    3100 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5D5442EA
P 5650 5350
F 0 "#PWR026" H 5650 5100 50  0001 C CNN
F 1 "GND" H 5655 5177 50  0000 C CNN
F 2 "" H 5650 5350 50  0001 C CNN
F 3 "" H 5650 5350 50  0001 C CNN
	1    5650 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5D544E68
P 6100 5350
F 0 "#PWR027" H 6100 5100 50  0001 C CNN
F 1 "GND" H 6105 5177 50  0000 C CNN
F 2 "" H 6100 5350 50  0001 C CNN
F 3 "" H 6100 5350 50  0001 C CNN
	1    6100 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5D54564F
P 2450 4600
F 0 "#PWR08" H 2450 4350 50  0001 C CNN
F 1 "GND" H 2455 4427 50  0000 C CNN
F 2 "" H 2450 4600 50  0001 C CNN
F 3 "" H 2450 4600 50  0001 C CNN
	1    2450 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5D5459F5
P 2350 3100
F 0 "#PWR07" H 2350 2850 50  0001 C CNN
F 1 "GND" H 2355 2927 50  0000 C CNN
F 2 "" H 2350 3100 50  0001 C CNN
F 3 "" H 2350 3100 50  0001 C CNN
	1    2350 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5D545E68
P 2700 1450
F 0 "#PWR010" H 2700 1200 50  0001 C CNN
F 1 "GND" H 2705 1277 50  0000 C CNN
F 2 "" H 2700 1450 50  0001 C CNN
F 3 "" H 2700 1450 50  0001 C CNN
	1    2700 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5D5465F7
P 3100 1450
F 0 "#PWR012" H 3100 1200 50  0001 C CNN
F 1 "GND" H 3105 1277 50  0000 C CNN
F 2 "" H 3100 1450 50  0001 C CNN
F 3 "" H 3100 1450 50  0001 C CNN
	1    3100 1450
	1    0    0    -1  
$EndComp
$Comp
L power:-12VA #PWR037
U 1 1 5D54688E
P 6650 5200
F 0 "#PWR037" H 6650 5050 50  0001 C CNN
F 1 "-12VA" H 6665 5373 50  0000 C CNN
F 2 "" H 6650 5200 50  0001 C CNN
F 3 "" H 6650 5200 50  0001 C CNN
	1    6650 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6650 1700 6650 1500
Wire Wire Line
	6950 1500 6950 1600
Wire Wire Line
	7050 1700 7050 1600
Wire Wire Line
	7050 1600 6950 1600
Connection ~ 6950 1600
Wire Wire Line
	6950 1600 6950 1700
Wire Wire Line
	7250 1500 7250 1700
Wire Wire Line
	7550 1700 7550 1500
$Comp
L power:GND #PWR028
U 1 1 5D54BE62
P 6150 2900
F 0 "#PWR028" H 6150 2650 50  0001 C CNN
F 1 "GND" V 6155 2772 50  0000 R CNN
F 2 "" H 6150 2900 50  0001 C CNN
F 3 "" H 6150 2900 50  0001 C CNN
	1    6150 2900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5D54C84E
P 6150 3100
F 0 "#PWR030" H 6150 2850 50  0001 C CNN
F 1 "GND" V 6155 2972 50  0000 R CNN
F 2 "" H 6150 3100 50  0001 C CNN
F 3 "" H 6150 3100 50  0001 C CNN
	1    6150 3100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5D54C9F7
P 6150 3400
F 0 "#PWR032" H 6150 3150 50  0001 C CNN
F 1 "GND" V 6155 3272 50  0000 R CNN
F 2 "" H 6150 3400 50  0001 C CNN
F 3 "" H 6150 3400 50  0001 C CNN
	1    6150 3400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5D54CCAB
P 6150 3700
F 0 "#PWR033" H 6150 3450 50  0001 C CNN
F 1 "GND" V 6155 3572 50  0000 R CNN
F 2 "" H 6150 3700 50  0001 C CNN
F 3 "" H 6150 3700 50  0001 C CNN
	1    6150 3700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5D54CF4F
P 6150 3950
F 0 "#PWR034" H 6150 3700 50  0001 C CNN
F 1 "GND" V 6155 3822 50  0000 R CNN
F 2 "" H 6150 3950 50  0001 C CNN
F 3 "" H 6150 3950 50  0001 C CNN
	1    6150 3950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5D54D161
P 6150 4550
F 0 "#PWR035" H 6150 4300 50  0001 C CNN
F 1 "GND" H 6155 4377 50  0000 C CNN
F 2 "" H 6150 4550 50  0001 C CNN
F 3 "" H 6150 4550 50  0001 C CNN
	1    6150 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2900 6250 2900
Wire Wire Line
	6150 3100 6250 3100
Wire Wire Line
	6250 3400 6150 3400
Wire Wire Line
	6250 3700 6150 3700
Wire Wire Line
	6200 3900 6200 3950
Wire Wire Line
	6200 4000 6250 4000
Wire Wire Line
	6200 3900 6250 3900
Wire Wire Line
	6150 3950 6200 3950
Connection ~ 6200 3950
Wire Wire Line
	6200 3950 6200 4000
Wire Wire Line
	6250 4500 6150 4500
Wire Wire Line
	6150 4500 6150 4550
$Comp
L power:+3V3 #PWR029
U 1 1 5D54FC03
P 6150 3000
F 0 "#PWR029" H 6150 2850 50  0001 C CNN
F 1 "+3V3" V 6165 3128 50  0000 L CNN
F 2 "" H 6150 3000 50  0001 C CNN
F 3 "" H 6150 3000 50  0001 C CNN
	1    6150 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR031
U 1 1 5D550A16
P 6150 3300
F 0 "#PWR031" H 6150 3150 50  0001 C CNN
F 1 "+3V3" V 6165 3428 50  0000 L CNN
F 2 "" H 6150 3300 50  0001 C CNN
F 3 "" H 6150 3300 50  0001 C CNN
	1    6150 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6150 3000 6250 3000
Wire Wire Line
	6150 3300 6250 3300
Wire Wire Line
	6250 4200 5650 4200
Wire Wire Line
	5650 3600 6250 3600
Wire Wire Line
	8050 4000 8550 4000
Wire Wire Line
	8550 3700 8050 3700
Wire Wire Line
	8050 3600 8550 3600
Wire Wire Line
	8550 3500 8050 3500
Wire Wire Line
	8050 3400 8550 3400
Wire Wire Line
	8050 3300 8550 3300
Wire Wire Line
	8050 3200 8550 3200
Wire Wire Line
	8550 3100 8050 3100
Wire Wire Line
	8050 3000 8550 3000
Wire Wire Line
	8550 2900 8050 2900
Wire Wire Line
	8050 2800 8550 2800
Wire Wire Line
	8550 2700 8050 2700
Wire Wire Line
	8050 2600 8550 2600
Wire Wire Line
	8550 2500 8050 2500
Wire Wire Line
	8050 2400 8550 2400
Wire Wire Line
	8550 2300 8050 2300
Wire Wire Line
	8050 2200 8550 2200
Wire Wire Line
	4300 4400 4350 4400
Wire Wire Line
	3700 4500 3650 4500
Wire Wire Line
	3650 4500 3650 4700
Wire Wire Line
	3650 4700 4350 4700
Wire Wire Line
	4350 4700 4350 4400
Connection ~ 4350 4400
Wire Wire Line
	4350 4400 4450 4400
$Comp
L Device:R_Small R5
U 1 1 5D56CE26
P 3050 4300
F 0 "R5" V 2854 4300 50  0000 C CNN
F 1 "19.1k" V 2945 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3050 4300 50  0001 C CNN
F 3 "~" H 3050 4300 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 19.1Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB1912V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB1912V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB1912V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3050 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5D56DB5E
P 2750 4300
F 0 "R3" V 2554 4300 50  0000 C CNN
F 1 "1.62k" V 2645 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2750 4300 50  0001 C CNN
F 3 "~" H 2750 4300 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 1.62Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB1621V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB1621V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB1621V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2750 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 4300 2900 4300
Wire Wire Line
	3150 4300 3250 4300
$Comp
L Device:C_Small C9
U 1 1 5D573FF1
P 3650 3850
F 0 "C9" V 3421 3850 50  0000 C CNN
F 1 "150nF" V 3512 3850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3650 3850 50  0001 C CNN
F 3 "~" H 3650 3850 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 150nF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X154K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X154K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X154K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3650 3850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5D574C10
P 3250 4450
F 0 "C6" H 3158 4404 50  0000 R CNN
F 1 "22nF" H 3158 4495 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3250 4450 50  0001 C CNN
F 3 "~" H 3250 4450 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 22nF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603C223K3RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603C223K3R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603C223K3R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3250 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C13
U 1 1 5D5753C6
P 4550 4550
F 0 "C13" H 4458 4504 50  0000 R CNN
F 1 "2.2nF" H 4458 4595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4550 4550 50  0001 C CNN
F 3 "~" H 4550 4550 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 2.2nF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603S222K3RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603S222K3R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603S222K3R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    4550 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3250 4350 3250 4300
Connection ~ 3250 4300
Wire Wire Line
	3250 4300 3700 4300
Wire Wire Line
	3250 4550 3250 4600
$Comp
L power:GND #PWR013
U 1 1 5D5453AD
P 3250 4600
F 0 "#PWR013" H 3250 4350 50  0001 C CNN
F 1 "GND" H 3255 4427 50  0000 C CNN
F 2 "" H 3250 4600 50  0001 C CNN
F 3 "" H 3250 4600 50  0001 C CNN
	1    3250 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4300 2900 3850
Wire Wire Line
	2900 3850 3550 3850
Connection ~ 2900 4300
Wire Wire Line
	2900 4300 2950 4300
Wire Wire Line
	3750 3850 4450 3850
Wire Wire Line
	4450 3850 4450 4400
Connection ~ 4450 4400
Wire Wire Line
	4450 4400 4550 4400
$Comp
L power:GND #PWR020
U 1 1 5D57FF1E
P 4550 4700
F 0 "#PWR020" H 4550 4450 50  0001 C CNN
F 1 "GND" H 4555 4527 50  0000 C CNN
F 2 "" H 4550 4700 50  0001 C CNN
F 3 "" H 4550 4700 50  0001 C CNN
	1    4550 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4700 4550 4650
Wire Wire Line
	4550 4450 4550 4400
Connection ~ 4550 4400
Wire Wire Line
	4550 4400 6250 4400
$Comp
L Device:D_Zener_Small D2
U 1 1 5D58418D
P 2450 4450
F 0 "D2" V 2404 4518 50  0000 L CNN
F 1 "10V" V 2495 4518 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" V 2450 4450 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30410-30695.pdf" V 2450 4450 50  0001 C CNN
F 4 "Diode Zener 500MW 10V LF" H 0   0   50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "DDZ9697-7" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-DDZ9697-7" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "621-DDZ9697-7" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2450 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 4350 2450 4300
Connection ~ 2450 4300
Wire Wire Line
	2450 4300 2650 4300
Wire Wire Line
	2450 4550 2450 4600
$Comp
L Device:C_Small C19
U 1 1 5D5928E2
P 6100 5200
F 0 "C19" H 6008 5154 50  0000 R CNN
F 1 "0.1uF" H 6008 5245 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6100 5200 50  0001 C CNN
F 3 "~" H 6100 5200 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    6100 5200
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C18
U 1 1 5D59304F
P 5650 5200
F 0 "C18" H 5558 5154 50  0000 R CNN
F 1 "10uF" H 5558 5245 50  0000 R CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 5650 5200 50  0001 C CNN
F 3 "~" H 5650 5200 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 10uF X7R 1210 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C1210X106K3RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C1210X106K3R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C1210X106K3R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    5650 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6650 5000 6650 5050
Wire Wire Line
	6650 5050 6100 5050
Wire Wire Line
	5650 5050 5650 5100
Connection ~ 6650 5050
Wire Wire Line
	6650 5050 6650 5200
Wire Wire Line
	6100 5100 6100 5050
Connection ~ 6100 5050
Wire Wire Line
	6100 5050 5650 5050
Wire Wire Line
	6100 5350 6100 5300
Wire Wire Line
	5650 5350 5650 5300
$Comp
L power:GND #PWR039
U 1 1 5D5AD79B
P 7150 5100
F 0 "#PWR039" H 7150 4850 50  0001 C CNN
F 1 "GND" H 7155 4927 50  0000 C CNN
F 2 "" H 7150 5100 50  0001 C CNN
F 3 "" H 7150 5100 50  0001 C CNN
	1    7150 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 5000 6850 5050
Wire Wire Line
	6850 5050 6950 5050
Wire Wire Line
	7650 5050 7650 5000
Wire Wire Line
	7550 5000 7550 5050
Connection ~ 7550 5050
Wire Wire Line
	7550 5050 7650 5050
Wire Wire Line
	7450 5000 7450 5050
Connection ~ 7450 5050
Wire Wire Line
	7450 5050 7550 5050
Wire Wire Line
	7250 5000 7250 5050
Connection ~ 7250 5050
Wire Wire Line
	7250 5050 7450 5050
Wire Wire Line
	7050 5000 7050 5050
Connection ~ 7050 5050
Wire Wire Line
	7050 5050 7150 5050
Wire Wire Line
	6950 5000 6950 5050
Connection ~ 6950 5050
Wire Wire Line
	6950 5050 7050 5050
Wire Wire Line
	7150 5100 7150 5050
Connection ~ 7150 5050
Wire Wire Line
	7150 5050 7250 5050
$Comp
L Device:C_Small C17
U 1 1 5D5C10F3
P 5650 2600
F 0 "C17" H 5558 2554 50  0000 R CNN
F 1 "0.1uF" H 5558 2645 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5650 2600 50  0001 C CNN
F 3 "~" H 5650 2600 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    5650 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C16
U 1 1 5D5C17E1
P 5400 2250
F 0 "C16" H 5308 2204 50  0000 R CNN
F 1 "22uF" H 5308 2295 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5400 2250 50  0001 C CNN
F 3 "~" H 5400 2250 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 1206 10Vdc 22uF X5R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "AVX" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "1206ZD226KAT2A" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "581-1206ZD226KAT2A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "581-1206ZD226KAT2A" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    5400 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6250 2600 6150 2600
Wire Wire Line
	6150 2600 6150 2700
Wire Wire Line
	6150 2700 6250 2700
Wire Wire Line
	6250 2500 5650 2500
Wire Wire Line
	6150 2700 5650 2700
Connection ~ 6150 2700
Wire Wire Line
	6250 2200 6150 2200
Wire Wire Line
	6150 2200 6150 2150
Wire Wire Line
	6150 2150 5400 2150
Wire Wire Line
	5400 2350 6150 2350
Wire Wire Line
	6150 2350 6150 2300
Wire Wire Line
	6150 2300 6250 2300
$Comp
L power:GND #PWR025
U 1 1 5D5D18CC
P 5650 2750
F 0 "#PWR025" H 5650 2500 50  0001 C CNN
F 1 "GND" H 5655 2577 50  0000 C CNN
F 2 "" H 5650 2750 50  0001 C CNN
F 3 "" H 5650 2750 50  0001 C CNN
	1    5650 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5D5D1FC6
P 5400 2400
F 0 "#PWR024" H 5400 2150 50  0001 C CNN
F 1 "GND" H 5405 2227 50  0000 C CNN
F 2 "" H 5400 2400 50  0001 C CNN
F 3 "" H 5400 2400 50  0001 C CNN
	1    5400 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2400 5400 2350
Connection ~ 5400 2350
Wire Wire Line
	5650 2750 5650 2700
Connection ~ 5650 2700
$Comp
L Device:C_Small C2
U 1 1 5D5DC9B2
P 1900 2700
F 0 "C2" H 1808 2654 50  0000 R CNN
F 1 "0.1uF" H 1808 2745 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1900 2700 50  0001 C CNN
F 3 "~" H 1900 2700 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    1900 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	2350 2300 2350 2350
Wire Wire Line
	2350 3100 2350 3050
Wire Wire Line
	1900 2800 1900 3050
Wire Wire Line
	1900 3050 2350 3050
Connection ~ 2350 3050
Wire Wire Line
	2350 3050 2350 3000
Wire Wire Line
	1900 2600 1900 2350
Wire Wire Line
	1900 2350 2350 2350
Connection ~ 2350 2350
Wire Wire Line
	2350 2350 2350 2400
Text Notes 4550 2150 0    50   ~ 0
22uF (X5R, 1206 size) ceramic\n(or 47uF tantalum)
$Comp
L Device:D_Zener_Small D1
U 1 1 5D5FDAC7
P 1850 5950
F 0 "D1" V 1804 6018 50  0000 L CNN
F 1 "10V" V 1895 6018 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" V 1850 5950 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30410-30695.pdf" V 1850 5950 50  0001 C CNN
F 4 "Diode Zener 500MW 10V LF" H 0   0   50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "DDZ9697-7" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-DDZ9697-7" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "621-DDZ9697-7" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    1850 5950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5D5FEC34
P 1850 6100
F 0 "#PWR03" H 1850 5850 50  0001 C CNN
F 1 "GND" H 1855 5927 50  0000 C CNN
F 2 "" H 1850 6100 50  0001 C CNN
F 3 "" H 1850 6100 50  0001 C CNN
	1    1850 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5D5FF2C0
P 2250 5950
F 0 "R1" H 2191 5904 50  0000 R CNN
F 1 "95.3k" H 2191 5995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2250 5950 50  0001 C CNN
F 3 "~" H 2250 5950 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 95.3Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB9532V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB9532V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB9532V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2250 5950
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5D5FF9DC
P 2250 6250
F 0 "R2" H 2191 6204 50  0000 R CNN
F 1 "47k" H 2191 6295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2250 6250 50  0001 C CNN
F 3 "~" H 2250 6250 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 47Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB473V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB473V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB473V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2250 6250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5D5FFC18
P 2250 6400
F 0 "#PWR05" H 2250 6150 50  0001 C CNN
F 1 "GND" H 2255 6227 50  0000 C CNN
F 2 "" H 2250 6400 50  0001 C CNN
F 3 "" H 2250 6400 50  0001 C CNN
	1    2250 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5800 2250 5850
Wire Wire Line
	1850 5850 1850 5800
Wire Wire Line
	1850 5800 2250 5800
Wire Wire Line
	1850 6050 1850 6100
Wire Wire Line
	2250 6050 2250 6100
Wire Wire Line
	2250 6350 2250 6400
$Comp
L Device:R_Small R4
U 1 1 5D619CB8
P 2750 6100
F 0 "R4" V 2554 6100 50  0000 C CNN
F 1 "1.62k" V 2645 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2750 6100 50  0001 C CNN
F 3 "~" H 2750 6100 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 1.62Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB1621V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB1621V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB1621V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2750 6100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5D61A5B6
P 3050 6100
F 0 "R6" V 2854 6100 50  0000 C CNN
F 1 "19.1k" V 2945 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3050 6100 50  0001 C CNN
F 3 "~" H 3050 6100 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 19.1Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB1912V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB1912V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB1912V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3050 6100
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 6100 2250 6100
Connection ~ 2250 6100
Wire Wire Line
	2250 6100 2250 6150
Wire Wire Line
	2850 6100 2900 6100
$Comp
L Device:C_Small C7
U 1 1 5D624BA3
P 3250 6250
F 0 "C7" H 3158 6204 50  0000 R CNN
F 1 "22nF" H 3158 6295 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3250 6250 50  0001 C CNN
F 3 "~" H 3250 6250 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 22nF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603C223K3RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603C223K3R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603C223K3R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3250 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 6100 3250 6100
Wire Wire Line
	3250 6150 3250 6100
Connection ~ 3250 6100
Wire Wire Line
	3250 6100 3700 6100
$Comp
L Device:C_Small C10
U 1 1 5D62F6D9
P 3650 5650
F 0 "C10" V 3421 5650 50  0000 C CNN
F 1 "150nF" V 3512 5650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3650 5650 50  0001 C CNN
F 3 "~" H 3650 5650 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 150nF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X154K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X154K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X154K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3650 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 5650 2900 5650
Wire Wire Line
	2900 5650 2900 6100
Connection ~ 2900 6100
Wire Wire Line
	2900 6100 2950 6100
Wire Wire Line
	3750 5650 4450 5650
Wire Wire Line
	4450 5650 4450 6200
Wire Wire Line
	4450 6200 4350 6200
Wire Wire Line
	3700 6300 3650 6300
Wire Wire Line
	3650 6300 3650 6500
Wire Wire Line
	3650 6500 4350 6500
Wire Wire Line
	4350 6500 4350 6200
Connection ~ 4350 6200
Wire Wire Line
	4350 6200 4300 6200
$Comp
L power:GND #PWR014
U 1 1 5D6416E0
P 3250 6400
F 0 "#PWR014" H 3250 6150 50  0001 C CNN
F 1 "GND" H 3255 6227 50  0000 C CNN
F 2 "" H 3250 6400 50  0001 C CNN
F 3 "" H 3250 6400 50  0001 C CNN
	1    3250 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6400 3250 6350
$Comp
L Device:C_Small C14
U 1 1 5D647C5A
P 4550 6350
F 0 "C14" H 4458 6304 50  0000 R CNN
F 1 "2.2nF" H 4458 6395 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4550 6350 50  0001 C CNN
F 3 "~" H 4550 6350 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 25V 2.2nF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603S222K3RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603S222K3R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603S222K3R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    4550 6350
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5D648443
P 4550 6500
F 0 "#PWR021" H 4550 6250 50  0001 C CNN
F 1 "GND" H 4555 6327 50  0000 C CNN
F 2 "" H 4550 6500 50  0001 C CNN
F 3 "" H 4550 6500 50  0001 C CNN
	1    4550 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 6500 4550 6450
Wire Wire Line
	4550 6250 4550 6200
Wire Wire Line
	4550 6200 4450 6200
Connection ~ 4450 6200
Wire Wire Line
	5000 6200 4550 6200
Connection ~ 4550 6200
$Comp
L Device:C_Small C3
U 1 1 5D697CAA
P 2100 1300
F 0 "C3" H 2008 1254 50  0000 R CNN
F 1 "0.1uF" H 2008 1345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2100 1300 50  0001 C CNN
F 3 "~" H 2100 1300 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2100 1300
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5D69854F
P 3350 1150
F 0 "R7" V 3154 1150 50  0000 C CNN
F 1 "10R" V 3245 1150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3350 1150 50  0001 C CNN
F 3 "~" H 3350 1150 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 10ohms 0.5% 25ppm 250mW" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "MCT0603PD1009DP500" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "594-MCT0603PD1009DP5" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "594-MCT0603PD1009DP5" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3350 1150
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5D6991D1
P 3100 1300
F 0 "C5" H 3008 1254 50  0000 R CNN
F 1 "0.1uF" H 3008 1345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3100 1300 50  0001 C CNN
F 3 "~" H 3100 1300 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3100 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3250 1150 3100 1150
Wire Wire Line
	2700 1150 2700 1100
Wire Wire Line
	3100 1100 3100 1150
Connection ~ 3100 1150
Wire Wire Line
	3100 1150 2700 1150
Wire Wire Line
	3100 1200 3100 1150
Wire Wire Line
	3100 1450 3100 1400
Wire Wire Line
	3600 1100 3600 1150
Wire Wire Line
	3600 1150 3450 1150
$Comp
L Device:C_Small C11
U 1 1 5D6BB07C
P 4000 1300
F 0 "C11" H 3908 1254 50  0000 R CNN
F 1 "0.1uF" H 3908 1345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4000 1300 50  0001 C CNN
F 3 "~" H 4000 1300 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    4000 1300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5D6BB5EE
P 4000 1450
F 0 "#PWR017" H 4000 1200 50  0001 C CNN
F 1 "GND" H 4005 1277 50  0000 C CNN
F 2 "" H 4000 1450 50  0001 C CNN
F 3 "" H 4000 1450 50  0001 C CNN
	1    4000 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1200 4000 1150
Wire Wire Line
	4000 1150 3600 1150
Wire Wire Line
	4000 1450 4000 1400
$Comp
L Device:C_Small C15
U 1 1 5D6C98F8
P 4950 1300
F 0 "C15" H 4858 1254 50  0000 R CNN
F 1 "0.1uF" H 4858 1345 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 1300 50  0001 C CNN
F 3 "~" H 4950 1300 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    4950 1300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5D6C9D2D
P 4950 1450
F 0 "#PWR022" H 4950 1200 50  0001 C CNN
F 1 "GND" H 4955 1277 50  0000 C CNN
F 2 "" H 4950 1450 50  0001 C CNN
F 3 "" H 4950 1450 50  0001 C CNN
	1    4950 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1450 4950 1400
Wire Wire Line
	4950 1200 4950 1150
Wire Wire Line
	4950 1150 4550 1150
Wire Wire Line
	4550 1150 4550 1100
$Comp
L power:GND #PWR016
U 1 1 5D6DF87C
P 3600 1450
F 0 "#PWR016" H 3600 1200 50  0001 C CNN
F 1 "GND" H 3605 1277 50  0000 C CNN
F 2 "" H 3600 1450 50  0001 C CNN
F 3 "" H 3600 1450 50  0001 C CNN
	1    3600 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5D6DFDDF
P 4550 1450
F 0 "#PWR019" H 4550 1200 50  0001 C CNN
F 1 "GND" H 4555 1277 50  0000 C CNN
F 2 "" H 4550 1450 50  0001 C CNN
F 3 "" H 4550 1450 50  0001 C CNN
	1    4550 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5D6E047D
P 2100 1450
F 0 "#PWR04" H 2100 1200 50  0001 C CNN
F 1 "GND" H 2105 1277 50  0000 C CNN
F 2 "" H 2100 1450 50  0001 C CNN
F 3 "" H 2100 1450 50  0001 C CNN
	1    2100 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D6E08A4
P 1700 1450
F 0 "#PWR02" H 1700 1200 50  0001 C CNN
F 1 "GND" H 1705 1277 50  0000 C CNN
F 2 "" H 1700 1450 50  0001 C CNN
F 3 "" H 1700 1450 50  0001 C CNN
	1    1700 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1200 2100 1150
Wire Wire Line
	2100 1150 1700 1150
Wire Wire Line
	1700 1150 1700 1100
Wire Wire Line
	2100 1450 2100 1400
$Comp
L Device:CP_Small C1
U 1 1 5D6F04E7
P 1700 1300
F 0 "C1" H 1788 1346 50  0000 L CNN
F 1 "10uF" H 1788 1255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 1700 1300 50  0001 C CNN
F 3 "~" H 1700 1300 50  0001 C CNN
F 4 "Condensateur au tantale CMS 10uF 10V 10% case A" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay/Sprague" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TR3A106K010C0900" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    1700 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C4
U 1 1 5D6F15D6
P 2700 1300
F 0 "C4" H 2788 1346 50  0000 L CNN
F 1 "10uF" H 2788 1255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 2700 1300 50  0001 C CNN
F 3 "~" H 2700 1300 50  0001 C CNN
F 4 "Condensateur au tantale CMS 10uF 10V 10% case A" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay/Sprague" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TR3A106K010C0900" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    2700 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C8
U 1 1 5D6F1BF2
P 3600 1300
F 0 "C8" H 3688 1346 50  0000 L CNN
F 1 "10uF" H 3688 1255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 3600 1300 50  0001 C CNN
F 3 "~" H 3600 1300 50  0001 C CNN
F 4 "Condensateur au tantale CMS 10uF 10V 10% case A" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay/Sprague" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TR3A106K010C0900" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    3600 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C12
U 1 1 5D6F220A
P 4550 1300
F 0 "C12" H 4638 1346 50  0000 L CNN
F 1 "10uF" H 4638 1255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A_Pad1.58x1.35mm_HandSolder" H 4550 1300 50  0001 C CNN
F 3 "~" H 4550 1300 50  0001 C CNN
F 4 "Condensateur au tantale CMS 10uF 10V 10% case A" H 0   0   50  0001 C CNN "Description"
F 5 "Vishay/Sprague" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TR3A106K010C0900" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "74-TR3A106K010C0900" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    4550 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1200 1700 1150
Connection ~ 1700 1150
Wire Wire Line
	1700 1450 1700 1400
Wire Wire Line
	2700 1450 2700 1400
Wire Wire Line
	2700 1200 2700 1150
Connection ~ 2700 1150
Wire Wire Line
	3600 1200 3600 1150
Wire Wire Line
	3600 1400 3600 1450
$Comp
L power:+5VD #PWR015
U 1 1 5D54333D
P 3600 1100
F 0 "#PWR015" H 3600 950 50  0001 C CNN
F 1 "+5VD" H 3615 1273 50  0000 C CNN
F 2 "" H 3600 1100 50  0001 C CNN
F 3 "" H 3600 1100 50  0001 C CNN
	1    3600 1100
	1    0    0    -1  
$EndComp
Connection ~ 3600 1150
Wire Wire Line
	4550 1200 4550 1150
Connection ~ 4550 1150
Wire Wire Line
	4550 1400 4550 1450
$Comp
L Device:R_Small R14
U 1 1 5D79B28C
P 11600 2450
F 0 "R14" V 11404 2450 50  0000 C CNN
F 1 "33k" V 11495 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11600 2450 50  0001 C CNN
F 3 "~" H 11600 2450 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 33Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB333V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    11600 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 5D79B9F3
P 11600 2650
F 0 "R15" V 11500 2650 50  0000 C CNN
F 1 "33k" V 11400 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11600 2650 50  0001 C CNN
F 3 "~" H 11600 2650 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 33Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB333V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    11600 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11700 2450 11800 2450
Wire Wire Line
	11900 2650 11800 2650
Wire Wire Line
	12500 2550 12550 2550
$Comp
L Device:R_Small R18
U 1 1 5D7C40EA
P 12200 2000
F 0 "R18" V 12004 2000 50  0000 C CNN
F 1 "150k" V 12095 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12200 2000 50  0001 C CNN
F 3 "~" H 12200 2000 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 150Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB154V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    12200 2000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR048
U 1 1 5D7C47FC
P 12550 2050
F 0 "#PWR048" H 12550 1800 50  0001 C CNN
F 1 "GND" H 12555 1877 50  0000 C CNN
F 2 "" H 12550 2050 50  0001 C CNN
F 3 "" H 12550 2050 50  0001 C CNN
	1    12550 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	12300 2000 12550 2000
Wire Wire Line
	12550 2000 12550 2050
Wire Wire Line
	12100 2000 11800 2000
Wire Wire Line
	11800 2000 11800 2450
Connection ~ 11800 2450
Wire Wire Line
	11800 2450 11900 2450
$Comp
L Device:R_Small R19
U 1 1 5D7D6BC7
P 12200 2950
F 0 "R19" V 12004 2950 50  0000 C CNN
F 1 "150k" V 12095 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12200 2950 50  0001 C CNN
F 3 "~" H 12200 2950 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 150Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB154V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    12200 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	12100 2950 11800 2950
Wire Wire Line
	11800 2950 11800 2650
Connection ~ 11800 2650
Wire Wire Line
	11800 2650 11700 2650
Wire Wire Line
	12300 2950 12550 2950
Wire Wire Line
	12550 2950 12550 2550
Connection ~ 12550 2550
Wire Wire Line
	12550 2550 12800 2550
Wire Wire Line
	12800 2750 12750 2750
Wire Wire Line
	12750 2750 12750 2950
Wire Wire Line
	12750 2950 13450 2950
Wire Wire Line
	13450 2950 13450 2650
Wire Wire Line
	13450 2650 13400 2650
Wire Wire Line
	14050 2650 13450 2650
Connection ~ 13450 2650
Wire Wire Line
	11500 2450 11200 2450
$Comp
L Device:R_Small R10
U 1 1 5D83283B
P 10600 2350
F 0 "R10" H 10541 2304 50  0000 R CNN
F 1 "6.8k" H 10541 2395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10600 2350 50  0001 C CNN
F 3 "~" H 10600 2350 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 6.8Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB682V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB682V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB682V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    10600 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5D83322E
P 10600 2950
F 0 "R11" H 10541 2904 50  0000 R CNN
F 1 "1.2k" H 10541 2995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10600 2950 50  0001 C CNN
F 3 "~" H 10600 2950 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 1.2Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB122V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB122V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB122V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    10600 2950
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C20
U 1 1 5D833752
P 11050 2800
F 0 "C20" H 10958 2754 50  0000 R CNN
F 1 "0.1uF" H 10958 2845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11050 2800 50  0001 C CNN
F 3 "~" H 11050 2800 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    11050 2800
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR046
U 1 1 5D83469B
P 11050 2950
F 0 "#PWR046" H 11050 2700 50  0001 C CNN
F 1 "GND" H 11055 2777 50  0000 C CNN
F 2 "" H 11050 2950 50  0001 C CNN
F 3 "" H 11050 2950 50  0001 C CNN
	1    11050 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5D840007
P 10600 3100
F 0 "#PWR043" H 10600 2850 50  0001 C CNN
F 1 "GND" H 10605 2927 50  0000 C CNN
F 2 "" H 10600 3100 50  0001 C CNN
F 3 "" H 10600 3100 50  0001 C CNN
	1    10600 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 3100 10600 3050
Wire Wire Line
	11050 2950 11050 2900
Wire Wire Line
	11050 2700 11050 2650
Wire Wire Line
	11050 2650 11500 2650
$Comp
L power:+3V3 #PWR042
U 1 1 5D861950
P 10600 2200
F 0 "#PWR042" H 10600 2050 50  0001 C CNN
F 1 "+3V3" H 10615 2373 50  0000 C CNN
F 2 "" H 10600 2200 50  0001 C CNN
F 3 "" H 10600 2200 50  0001 C CNN
	1    10600 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 2200 10600 2250
$Comp
L Device:R_POT Rp1
U 1 1 5D86DE2C
P 10600 2650
F 0 "Rp1" H 10531 2696 50  0000 R CNN
F 1 "200" H 10531 2605 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 10600 2650 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/54/296-776415.pdf" H 10600 2650 50  0001 C CNN
F 4 "Résistances ajustable - Trou débouchant 3/8IN 200 OHM Sealed Vertical Adjust" H 0   0   50  0001 C CNN "Description"
F 5 "Bourns" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "PV36W201C01B00" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "81-PV36W201C01B00" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "81-PV36W201C01B00" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    10600 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 2650 11050 2650
Connection ~ 11050 2650
Wire Wire Line
	10600 2500 10600 2450
Wire Wire Line
	10600 2850 10600 2800
$Comp
L Device:C_Small C22
U 1 1 5D8B3E9C
P 13650 4000
F 0 "C22" H 13558 3954 50  0000 R CNN
F 1 "0.1uF" H 13558 4045 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 13650 4000 50  0001 C CNN
F 3 "~" H 13650 4000 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    13650 4000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR051
U 1 1 5D8B46F1
P 14000 4400
F 0 "#PWR051" H 14000 4150 50  0001 C CNN
F 1 "GND" H 14005 4227 50  0000 C CNN
F 2 "" H 14000 4400 50  0001 C CNN
F 3 "" H 14000 4400 50  0001 C CNN
	1    14000 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 4400 14000 4350
Wire Wire Line
	14000 4350 13650 4350
Wire Wire Line
	13650 4350 13650 4100
Connection ~ 14000 4350
Wire Wire Line
	14000 4350 14000 4300
Wire Wire Line
	13650 3900 13650 3650
Wire Wire Line
	13650 3650 14000 3650
Wire Wire Line
	14000 3650 14000 3700
$Comp
L power:+12VA #PWR050
U 1 1 5D8E632E
P 14000 3600
F 0 "#PWR050" H 14000 3450 50  0001 C CNN
F 1 "+12VA" H 14015 3773 50  0000 C CNN
F 2 "" H 14000 3600 50  0001 C CNN
F 3 "" H 14000 3600 50  0001 C CNN
	1    14000 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+12VA #PWR052
U 1 1 5D8E721F
P 15000 3600
F 0 "#PWR052" H 15000 3450 50  0001 C CNN
F 1 "+12VA" H 15015 3773 50  0000 C CNN
F 2 "" H 15000 3600 50  0001 C CNN
F 3 "" H 15000 3600 50  0001 C CNN
	1    15000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 3600 14000 3650
Connection ~ 14000 3650
$Comp
L power:GND #PWR053
U 1 1 5D90180B
P 15000 4400
F 0 "#PWR053" H 15000 4150 50  0001 C CNN
F 1 "GND" H 15005 4227 50  0000 C CNN
F 2 "" H 15000 4400 50  0001 C CNN
F 3 "" H 15000 4400 50  0001 C CNN
	1    15000 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C23
U 1 1 5D901C6F
P 14650 4050
F 0 "C23" H 14558 4004 50  0000 R CNN
F 1 "0.1uF" H 14558 4095 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 14650 4050 50  0001 C CNN
F 3 "~" H 14650 4050 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    14650 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	14650 3950 14650 3650
Wire Wire Line
	14650 3650 15000 3650
Wire Wire Line
	15000 3650 15000 3600
Wire Wire Line
	15000 3700 15000 3650
Connection ~ 15000 3650
Wire Wire Line
	15000 4400 15000 4350
Wire Wire Line
	14650 4150 14650 4350
Wire Wire Line
	14650 4350 15000 4350
Connection ~ 15000 4350
Wire Wire Line
	15000 4350 15000 4300
$Comp
L SamacSys_Parts:LT1490A U4
U 1 1 5D9795FE
P 12200 5250
F 0 "U4" H 12200 5617 50  0000 C CNN
F 1 "TS922AIDT" H 12200 5526 50  0000 C CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 12200 5250 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 12200 5250 50  0001 C CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TS922AIDT" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "511-TS922AIDT" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    12200 5250
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:LT1490A U4
U 2 1 5D979608
P 13100 5350
F 0 "U4" H 13100 5717 50  0000 C CNN
F 1 "TS922AIDT" H 13100 5626 50  0000 C CNN
F 2 "EuroPress:SO-8_3.9x4.9mm_P1.27mm" H 13100 5350 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/cd00001188.pdf" H 13100 5350 50  0001 C CNN
F 4 "TS922AIDT, Dual Operational Amplifier 4MHz CMOS, Rail-Rail, 3 V, 5 V, 9 V, 8-Pin SO" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "TS922AIDT" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "511-TS922AIDT" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "511-TS922AIDT" H 0   0   50  0001 C CNN "Supplier_Ref"
	2    13100 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R16
U 1 1 5D979612
P 11600 5150
F 0 "R16" V 11404 5150 50  0000 C CNN
F 1 "33k" V 11495 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11600 5150 50  0001 C CNN
F 3 "~" H 11600 5150 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 33Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB333V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    11600 5150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 5D97961C
P 11600 5350
F 0 "R17" V 11500 5350 50  0000 C CNN
F 1 "33k" V 11400 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11600 5350 50  0001 C CNN
F 3 "~" H 11600 5350 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 33Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB333V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB333V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    11600 5350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11700 5150 11800 5150
Wire Wire Line
	11900 5350 11800 5350
Wire Wire Line
	12500 5250 12550 5250
$Comp
L Device:R_Small R20
U 1 1 5D979629
P 12200 4700
F 0 "R20" V 12004 4700 50  0000 C CNN
F 1 "150k" V 12095 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12200 4700 50  0001 C CNN
F 3 "~" H 12200 4700 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 150Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB154V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    12200 4700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR049
U 1 1 5D979633
P 12550 4750
F 0 "#PWR049" H 12550 4500 50  0001 C CNN
F 1 "GND" H 12555 4577 50  0000 C CNN
F 2 "" H 12550 4750 50  0001 C CNN
F 3 "" H 12550 4750 50  0001 C CNN
	1    12550 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	12300 4700 12550 4700
Wire Wire Line
	12550 4700 12550 4750
Wire Wire Line
	12100 4700 11800 4700
Wire Wire Line
	11800 4700 11800 5150
Connection ~ 11800 5150
Wire Wire Line
	11800 5150 11900 5150
$Comp
L Device:R_Small R21
U 1 1 5D979643
P 12200 5650
F 0 "R21" V 12004 5650 50  0000 C CNN
F 1 "150k" V 12095 5650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12200 5650 50  0001 C CNN
F 3 "~" H 12200 5650 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 150Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB154V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB154V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    12200 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	12100 5650 11800 5650
Wire Wire Line
	11800 5650 11800 5350
Connection ~ 11800 5350
Wire Wire Line
	11800 5350 11700 5350
Wire Wire Line
	12300 5650 12550 5650
Wire Wire Line
	12550 5650 12550 5250
Connection ~ 12550 5250
Wire Wire Line
	12550 5250 12800 5250
Wire Wire Line
	12800 5450 12750 5450
Wire Wire Line
	12750 5450 12750 5650
Wire Wire Line
	12750 5650 13450 5650
Wire Wire Line
	13450 5650 13450 5350
Wire Wire Line
	13450 5350 13400 5350
Wire Wire Line
	14050 5350 13450 5350
Connection ~ 13450 5350
Wire Wire Line
	11500 5150 11200 5150
$Comp
L Device:R_Small R12
U 1 1 5D97965F
P 10600 5050
F 0 "R12" H 10541 5004 50  0000 R CNN
F 1 "6.8k" H 10541 5095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10600 5050 50  0001 C CNN
F 3 "~" H 10600 5050 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 6.8Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB682V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB682V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB682V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    10600 5050
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5D979669
P 10600 5650
F 0 "R13" H 10541 5604 50  0000 R CNN
F 1 "1.2k" H 10541 5695 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10600 5650 50  0001 C CNN
F 3 "~" H 10600 5650 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 1.2Kohms 0.1% 25ppm" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB122V" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB122V" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB122V" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    10600 5650
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C21
U 1 1 5D979673
P 11050 5500
F 0 "C21" H 10958 5454 50  0000 R CNN
F 1 "0.1uF" H 10958 5545 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11050 5500 50  0001 C CNN
F 3 "~" H 11050 5500 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 0   0   50  0001 C CNN "Description"
F 5 "Kemet" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    11050 5500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR047
U 1 1 5D97967D
P 11050 5650
F 0 "#PWR047" H 11050 5400 50  0001 C CNN
F 1 "GND" H 11055 5477 50  0000 C CNN
F 2 "" H 11050 5650 50  0001 C CNN
F 3 "" H 11050 5650 50  0001 C CNN
	1    11050 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR045
U 1 1 5D979687
P 10600 5800
F 0 "#PWR045" H 10600 5550 50  0001 C CNN
F 1 "GND" H 10605 5627 50  0000 C CNN
F 2 "" H 10600 5800 50  0001 C CNN
F 3 "" H 10600 5800 50  0001 C CNN
	1    10600 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 5800 10600 5750
Wire Wire Line
	11050 5650 11050 5600
Wire Wire Line
	11050 5400 11050 5350
Wire Wire Line
	11050 5350 11500 5350
$Comp
L power:+3V3 #PWR044
U 1 1 5D979695
P 10600 4900
F 0 "#PWR044" H 10600 4750 50  0001 C CNN
F 1 "+3V3" H 10615 5073 50  0000 C CNN
F 2 "" H 10600 4900 50  0001 C CNN
F 3 "" H 10600 4900 50  0001 C CNN
	1    10600 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 4900 10600 4950
$Comp
L Device:R_POT Rp2
U 1 1 5D9796A0
P 10600 5350
F 0 "Rp2" H 10531 5396 50  0000 R CNN
F 1 "200" H 10531 5305 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 10600 5350 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/54/296-776415.pdf" H 10600 5350 50  0001 C CNN
F 4 "Résistances ajustable - Trou débouchant 3/8IN 200 OHM Sealed Vertical Adjust" H 0   0   50  0001 C CNN "Description"
F 5 "Bourns" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 6 "PV36W201C01B00" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 7 "81-PV36W201C01B00" H 0   0   50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 0   0   50  0001 C CNN "Supplier"
F 9 "81-PV36W201C01B00" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    10600 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 5350 11050 5350
Connection ~ 11050 5350
Wire Wire Line
	10600 5200 10600 5150
Wire Wire Line
	10600 5550 10600 5500
Text Notes 10600 3650 0    50   ~ 0
Arduino Due analog output voltage range is [0.55; 2.75]V.\nThe differential amplifier is used to remove the 0.55V offset\nand to amplify the output signal to reach [0;10]V range.
Text Notes 10600 3950 0    50   ~ 0
Gain = 10 / (2.75 - 0.55) = 10 / 2.2 = 4.545\nG = R2 / R1 = 150000 / 33000 = 4.545
Wire Notes Line
	9400 500  9400 7000
Text Notes 600  650  0    60   ~ 0
ANALOG INPUTS
Wire Notes Line
	500  700  1400 700 
Wire Notes Line
	1400 700  1400 500 
Text Notes 9600 650  0    60   ~ 0
ANALOG OUTPUTS
Wire Notes Line
	9550 700  10450 700 
Wire Notes Line
	10450 700  10450 500 
Wire Notes Line
	500  7000 16050 7000
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J1
U 1 1 5DC951FC
P 14550 8250
F 0 "J1" H 14600 8667 50  0000 C CNN
F 1 "Analog_in_out" H 14600 8576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 14550 8250 50  0001 C CNN
F 3 "~" H 14550 8250 50  0001 C CNN
F 4 "Samtec" H 5450 -50 50  0001 C CNN "Manufacturer_Name"
F 5 "Samtec" H 5450 -50 50  0001 C CNN "Supplier"
F 6 "Embase male 2 rangées 6 positions" H 0   0   50  0001 C CNN "Description"
F 7 "IPL1-106-01-L-D-K" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 8 "Yes" H 0   0   50  0001 C CNN "Sample"
F 9 "IPL1-106-01-L-D-K" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    14550 8250
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR054
U 1 1 5DC965BF
P 14950 7900
F 0 "#PWR054" H 14950 7750 50  0001 C CNN
F 1 "+24V" H 14965 8073 50  0000 C CNN
F 2 "" H 14950 7900 50  0001 C CNN
F 3 "" H 14950 7900 50  0001 C CNN
	1    14950 7900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5DC96DE5
P 14950 8650
F 0 "#PWR055" H 14950 8400 50  0001 C CNN
F 1 "GND" H 14955 8477 50  0000 C CNN
F 2 "" H 14950 8650 50  0001 C CNN
F 3 "" H 14950 8650 50  0001 C CNN
	1    14950 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	14850 8250 14950 8250
Wire Wire Line
	14950 8250 14950 8150
Wire Wire Line
	14850 8050 14950 8050
Connection ~ 14950 8050
Wire Wire Line
	14950 8050 14950 7900
Wire Wire Line
	14850 8150 14950 8150
Connection ~ 14950 8150
Wire Wire Line
	14950 8150 14950 8050
Wire Wire Line
	14850 8350 14950 8350
Wire Wire Line
	14950 8350 14950 8450
Wire Wire Line
	14850 8450 14950 8450
Connection ~ 14950 8450
Wire Wire Line
	14950 8450 14950 8550
Wire Wire Line
	14850 8550 14950 8550
Connection ~ 14950 8550
Wire Wire Line
	14950 8550 14950 8650
Wire Wire Line
	13750 8050 14350 8050
Wire Wire Line
	13750 8150 14350 8150
Wire Wire Line
	13750 8350 14350 8350
Wire Wire Line
	13750 8450 14350 8450
Text Label 13750 8050 0    50   ~ 0
ANA_IN_CH2
Text Label 13750 8150 0    50   ~ 0
ANA_IN_CH1
Text Label 13750 8350 0    50   ~ 0
ANA_OUT_CH2
Text Label 13750 8450 0    50   ~ 0
ANA_OUT_CH1
Text Label 14050 2650 2    50   ~ 0
ANA_OUT_CH1
Text Label 14050 5350 2    50   ~ 0
ANA_OUT_CH2
Text Label 1250 4300 0    50   ~ 0
ANA_IN_CH2
Text Label 1000 5800 0    50   ~ 0
ANA_IN_CH1
Text Label 8550 2200 2    50   ~ 0
PP_BIT0
Text Label 8550 2300 2    50   ~ 0
PP_BIT1
Text Label 8550 2400 2    50   ~ 0
PP_BIT2
Text Label 8550 2500 2    50   ~ 0
PP_BIT3
Text Label 8550 2600 2    50   ~ 0
PP_BIT4
Text Label 8550 2700 2    50   ~ 0
PP_BIT5
Text Label 8550 2800 2    50   ~ 0
PP_BIT6
Text Label 8550 2900 2    50   ~ 0
PP_BIT7
Text Label 8550 3000 2    50   ~ 0
PP_BIT8
Text Label 8550 3100 2    50   ~ 0
PP_BIT9
Text Label 8550 3200 2    50   ~ 0
PP_BIT10
Text Label 8550 3300 2    50   ~ 0
PP_BIT11
Text Label 8550 3400 2    50   ~ 0
PP_BIT12
Text Label 8550 3500 2    50   ~ 0
PP_BIT13
Text Label 8550 3600 2    50   ~ 0
PP_BIT14
Text Label 8550 3700 2    50   ~ 0
PP_BIT15
Text Label 5650 4200 0    50   ~ 0
ADC_CONVST
Text Label 5650 3600 0    50   ~ 0
ADC_RD
Text Label 8550 4000 2    50   ~ 0
ADC_BUSY
Text Label 5000 6200 2    50   ~ 0
ADC1
$Comp
L EuroPress:EuroPress_AnalogBoard XA1
U 1 1 5DD26257
P 8850 8900
F 0 "XA1" H 8200 9850 50  0000 C CNN
F 1 "EuroPress_AnalogBoard" H 9550 9850 50  0000 C CNN
F 2 "EuroPress:EuroPress_AnalogBoard" V 9050 8900 50  0001 C CNN
F 3 "" H 9250 9400 50  0001 C CNN
F 4 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Name"
F 5 "N/A" H 0   0   50  0001 C CNN "Manufacturer_Part_Number"
F 6 "N/A" H 0   0   50  0001 C CNN "Mouser Part Number"
F 7 "N/A" H 0   0   50  0001 C CNN "Supplier"
F 8 "N/A" H 0   0   50  0001 C CNN "Supplier_Ref"
	1    8850 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 8150 9850 8150
Wire Wire Line
	10350 8250 9850 8250
Wire Wire Line
	10350 8350 9850 8350
Wire Wire Line
	10350 8450 9850 8450
Wire Wire Line
	10350 8550 9850 8550
Wire Wire Line
	10350 8650 9850 8650
Wire Wire Line
	10350 8750 9850 8750
Wire Wire Line
	10350 8850 9850 8850
Wire Wire Line
	10350 8950 9850 8950
Wire Wire Line
	10350 9050 9850 9050
Wire Wire Line
	10350 9150 9850 9150
Wire Wire Line
	10350 9250 9850 9250
Wire Wire Line
	10350 9350 9850 9350
Wire Wire Line
	10350 9450 9850 9450
Wire Wire Line
	10350 9550 9850 9550
Wire Wire Line
	10350 9650 9850 9650
Wire Wire Line
	7200 8200 7850 8200
Wire Wire Line
	7200 8300 7850 8300
Wire Wire Line
	7200 8400 7850 8400
Wire Wire Line
	7200 8600 7850 8600
Wire Wire Line
	7200 8700 7850 8700
Wire Wire Line
	7200 8800 7850 8800
Wire Wire Line
	7750 9050 7850 9050
Wire Wire Line
	7750 9150 7850 9150
Wire Wire Line
	7750 9250 7850 9250
Wire Wire Line
	7750 9350 7850 9350
Wire Wire Line
	7750 9450 7850 9450
Wire Wire Line
	7750 9550 7850 9550
Wire Wire Line
	7750 9650 7850 9650
$Comp
L power:GND #PWR023
U 1 1 5E023AC9
P 7750 9700
F 0 "#PWR023" H 7750 9450 50  0001 C CNN
F 1 "GND" H 7755 9527 50  0000 C CNN
F 2 "" H 7750 9700 50  0001 C CNN
F 3 "" H 7750 9700 50  0001 C CNN
	1    7750 9700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 9700 7750 9650
Connection ~ 7750 9150
Wire Wire Line
	7750 9150 7750 9050
Connection ~ 7750 9250
Wire Wire Line
	7750 9250 7750 9150
Connection ~ 7750 9350
Wire Wire Line
	7750 9350 7750 9250
Connection ~ 7750 9450
Wire Wire Line
	7750 9450 7750 9350
Connection ~ 7750 9550
Wire Wire Line
	7750 9550 7750 9450
Connection ~ 7750 9650
Wire Wire Line
	7750 9650 7750 9550
Text Label 10350 8150 2    50   ~ 0
PP_BIT0
Text Label 10350 8250 2    50   ~ 0
PP_BIT1
Text Label 10350 8350 2    50   ~ 0
PP_BIT2
Text Label 10350 8450 2    50   ~ 0
PP_BIT3
Text Label 10350 8550 2    50   ~ 0
PP_BIT4
Text Label 10350 8650 2    50   ~ 0
PP_BIT5
Text Label 10350 8750 2    50   ~ 0
PP_BIT6
Text Label 10350 8850 2    50   ~ 0
PP_BIT7
Text Label 10350 8950 2    50   ~ 0
PP_BIT8
Text Label 10350 9050 2    50   ~ 0
PP_BIT9
Text Label 10350 9150 2    50   ~ 0
PP_BIT10
Text Label 10350 9250 2    50   ~ 0
PP_BIT11
Text Label 10350 9350 2    50   ~ 0
PP_BIT12
Text Label 10350 9450 2    50   ~ 0
PP_BIT13
Text Label 10350 9550 2    50   ~ 0
PP_BIT14
Text Label 10350 9650 2    50   ~ 0
PP_BIT15
Text Label 7200 8200 0    50   ~ 0
ADC_BUSY
Text Label 7200 8300 0    50   ~ 0
ADC_RD
Text Label 7200 8400 0    50   ~ 0
ADC_CONVST
Text Label 7200 8600 0    50   ~ 0
ADC1
Text Label 7200 8700 0    50   ~ 0
DAC1
Text Label 7200 8800 0    50   ~ 0
DAC2
$Comp
L power:-12VA #PWR065
U 1 1 5E104446
P 8850 10200
F 0 "#PWR065" H 8850 10050 50  0001 C CNN
F 1 "-12VA" H 8865 10373 50  0000 C CNN
F 2 "" H 8850 10200 50  0001 C CNN
F 3 "" H 8850 10200 50  0001 C CNN
	1    8850 10200
	-1   0    0    1   
$EndComp
$Comp
L power:+12VA #PWR064
U 1 1 5E105669
P 8800 7450
F 0 "#PWR064" H 8800 7300 50  0001 C CNN
F 1 "+12VA" H 8815 7623 50  0000 C CNN
F 2 "" H 8800 7450 50  0001 C CNN
F 3 "" H 8800 7450 50  0001 C CNN
	1    8800 7450
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR063
U 1 1 5E10678A
P 8700 7650
F 0 "#PWR063" H 8700 7500 50  0001 C CNN
F 1 "+24V" H 8715 7823 50  0000 C CNN
F 2 "" H 8700 7650 50  0001 C CNN
F 3 "" H 8700 7650 50  0001 C CNN
	1    8700 7650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR066
U 1 1 5E106FE7
P 8900 7650
F 0 "#PWR066" H 8900 7500 50  0001 C CNN
F 1 "+5V" H 8915 7823 50  0000 C CNN
F 2 "" H 8900 7650 50  0001 C CNN
F 3 "" H 8900 7650 50  0001 C CNN
	1    8900 7650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR067
U 1 1 5E108676
P 9000 7450
F 0 "#PWR067" H 9000 7300 50  0001 C CNN
F 1 "+3V3" H 9015 7623 50  0000 C CNN
F 2 "" H 9000 7450 50  0001 C CNN
F 3 "" H 9000 7450 50  0001 C CNN
	1    9000 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 10150 8850 10200
Wire Wire Line
	8700 7650 8700 7700
Wire Wire Line
	8800 7450 8800 7700
Wire Wire Line
	8900 7650 8900 7700
Wire Wire Line
	9000 7450 9000 7700
Text Notes 1750 5450 0    50   ~ 0
Filtre actif Sallen et Key (passe-bas du second ordre).\nA = 1\nfc = 2.Pi / sqrt(R3.R5.C6.C9)\nm = (C6.(R3 + R5)) / (2.sqrt(R3.R5.C6.C9))\n\n=> fc=498Hz et m=0.714
$Comp
L Device:R_Small R8
U 1 1 5E0E07DA
P 2100 4450
F 0 "R8" H 2041 4404 50  0000 R CNN
F 1 "150k" H 2041 4495 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2100 4450 50  0001 C CNN
F 3 "~" H 2100 4450 50  0001 C CNN
F 4 "Résistance à couches minces - CMS 0603 150Kohms 0.1% 25ppm" H -150 -1500 50  0001 C CNN "Description"
F 5 "Panasonic" H -150 -1500 50  0001 C CNN "Manufacturer_Name"
F 6 "ERA-3AEB154V" H -150 -1500 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "667-ERA-3AEB154V" H -150 -1500 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H -150 -1500 50  0001 C CNN "Supplier"
F 9 "667-ERA-3AEB154V" H -150 -1500 50  0001 C CNN "Supplier_Ref"
	1    2100 4450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR056
U 1 1 5E0E11DF
P 2100 4600
F 0 "#PWR056" H 2100 4350 50  0001 C CNN
F 1 "GND" H 2105 4427 50  0000 C CNN
F 2 "" H 2100 4600 50  0001 C CNN
F 3 "" H 2100 4600 50  0001 C CNN
	1    2100 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4600 2100 4550
Wire Wire Line
	2100 4350 2100 4300
Wire Wire Line
	2100 4300 2450 4300
$Comp
L Device:R_Small R22
U 1 1 5E104FC5
P 1900 4300
F 0 "R22" V 1704 4300 50  0000 C CNN
F 1 "Shunt" V 1795 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1900 4300 50  0001 C CNN
F 3 "~" H 1900 4300 50  0001 C CNN
	1    1900 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5E10578A
P 1650 5800
F 0 "R9" V 1454 5800 50  0000 C CNN
F 1 "Shunt" V 1545 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1650 5800 50  0001 C CNN
F 3 "~" H 1650 5800 50  0001 C CNN
	1    1650 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 4300 1800 4300
Wire Wire Line
	2000 4300 2100 4300
Connection ~ 2100 4300
Wire Wire Line
	1000 5800 1550 5800
Wire Wire Line
	1750 5800 1850 5800
Connection ~ 1850 5800
Text Label 11200 5150 0    50   ~ 0
DAC2
Text Label 11200 2450 0    50   ~ 0
DAC1
NoConn ~ 14350 8250
NoConn ~ 14350 8550
NoConn ~ 8050 4200
$EndSCHEMATC
