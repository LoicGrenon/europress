
kikit panelize grid --space 5 --gridsize 2 1 --tabwidth 18 --tabheight 10 --mousebites 0.5 1 0 --radius 1 --railsTb 5 --fiducials 10 2.5 1 2 --tooling 5 2.5 2.5 EuroPress/Schematics/OutputsBoard/EuroPress_OutputsBoard.kicad_pcb EuroPress/Schematics/OutputsBoard/EuroPress_OutputsBoard-panel_2x1.kicad_pcb

kikit fab jlcpcb --assembly --schematic EuroPress/Schematics/OutputsBoard/EuroPress_OutputsBoard.sch EuroPress/Schematics/OutputsBoard/EuroPress_OutputsBoard-panel_2x1.kicad_pcb EuroPress/Schematics/OutputsBoard/assembly

# Carte seule :

kikit fab jlcpcb --assembly --schematic EuroPress/Schematics/OutputsBoard/EuroPress_OutputsBoard.sch EuroPress/Schematics/OutputsBoard/EuroPress_OutputsBoard.kicad_pcb EuroPress/Schematics/OutputsBoard/assembly



5x 2x1 = 10 cartes => 57.25€
5x 2x2 = 20 cartes => 95.55€

