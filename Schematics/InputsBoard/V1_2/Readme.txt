
kikit panelize grid --space 3.5 --gridsize 2 1 --tabwidth 18 --tabheight 10 --mousebites 0.5 1 0 --radius 1 --railsTb 5 --fiducials 10 2.5 1 2 --tooling 3.5 2.25 2.5 EuroPress/Schematics/InputsBoard/EuroPress_InputsBoard.kicad_pcb EuroPress/Schematics/InputsBoard/EuroPress_InputsBoard-panel_2x1.kicad_pcb

kikit fab jlcpcb --assembly --schematic EuroPress/Schematics/InputsBoard/EuroPress_InputsBoard.sch EuroPress/Schematics/InputsBoard/EuroPress_InputsBoard-panel_2x1.kicad_pcb EuroPress/Schematics/InputsBoard/assembly

# Une seule carte :

kikit fab jlcpcb --assembly --schematic EuroPress/Schematics/InputsBoard/EuroPress_InputsBoard.sch EuroPress/Schematics/InputsBoard/EuroPress_InputsBoard.kicad_pcb EuroPress/Schematics/InputsBoard/assembly
