EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "EuroPress - Inputs board"
Date "2020-02-02"
Rev "1.0"
Comp "LGR"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L EuroPress:ACPL-247-500E Q1
U 1 1 5E36E6BA
P 6500 1600
F 0 "Q1" H 6950 1865 50  0000 C CNN
F 1 "ACPL-247-500E" H 6950 1774 50  0000 C CNN
F 2 "Package_SO:SOP-16_4.4x10.4mm_P1.27mm" H 7250 1700 50  0001 L CNN
F 3 "" H 7250 1600 50  0001 L CNN
F 4 "DC Input Transistor Output Quad Optocoupler, Surface Mount, 16-Pin SOIC" H 7250 1500 50  0001 L CNN "Description"
F 5 "2.42" H 7250 1400 50  0001 L CNN "Height"
F 6 "630-ACPL-247-500E" H 7250 1300 50  0001 L CNN "Mouser Part Number"
F 7 "Avago Technologies" H 7250 1200 50  0001 L CNN "Manufacturer_Name"
F 8 "ACPL-247-500E" H 7250 1100 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 6500 1600 50  0001 C CNN "Supplier"
F 10 "630-ACPL-247-500E" H 6500 1600 50  0001 C CNN "SupplierRef"
	1    6500 1600
	1    0    0    -1  
$EndComp
$Comp
L EuroPress:ACPL-247-500E Q2
U 1 1 5E370245
P 6500 2850
F 0 "Q2" H 6950 3115 50  0000 C CNN
F 1 "ACPL-247-500E" H 6950 3024 50  0000 C CNN
F 2 "Package_SO:SOP-16_4.4x10.4mm_P1.27mm" H 7250 2950 50  0001 L CNN
F 3 "" H 7250 2850 50  0001 L CNN
F 4 "DC Input Transistor Output Quad Optocoupler, Surface Mount, 16-Pin SOIC" H 7250 2750 50  0001 L CNN "Description"
F 5 "2.42" H 7250 2650 50  0001 L CNN "Height"
F 6 "630-ACPL-247-500E" H 7250 2550 50  0001 L CNN "Mouser Part Number"
F 7 "Avago Technologies" H 7250 2450 50  0001 L CNN "Manufacturer_Name"
F 8 "ACPL-247-500E" H 7250 2350 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 6500 2850 50  0001 C CNN "Supplier"
F 10 "630-ACPL-247-500E" H 6500 2850 50  0001 C CNN "SupplierRef"
	1    6500 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5E375131
P 5850 1600
F 0 "R1" V 5800 1450 50  0000 C CNN
F 1 "10k" V 5800 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 1600 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 1600 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 1600 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 1600 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 1600 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 1600 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 1600 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 1600 50  0001 C CNN "SupplierRef"
	1    5850 1600
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5E375E15
P 5850 1800
F 0 "R2" V 5800 1650 50  0000 C CNN
F 1 "10k" V 5800 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 1800 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 1800 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 1800 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 1800 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 1800 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 1800 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 1800 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 1800 50  0001 C CNN "SupplierRef"
	1    5850 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5E3760E0
P 5850 2000
F 0 "R3" V 5800 1850 50  0000 C CNN
F 1 "10k" V 5800 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 2000 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 2000 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 2000 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 2000 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 2000 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 2000 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 2000 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 2000 50  0001 C CNN "SupplierRef"
	1    5850 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5E3763F3
P 5850 2200
F 0 "R4" V 5800 2050 50  0000 C CNN
F 1 "10k" V 5800 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 2200 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 2200 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 2200 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 2200 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 2200 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 2200 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 2200 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 2200 50  0001 C CNN "SupplierRef"
	1    5850 2200
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D1
U 1 1 5E376A73
P 6300 1700
F 0 "D1" H 6200 1750 50  0000 C CNN
F 1 "1N4148" H 6300 1814 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 1700 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 1700 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 1700 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 1700 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 1700 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 1700 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 1700 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 1700 50  0001 C CNN "SupplierRef"
	1    6300 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D2
U 1 1 5E377C1E
P 6300 1900
F 0 "D2" H 6200 1950 50  0000 C CNN
F 1 "1N4148" H 6300 2014 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 1900 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 1900 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 1900 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 1900 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 1900 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 1900 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 1900 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 1900 50  0001 C CNN "SupplierRef"
	1    6300 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D3
U 1 1 5E378089
P 6300 2100
F 0 "D3" H 6200 2150 50  0000 C CNN
F 1 "1N4148" H 6300 2214 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 2100 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 2100 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 2100 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 2100 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 2100 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 2100 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 2100 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 2100 50  0001 C CNN "SupplierRef"
	1    6300 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D4
U 1 1 5E37847C
P 6300 2300
F 0 "D4" H 6200 2350 50  0000 C CNN
F 1 "1N4148" H 6300 2414 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 2300 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 2300 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 2300 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 2300 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 2300 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 2300 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 2300 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 2300 50  0001 C CNN "SupplierRef"
	1    6300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 1700 6450 1700
Wire Wire Line
	6400 1900 6450 1900
Wire Wire Line
	6400 2100 6450 2100
Wire Wire Line
	6400 2300 6450 2300
Wire Wire Line
	6500 1600 6100 1600
Wire Wire Line
	5950 1800 6100 1800
Wire Wire Line
	6500 2000 6100 2000
Wire Wire Line
	5950 2200 6100 2200
Wire Wire Line
	6200 2300 6100 2300
Wire Wire Line
	6100 2300 6100 2200
Connection ~ 6100 2200
Wire Wire Line
	6100 2200 6500 2200
Wire Wire Line
	6200 2100 6100 2100
Wire Wire Line
	6100 2100 6100 2000
Connection ~ 6100 2000
Wire Wire Line
	6100 2000 5950 2000
Wire Wire Line
	6200 1900 6100 1900
Wire Wire Line
	6100 1900 6100 1800
Connection ~ 6100 1800
Wire Wire Line
	6100 1800 6500 1800
Wire Wire Line
	6200 1700 6100 1700
Wire Wire Line
	6100 1700 6100 1600
Connection ~ 6100 1600
Wire Wire Line
	6100 1600 5950 1600
$Comp
L power:GND #PWR01
U 1 1 5E37BDBF
P 6450 2400
F 0 "#PWR01" H 6450 2150 50  0001 C CNN
F 1 "GND" H 6455 2227 50  0000 C CNN
F 2 "" H 6450 2400 50  0001 C CNN
F 3 "" H 6450 2400 50  0001 C CNN
	1    6450 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5E37C5E8
P 7500 2400
F 0 "#PWR06" H 7500 2150 50  0001 C CNN
F 1 "GND" H 7505 2227 50  0000 C CNN
F 2 "" H 7500 2400 50  0001 C CNN
F 3 "" H 7500 2400 50  0001 C CNN
	1    7500 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2400 6450 2300
Connection ~ 6450 1700
Wire Wire Line
	6450 1700 6400 1700
Connection ~ 6450 1900
Wire Wire Line
	6450 1900 6500 1900
Wire Wire Line
	6450 1900 6450 1700
Connection ~ 6450 2100
Wire Wire Line
	6450 2100 6500 2100
Wire Wire Line
	6450 2100 6450 1900
Connection ~ 6450 2300
Wire Wire Line
	6450 2300 6500 2300
Wire Wire Line
	6450 2300 6450 2100
Wire Wire Line
	7400 1700 7500 1700
Wire Wire Line
	7500 1700 7500 1900
Wire Wire Line
	7400 2300 7500 2300
Connection ~ 7500 2300
Wire Wire Line
	7500 2300 7500 2400
Wire Wire Line
	7400 2100 7500 2100
Connection ~ 7500 2100
Wire Wire Line
	7500 2100 7500 2300
Wire Wire Line
	7400 1900 7500 1900
Connection ~ 7500 1900
Wire Wire Line
	7500 1900 7500 2100
Text GLabel 5550 1600 0    50   Input ~ 0
DIG_IN_CH1
Wire Wire Line
	5550 1600 5750 1600
Text GLabel 5550 1800 0    50   Input ~ 0
DIG_IN_CH2
Wire Wire Line
	5550 1800 5750 1800
Text GLabel 5550 2000 0    50   Input ~ 0
DIG_IN_CH3
Wire Wire Line
	5550 2000 5750 2000
Text GLabel 5550 2200 0    50   Input ~ 0
DIG_IN_CH4
Wire Wire Line
	5550 2200 5750 2200
$Comp
L Device:R_Small R5
U 1 1 5E384932
P 5850 2850
F 0 "R5" V 5800 2700 50  0000 C CNN
F 1 "10k" V 5800 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 2850 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 2850 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 2850 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 2850 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 2850 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 2850 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 2850 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 2850 50  0001 C CNN "SupplierRef"
	1    5850 2850
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5E38493C
P 5850 3050
F 0 "R6" V 5800 2900 50  0000 C CNN
F 1 "10k" V 5800 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 3050 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 3050 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 3050 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 3050 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 3050 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 3050 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 3050 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 3050 50  0001 C CNN "SupplierRef"
	1    5850 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5E384946
P 5850 3250
F 0 "R7" V 5800 3100 50  0000 C CNN
F 1 "10k" V 5800 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 3250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 3250 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 3250 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 3250 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 3250 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 3250 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 3250 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 3250 50  0001 C CNN "SupplierRef"
	1    5850 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5E384950
P 5850 3450
F 0 "R8" V 5800 3300 50  0000 C CNN
F 1 "10k" V 5800 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 3450 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 3450 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 3450 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 3450 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 3450 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 3450 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 3450 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 3450 50  0001 C CNN "SupplierRef"
	1    5850 3450
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D5
U 1 1 5E38495A
P 6300 2950
F 0 "D5" H 6200 3000 50  0000 C CNN
F 1 "1N4148" H 6300 3064 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 2950 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 2950 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 2950 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 2950 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 2950 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 2950 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 2950 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 2950 50  0001 C CNN "SupplierRef"
	1    6300 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D6
U 1 1 5E384964
P 6300 3150
F 0 "D6" H 6200 3200 50  0000 C CNN
F 1 "1N4148" H 6300 3264 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 3150 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 3150 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 3150 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 3150 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 3150 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 3150 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 3150 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 3150 50  0001 C CNN "SupplierRef"
	1    6300 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D7
U 1 1 5E38496E
P 6300 3350
F 0 "D7" H 6200 3400 50  0000 C CNN
F 1 "1N4148" H 6300 3464 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 3350 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 3350 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 3350 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 3350 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 3350 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 3350 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 3350 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 3350 50  0001 C CNN "SupplierRef"
	1    6300 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D8
U 1 1 5E384978
P 6300 3550
F 0 "D8" H 6200 3600 50  0000 C CNN
F 1 "1N4148" H 6300 3664 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 3550 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 3550 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 3550 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 3550 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 3550 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 3550 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 3550 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 3550 50  0001 C CNN "SupplierRef"
	1    6300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2950 6450 2950
Wire Wire Line
	6400 3150 6450 3150
Wire Wire Line
	6400 3350 6450 3350
Wire Wire Line
	6400 3550 6450 3550
Wire Wire Line
	6500 2850 6100 2850
Wire Wire Line
	5950 3050 6100 3050
Wire Wire Line
	6500 3250 6100 3250
Wire Wire Line
	5950 3450 6100 3450
Wire Wire Line
	6200 3550 6100 3550
Wire Wire Line
	6100 3550 6100 3450
Connection ~ 6100 3450
Wire Wire Line
	6100 3450 6500 3450
Wire Wire Line
	6200 3350 6100 3350
Wire Wire Line
	6100 3350 6100 3250
Connection ~ 6100 3250
Wire Wire Line
	6100 3250 5950 3250
Wire Wire Line
	6200 3150 6100 3150
Wire Wire Line
	6100 3150 6100 3050
Connection ~ 6100 3050
Wire Wire Line
	6100 3050 6500 3050
Wire Wire Line
	6200 2950 6100 2950
Wire Wire Line
	6100 2950 6100 2850
Connection ~ 6100 2850
Wire Wire Line
	6100 2850 5950 2850
$Comp
L power:GND #PWR02
U 1 1 5E38499A
P 6450 3650
F 0 "#PWR02" H 6450 3400 50  0001 C CNN
F 1 "GND" H 6455 3477 50  0000 C CNN
F 2 "" H 6450 3650 50  0001 C CNN
F 3 "" H 6450 3650 50  0001 C CNN
	1    6450 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3650 6450 3550
Connection ~ 6450 2950
Wire Wire Line
	6450 2950 6400 2950
Connection ~ 6450 3150
Wire Wire Line
	6450 3150 6500 3150
Wire Wire Line
	6450 3150 6450 2950
Connection ~ 6450 3350
Wire Wire Line
	6450 3350 6500 3350
Wire Wire Line
	6450 3350 6450 3150
Connection ~ 6450 3550
Wire Wire Line
	6450 3550 6500 3550
Wire Wire Line
	6450 3550 6450 3350
Text GLabel 5550 2850 0    50   Input ~ 0
DIG_IN_CH5
Wire Wire Line
	5550 2850 5750 2850
Text GLabel 5550 3050 0    50   Input ~ 0
DIG_IN_CH6
Wire Wire Line
	5550 3050 5750 3050
Text GLabel 5550 3250 0    50   Input ~ 0
DIG_IN_CH7
Wire Wire Line
	5550 3250 5750 3250
Text GLabel 5550 3450 0    50   Input ~ 0
DIG_IN_CH8
Wire Wire Line
	5550 3450 5750 3450
$Comp
L power:GND #PWR07
U 1 1 5E38E2E9
P 7500 3650
F 0 "#PWR07" H 7500 3400 50  0001 C CNN
F 1 "GND" H 7505 3477 50  0000 C CNN
F 2 "" H 7500 3650 50  0001 C CNN
F 3 "" H 7500 3650 50  0001 C CNN
	1    7500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 2950 7500 2950
Wire Wire Line
	7500 2950 7500 3150
Wire Wire Line
	7400 3550 7500 3550
Connection ~ 7500 3550
Wire Wire Line
	7500 3550 7500 3650
Wire Wire Line
	7400 3350 7500 3350
Connection ~ 7500 3350
Wire Wire Line
	7500 3350 7500 3550
Wire Wire Line
	7400 3150 7500 3150
Connection ~ 7500 3150
Wire Wire Line
	7500 3150 7500 3350
$Comp
L Device:R_Network08 RN1
U 1 1 5E391400
P 7900 1250
F 0 "RN1" H 8288 1296 50  0000 L CNN
F 1 "1k" H 8288 1205 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP9" V 8375 1250 50  0001 C CNN
F 3 "https://www.bourns.com/pdfs/4600X.pdf" H 7900 1250 50  0001 C CNN
F 4 "Resistors array 8x1kOhms 2% SIP9" H 7900 1250 50  0001 C CNN "Description"
F 5 "Bourns" H 7900 1250 50  0001 C CNN "Manufacturer_Name"
F 6 "4609X-AP1-102LF" H 7900 1250 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "652-4609X-AP1-102LF" H 7900 1250 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 7900 1250 50  0001 C CNN "Supplier"
F 9 "652-4609X-AP1-102LF" H 7900 1250 50  0001 C CNN "SupplierRef"
	1    7900 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1600 7500 1600
Wire Wire Line
	7500 1600 7500 1450
Wire Wire Line
	7400 1800 7600 1800
Wire Wire Line
	7600 1800 7600 1700
Wire Wire Line
	7400 2000 7700 2000
Wire Wire Line
	7700 2000 7700 1800
Wire Wire Line
	7400 2200 7800 2200
Wire Wire Line
	7800 2200 7800 1900
Wire Wire Line
	7400 2850 7900 2850
Wire Wire Line
	7900 2850 7900 2000
Wire Wire Line
	7400 3050 8000 3050
Wire Wire Line
	8000 3050 8000 2100
Wire Wire Line
	8100 1450 8100 2200
Wire Wire Line
	8100 3250 7400 3250
Wire Wire Line
	7400 3450 8200 3450
Wire Wire Line
	8200 3450 8200 2300
$Comp
L power:+3.3V #PWR05
U 1 1 5E3A8170
P 7500 1000
F 0 "#PWR05" H 7500 850 50  0001 C CNN
F 1 "+3.3V" H 7515 1173 50  0000 C CNN
F 2 "" H 7500 1000 50  0001 C CNN
F 3 "" H 7500 1000 50  0001 C CNN
	1    7500 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 1000 7500 1050
$Comp
L EuroPress:SN74HC540PW U1
U 1 1 5E3AEBDF
P 9000 1500
F 0 "U1" H 9550 1765 50  0000 C CNN
F 1 "SN74HC540PW" H 9550 1674 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 9950 1600 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc540.pdf" H 9950 1500 50  0001 L CNN
F 4 "Octal Buffer & Line Driver, 3-State, Inverting, 2  6 V, 20-Pin TSSOP" H 9950 1400 50  0001 L CNN "Description"
F 5 "595-SN74HC540PW" H 9950 1200 50  0001 L CNN "Mouser Part Number"
F 6 "Texas Instruments" H 9950 1100 50  0001 L CNN "Manufacturer_Name"
F 7 "SN74HC540PW" H 9950 1000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "Mouser" H 9000 1500 50  0001 C CNN "Supplier"
F 9 "595-SN74HC540PW" H 9000 1500 50  0001 C CNN "SupplierRef"
	1    9000 1500
	1    0    0    -1  
$EndComp
Text GLabel 8850 2550 0    50   Input ~ 0
CS_DIG_IN
Wire Wire Line
	8850 2550 8950 2550
Wire Wire Line
	8950 2550 8950 2500
Wire Wire Line
	8950 2500 9000 2500
Wire Wire Line
	9000 2600 8950 2600
Wire Wire Line
	8950 2600 8950 2550
Connection ~ 8950 2550
Wire Wire Line
	9000 1600 7500 1600
Connection ~ 7500 1600
Wire Wire Line
	9000 1700 7600 1700
Connection ~ 7600 1700
Wire Wire Line
	7600 1700 7600 1450
Wire Wire Line
	9000 1800 7700 1800
Connection ~ 7700 1800
Wire Wire Line
	7700 1800 7700 1450
Wire Wire Line
	9000 1900 7800 1900
Connection ~ 7800 1900
Wire Wire Line
	7800 1900 7800 1450
Wire Wire Line
	7900 2000 9000 2000
Connection ~ 7900 2000
Wire Wire Line
	7900 2000 7900 1450
Wire Wire Line
	9000 2100 8000 2100
Connection ~ 8000 2100
Wire Wire Line
	8000 2100 8000 1450
Wire Wire Line
	8100 2200 9000 2200
Connection ~ 8100 2200
Wire Wire Line
	8100 2200 8100 3250
Wire Wire Line
	9000 2300 8200 2300
Connection ~ 8200 2300
Wire Wire Line
	8200 2300 8200 1450
$Comp
L power:+3.3V #PWR015
U 1 1 5E3E4954
P 10200 1400
F 0 "#PWR015" H 10200 1250 50  0001 C CNN
F 1 "+3.3V" H 10215 1573 50  0000 C CNN
F 2 "" H 10200 1400 50  0001 C CNN
F 3 "" H 10200 1400 50  0001 C CNN
	1    10200 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5E3E502C
P 10200 2700
F 0 "#PWR016" H 10200 2450 50  0001 C CNN
F 1 "GND" H 10205 2527 50  0000 C CNN
F 2 "" H 10200 2700 50  0001 C CNN
F 3 "" H 10200 2700 50  0001 C CNN
	1    10200 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5E3E5BB9
P 9550 3300
F 0 "#PWR012" H 9550 3050 50  0001 C CNN
F 1 "GND" H 9555 3127 50  0000 C CNN
F 2 "" H 9550 3300 50  0001 C CNN
F 3 "" H 9550 3300 50  0001 C CNN
	1    9550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR011
U 1 1 5E3E5F6D
P 9550 3000
F 0 "#PWR011" H 9550 2850 50  0001 C CNN
F 1 "+3.3V" H 9565 3173 50  0000 C CNN
F 2 "" H 9550 3000 50  0001 C CNN
F 3 "" H 9550 3000 50  0001 C CNN
	1    9550 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5E3E6D89
P 9550 3150
F 0 "C1" H 9642 3196 50  0000 L CNN
F 1 "0.1uF" H 9642 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9550 3150 50  0001 C CNN
F 3 "~" H 9550 3150 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 9550 3150 50  0001 C CNN "Description"
F 5 "Kemet" H 9550 3150 50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 9550 3150 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 9550 3150 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 9550 3150 50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 9550 3150 50  0001 C CNN "SupplierRef"
	1    9550 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3000 9550 3050
Wire Wire Line
	9550 3250 9550 3300
Wire Wire Line
	10100 1500 10200 1500
Wire Wire Line
	10200 1500 10200 1400
Wire Wire Line
	10100 2600 10200 2600
Wire Wire Line
	10200 2600 10200 2700
Text GLabel 10400 1700 2    50   Output ~ 0
PP_IN0
Wire Wire Line
	10100 1700 10400 1700
Text GLabel 10400 1800 2    50   Output ~ 0
PP_IN1
Wire Wire Line
	10100 1800 10400 1800
Text GLabel 10400 1900 2    50   Output ~ 0
PP_IN2
Wire Wire Line
	10100 1900 10400 1900
Text GLabel 10400 2000 2    50   Output ~ 0
PP_IN3
Wire Wire Line
	10100 2000 10400 2000
Text GLabel 10400 2100 2    50   Output ~ 0
PP_IN4
Wire Wire Line
	10100 2100 10400 2100
Text GLabel 10400 2200 2    50   Output ~ 0
PP_IN5
Wire Wire Line
	10100 2200 10400 2200
Text GLabel 10400 2300 2    50   Output ~ 0
PP_IN6
Wire Wire Line
	10100 2300 10400 2300
Text GLabel 10400 2400 2    50   Output ~ 0
PP_IN7
Wire Wire Line
	10100 2400 10400 2400
$Comp
L EuroPress:ACPL-247-500E Q3
U 1 1 5E40FEEE
P 6500 4850
F 0 "Q3" H 6950 5115 50  0000 C CNN
F 1 "ACPL-247-500E" H 6950 5024 50  0000 C CNN
F 2 "Package_SO:SOP-16_4.4x10.4mm_P1.27mm" H 7250 4950 50  0001 L CNN
F 3 "" H 7250 4850 50  0001 L CNN
F 4 "DC Input Transistor Output Quad Optocoupler, Surface Mount, 16-Pin SOIC" H 7250 4750 50  0001 L CNN "Description"
F 5 "2.42" H 7250 4650 50  0001 L CNN "Height"
F 6 "630-ACPL-247-500E" H 7250 4550 50  0001 L CNN "Mouser Part Number"
F 7 "Avago Technologies" H 7250 4450 50  0001 L CNN "Manufacturer_Name"
F 8 "ACPL-247-500E" H 7250 4350 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 6500 4850 50  0001 C CNN "Supplier"
F 10 "630-ACPL-247-500E" H 6500 4850 50  0001 C CNN "SupplierRef"
	1    6500 4850
	1    0    0    -1  
$EndComp
$Comp
L EuroPress:ACPL-247-500E Q4
U 1 1 5E40FEFD
P 6500 6100
F 0 "Q4" H 6950 6365 50  0000 C CNN
F 1 "ACPL-247-500E" H 6950 6274 50  0000 C CNN
F 2 "Package_SO:SOP-16_4.4x10.4mm_P1.27mm" H 7250 6200 50  0001 L CNN
F 3 "" H 7250 6100 50  0001 L CNN
F 4 "DC Input Transistor Output Quad Optocoupler, Surface Mount, 16-Pin SOIC" H 7250 6000 50  0001 L CNN "Description"
F 5 "2.42" H 7250 5900 50  0001 L CNN "Height"
F 6 "630-ACPL-247-500E" H 7250 5800 50  0001 L CNN "Mouser Part Number"
F 7 "Avago Technologies" H 7250 5700 50  0001 L CNN "Manufacturer_Name"
F 8 "ACPL-247-500E" H 7250 5600 50  0001 L CNN "Manufacturer_Part_Number"
F 9 "Mouser" H 6500 6100 50  0001 C CNN "Supplier"
F 10 "630-ACPL-247-500E" H 6500 6100 50  0001 C CNN "SupplierRef"
	1    6500 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5E40FF07
P 5850 4850
F 0 "R9" V 5800 4700 50  0000 C CNN
F 1 "10k" V 5800 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 4850 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 4850 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 4850 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 4850 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 4850 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 4850 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 4850 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 4850 50  0001 C CNN "SupplierRef"
	1    5850 4850
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R10
U 1 1 5E40FF11
P 5850 5050
F 0 "R10" V 5800 4900 50  0000 C CNN
F 1 "10k" V 5800 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 5050 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 5050 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 5050 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 5050 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 5050 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 5050 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 5050 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 5050 50  0001 C CNN "SupplierRef"
	1    5850 5050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5E40FF1B
P 5850 5250
F 0 "R11" V 5800 5100 50  0000 C CNN
F 1 "10k" V 5800 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 5250 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 5250 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 5250 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 5250 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 5250 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 5250 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 5250 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 5250 50  0001 C CNN "SupplierRef"
	1    5850 5250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5E40FF25
P 5850 5450
F 0 "R12" V 5800 5300 50  0000 C CNN
F 1 "10k" V 5800 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 5450 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 5450 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 5450 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 5450 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 5450 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 5450 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 5450 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 5450 50  0001 C CNN "SupplierRef"
	1    5850 5450
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D9
U 1 1 5E40FF2F
P 6300 4950
F 0 "D9" H 6200 5000 50  0000 C CNN
F 1 "1N4148" H 6300 5064 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 4950 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 4950 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 4950 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 4950 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 4950 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 4950 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 4950 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 4950 50  0001 C CNN "SupplierRef"
	1    6300 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D10
U 1 1 5E40FF39
P 6300 5150
F 0 "D10" H 6200 5200 50  0000 C CNN
F 1 "1N4148" H 6300 5264 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 5150 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 5150 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 5150 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 5150 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 5150 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 5150 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 5150 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 5150 50  0001 C CNN "SupplierRef"
	1    6300 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D11
U 1 1 5E40FF43
P 6300 5350
F 0 "D11" H 6200 5400 50  0000 C CNN
F 1 "1N4148" H 6300 5464 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 5350 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 5350 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 5350 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 5350 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 5350 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 5350 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 5350 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 5350 50  0001 C CNN "SupplierRef"
	1    6300 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D12
U 1 1 5E40FF4D
P 6300 5550
F 0 "D12" H 6200 5600 50  0000 C CNN
F 1 "1N4148" H 6300 5664 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 5550 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 5550 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 5550 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 5550 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 5550 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 5550 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 5550 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 5550 50  0001 C CNN "SupplierRef"
	1    6300 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4950 6450 4950
Wire Wire Line
	6400 5150 6450 5150
Wire Wire Line
	6400 5350 6450 5350
Wire Wire Line
	6400 5550 6450 5550
Wire Wire Line
	6500 4850 6100 4850
Wire Wire Line
	5950 5050 6100 5050
Wire Wire Line
	6500 5250 6100 5250
Wire Wire Line
	5950 5450 6100 5450
Wire Wire Line
	6200 5550 6100 5550
Wire Wire Line
	6100 5550 6100 5450
Connection ~ 6100 5450
Wire Wire Line
	6100 5450 6500 5450
Wire Wire Line
	6200 5350 6100 5350
Wire Wire Line
	6100 5350 6100 5250
Connection ~ 6100 5250
Wire Wire Line
	6100 5250 5950 5250
Wire Wire Line
	6200 5150 6100 5150
Wire Wire Line
	6100 5150 6100 5050
Connection ~ 6100 5050
Wire Wire Line
	6100 5050 6500 5050
Wire Wire Line
	6200 4950 6100 4950
Wire Wire Line
	6100 4950 6100 4850
Connection ~ 6100 4850
Wire Wire Line
	6100 4850 5950 4850
$Comp
L power:GND #PWR03
U 1 1 5E40FF6F
P 6450 5650
F 0 "#PWR03" H 6450 5400 50  0001 C CNN
F 1 "GND" H 6455 5477 50  0000 C CNN
F 2 "" H 6450 5650 50  0001 C CNN
F 3 "" H 6450 5650 50  0001 C CNN
	1    6450 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5E40FF79
P 7500 5650
F 0 "#PWR09" H 7500 5400 50  0001 C CNN
F 1 "GND" H 7505 5477 50  0000 C CNN
F 2 "" H 7500 5650 50  0001 C CNN
F 3 "" H 7500 5650 50  0001 C CNN
	1    7500 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 5650 6450 5550
Connection ~ 6450 4950
Wire Wire Line
	6450 4950 6400 4950
Connection ~ 6450 5150
Wire Wire Line
	6450 5150 6500 5150
Wire Wire Line
	6450 5150 6450 4950
Connection ~ 6450 5350
Wire Wire Line
	6450 5350 6500 5350
Wire Wire Line
	6450 5350 6450 5150
Connection ~ 6450 5550
Wire Wire Line
	6450 5550 6500 5550
Wire Wire Line
	6450 5550 6450 5350
Wire Wire Line
	7400 4950 7500 4950
Wire Wire Line
	7500 4950 7500 5150
Wire Wire Line
	7400 5550 7500 5550
Connection ~ 7500 5550
Wire Wire Line
	7500 5550 7500 5650
Wire Wire Line
	7400 5350 7500 5350
Connection ~ 7500 5350
Wire Wire Line
	7500 5350 7500 5550
Wire Wire Line
	7400 5150 7500 5150
Connection ~ 7500 5150
Wire Wire Line
	7500 5150 7500 5350
Text GLabel 5550 4850 0    50   Input ~ 0
DIG_IN_CH9
Wire Wire Line
	5550 4850 5750 4850
Text GLabel 5550 5050 0    50   Input ~ 0
DIG_IN_CH10
Wire Wire Line
	5550 5050 5750 5050
Text GLabel 5550 5250 0    50   Input ~ 0
DIG_IN_CH11
Wire Wire Line
	5550 5250 5750 5250
Text GLabel 5550 5450 0    50   Input ~ 0
DIG_IN_CH12
Wire Wire Line
	5550 5450 5750 5450
$Comp
L Device:R_Small R13
U 1 1 5E40FFA2
P 5850 6100
F 0 "R13" V 5800 5950 50  0000 C CNN
F 1 "10k" V 5800 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 6100 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 6100 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 6100 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 6100 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 6100 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 6100 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 6100 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 6100 50  0001 C CNN "SupplierRef"
	1    5850 6100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R14
U 1 1 5E40FFAC
P 5850 6300
F 0 "R14" V 5800 6150 50  0000 C CNN
F 1 "10k" V 5800 6450 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 6300 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 6300 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 6300 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 6300 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 6300 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 6300 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 6300 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 6300 50  0001 C CNN "SupplierRef"
	1    5850 6300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 5E40FFB6
P 5850 6500
F 0 "R15" V 5800 6350 50  0000 C CNN
F 1 "10k" V 5800 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 6500 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 6500 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 6500 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 6500 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 6500 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 6500 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 6500 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 6500 50  0001 C CNN "SupplierRef"
	1    5850 6500
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R16
U 1 1 5E40FFC0
P 5850 6700
F 0 "R16" V 5800 6550 50  0000 C CNN
F 1 "10k" V 5800 6850 50  0000 C CNN
F 2 "Resistor_SMD:R_MELF_MMB-0207" H 5850 6700 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/418/NG_DS_6-1773446-2_1-1287253.pdf" H 5850 6700 50  0001 C CNN
F 4 "Résistances MELF Metal Film Resistor MELF 10K 207 1W" H 5850 6700 50  0001 C CNN "Description"
F 5 "TE Connectivity" H 5850 6700 50  0001 C CNN "Manufacturer_Name"
F 6 "SMA-A0207FTDT10K" H 5850 6700 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "279-SMAA0207FTDT10K" H 5850 6700 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 5850 6700 50  0001 C CNN "Supplier"
F 9 "279-SMAA0207FTDT10K" H 5850 6700 50  0001 C CNN "SupplierRef"
	1    5850 6700
	0    1    1    0   
$EndComp
$Comp
L Device:D_Small D13
U 1 1 5E40FFCA
P 6300 6200
F 0 "D13" H 6200 6250 50  0000 C CNN
F 1 "1N4148" H 6300 6314 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 6200 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 6200 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 6200 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 6200 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 6200 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 6200 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 6200 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 6200 50  0001 C CNN "SupplierRef"
	1    6300 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D14
U 1 1 5E40FFD4
P 6300 6400
F 0 "D14" H 6200 6450 50  0000 C CNN
F 1 "1N4148" H 6300 6514 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 6400 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 6400 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 6400 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 6400 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 6400 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 6400 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 6400 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 6400 50  0001 C CNN "SupplierRef"
	1    6300 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D15
U 1 1 5E40FFDE
P 6300 6600
F 0 "D15" H 6200 6650 50  0000 C CNN
F 1 "1N4148" H 6300 6714 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 6600 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 6600 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 6600 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 6600 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 6600 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 6600 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 6600 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 6600 50  0001 C CNN "SupplierRef"
	1    6300 6600
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D16
U 1 1 5E40FFE8
P 6300 6800
F 0 "D16" H 6200 6850 50  0000 C CNN
F 1 "1N4148" H 6300 6914 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" V 6300 6800 50  0001 C CNN
F 3 "https://www.mouser.fr/datasheet/2/115/ds30086-60872.pdf" V 6300 6800 50  0001 C CNN
F 4 "Surface mount fast switching diode Vr=100V If=300mA" H 6300 6800 50  0001 C CNN "Description"
F 5 "Diodes Incorporated" H 6300 6800 50  0001 C CNN "Manufacturer_Name"
F 6 "1N4148W-7-F" H 6300 6800 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "621-1N4148W-F" H 6300 6800 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 6300 6800 50  0001 C CNN "Supplier"
F 9 "621-1N4148W-F" H 6300 6800 50  0001 C CNN "SupplierRef"
	1    6300 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 6200 6450 6200
Wire Wire Line
	6400 6400 6450 6400
Wire Wire Line
	6400 6600 6450 6600
Wire Wire Line
	6400 6800 6450 6800
Wire Wire Line
	6500 6100 6100 6100
Wire Wire Line
	5950 6300 6100 6300
Wire Wire Line
	6500 6500 6100 6500
Wire Wire Line
	5950 6700 6100 6700
Wire Wire Line
	6200 6800 6100 6800
Wire Wire Line
	6100 6800 6100 6700
Connection ~ 6100 6700
Wire Wire Line
	6100 6700 6500 6700
Wire Wire Line
	6200 6600 6100 6600
Wire Wire Line
	6100 6600 6100 6500
Connection ~ 6100 6500
Wire Wire Line
	6100 6500 5950 6500
Wire Wire Line
	6200 6400 6100 6400
Wire Wire Line
	6100 6400 6100 6300
Connection ~ 6100 6300
Wire Wire Line
	6100 6300 6500 6300
Wire Wire Line
	6200 6200 6100 6200
Wire Wire Line
	6100 6200 6100 6100
Connection ~ 6100 6100
Wire Wire Line
	6100 6100 5950 6100
$Comp
L power:GND #PWR04
U 1 1 5E41000A
P 6450 6900
F 0 "#PWR04" H 6450 6650 50  0001 C CNN
F 1 "GND" H 6455 6727 50  0000 C CNN
F 2 "" H 6450 6900 50  0001 C CNN
F 3 "" H 6450 6900 50  0001 C CNN
	1    6450 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 6900 6450 6800
Connection ~ 6450 6200
Wire Wire Line
	6450 6200 6400 6200
Connection ~ 6450 6400
Wire Wire Line
	6450 6400 6500 6400
Wire Wire Line
	6450 6400 6450 6200
Connection ~ 6450 6600
Wire Wire Line
	6450 6600 6500 6600
Wire Wire Line
	6450 6600 6450 6400
Connection ~ 6450 6800
Wire Wire Line
	6450 6800 6500 6800
Wire Wire Line
	6450 6800 6450 6600
Text GLabel 5550 6100 0    50   Input ~ 0
DIG_IN_CH13
Wire Wire Line
	5550 6100 5750 6100
Text GLabel 5550 6300 0    50   Input ~ 0
DIG_IN_CH14
Wire Wire Line
	5550 6300 5750 6300
Text GLabel 5550 6500 0    50   Input ~ 0
DIG_IN_CH15
Wire Wire Line
	5550 6500 5750 6500
Text GLabel 5550 6700 0    50   Input ~ 0
DIG_IN_CH16
Wire Wire Line
	5550 6700 5750 6700
$Comp
L power:GND #PWR010
U 1 1 5E410028
P 7500 6900
F 0 "#PWR010" H 7500 6650 50  0001 C CNN
F 1 "GND" H 7505 6727 50  0000 C CNN
F 2 "" H 7500 6900 50  0001 C CNN
F 3 "" H 7500 6900 50  0001 C CNN
	1    7500 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 6200 7500 6200
Wire Wire Line
	7500 6200 7500 6400
Wire Wire Line
	7400 6800 7500 6800
Connection ~ 7500 6800
Wire Wire Line
	7500 6800 7500 6900
Wire Wire Line
	7400 6600 7500 6600
Connection ~ 7500 6600
Wire Wire Line
	7500 6600 7500 6800
Wire Wire Line
	7400 6400 7500 6400
Connection ~ 7500 6400
Wire Wire Line
	7500 6400 7500 6600
$Comp
L Device:R_Network08 RN2
U 1 1 5E41003D
P 7900 4500
F 0 "RN2" H 8288 4546 50  0000 L CNN
F 1 "1k" H 8288 4455 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP9" V 8375 4500 50  0001 C CNN
F 3 "https://www.bourns.com/pdfs/4600X.pdf" H 7900 4500 50  0001 C CNN
F 4 "Resistors array 8x1kOhms 2% SIP9" H 7900 4500 50  0001 C CNN "Description"
F 5 "Bourns" H 7900 4500 50  0001 C CNN "Manufacturer_Name"
F 6 "4609X-AP1-102LF" H 7900 4500 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "652-4609X-AP1-102LF" H 7900 4500 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 7900 4500 50  0001 C CNN "Supplier"
F 9 "652-4609X-AP1-102LF" H 7900 4500 50  0001 C CNN "SupplierRef"
	1    7900 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4850 7500 4850
Wire Wire Line
	7500 4850 7500 4700
Wire Wire Line
	7400 5050 7600 5050
Wire Wire Line
	7600 5050 7600 4950
Wire Wire Line
	7400 5250 7700 5250
Wire Wire Line
	7700 5250 7700 5050
Wire Wire Line
	7400 5450 7800 5450
Wire Wire Line
	7800 5450 7800 5150
Wire Wire Line
	7400 6100 7900 6100
Wire Wire Line
	7900 6100 7900 5250
Wire Wire Line
	7400 6300 8000 6300
Wire Wire Line
	8000 6300 8000 5350
Wire Wire Line
	8100 4700 8100 5450
Wire Wire Line
	8100 6500 7400 6500
Wire Wire Line
	7400 6700 8200 6700
Wire Wire Line
	8200 6700 8200 5550
$Comp
L power:+3.3V #PWR08
U 1 1 5E410057
P 7500 4250
F 0 "#PWR08" H 7500 4100 50  0001 C CNN
F 1 "+3.3V" H 7515 4423 50  0000 C CNN
F 2 "" H 7500 4250 50  0001 C CNN
F 3 "" H 7500 4250 50  0001 C CNN
	1    7500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4250 7500 4300
$Comp
L EuroPress:SN74HC540PW U2
U 1 1 5E410066
P 9000 4750
F 0 "U2" H 9550 5015 50  0000 C CNN
F 1 "SN74HC540PW" H 9550 4924 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 9950 4850 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc540.pdf" H 9950 4750 50  0001 L CNN
F 4 "Octal Buffer & Line Driver, 3-State, Inverting, 2  6 V, 20-Pin TSSOP" H 9950 4650 50  0001 L CNN "Description"
F 5 "595-SN74HC540PW" H 9950 4450 50  0001 L CNN "Mouser Part Number"
F 6 "Texas Instruments" H 9950 4350 50  0001 L CNN "Manufacturer_Name"
F 7 "SN74HC540PW" H 9950 4250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "Mouser" H 9000 4750 50  0001 C CNN "Supplier"
F 9 "595-SN74HC540PW" H 9000 4750 50  0001 C CNN "SupplierRef"
	1    9000 4750
	1    0    0    -1  
$EndComp
Text GLabel 8850 5800 0    50   Input ~ 0
CS_DIG_IN
Wire Wire Line
	8850 5800 8950 5800
Wire Wire Line
	8950 5800 8950 5750
Wire Wire Line
	8950 5750 9000 5750
Wire Wire Line
	9000 5850 8950 5850
Wire Wire Line
	8950 5850 8950 5800
Connection ~ 8950 5800
Wire Wire Line
	9000 4850 7500 4850
Connection ~ 7500 4850
Wire Wire Line
	9000 4950 7600 4950
Connection ~ 7600 4950
Wire Wire Line
	7600 4950 7600 4700
Wire Wire Line
	9000 5050 7700 5050
Connection ~ 7700 5050
Wire Wire Line
	7700 5050 7700 4700
Wire Wire Line
	9000 5150 7800 5150
Connection ~ 7800 5150
Wire Wire Line
	7800 5150 7800 4700
Wire Wire Line
	7900 5250 9000 5250
Connection ~ 7900 5250
Wire Wire Line
	7900 5250 7900 4700
Wire Wire Line
	9000 5350 8000 5350
Connection ~ 8000 5350
Wire Wire Line
	8000 5350 8000 4700
Wire Wire Line
	8100 5450 9000 5450
Connection ~ 8100 5450
Wire Wire Line
	8100 5450 8100 6500
Wire Wire Line
	9000 5550 8200 5550
Connection ~ 8200 5550
Wire Wire Line
	8200 5550 8200 4700
$Comp
L power:+3.3V #PWR017
U 1 1 5E41008E
P 10200 4650
F 0 "#PWR017" H 10200 4500 50  0001 C CNN
F 1 "+3.3V" H 10215 4823 50  0000 C CNN
F 2 "" H 10200 4650 50  0001 C CNN
F 3 "" H 10200 4650 50  0001 C CNN
	1    10200 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5E410098
P 10200 5950
F 0 "#PWR018" H 10200 5700 50  0001 C CNN
F 1 "GND" H 10205 5777 50  0000 C CNN
F 2 "" H 10200 5950 50  0001 C CNN
F 3 "" H 10200 5950 50  0001 C CNN
	1    10200 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5E4100A2
P 9550 6550
F 0 "#PWR014" H 9550 6300 50  0001 C CNN
F 1 "GND" H 9555 6377 50  0000 C CNN
F 2 "" H 9550 6550 50  0001 C CNN
F 3 "" H 9550 6550 50  0001 C CNN
	1    9550 6550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR013
U 1 1 5E4100AC
P 9550 6250
F 0 "#PWR013" H 9550 6100 50  0001 C CNN
F 1 "+3.3V" H 9565 6423 50  0000 C CNN
F 2 "" H 9550 6250 50  0001 C CNN
F 3 "" H 9550 6250 50  0001 C CNN
	1    9550 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5E4100B6
P 9550 6400
F 0 "C2" H 9642 6446 50  0000 L CNN
F 1 "0.1uF" H 9642 6355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9550 6400 50  0001 C CNN
F 3 "~" H 9550 6400 50  0001 C CNN
F 4 "Condensateur céramique multicouches MLCC - CMS 50V 0.1uF 0603 X7R 10%" H 9550 6400 50  0001 C CNN "Description"
F 5 "Kemet" H 9550 6400 50  0001 C CNN "Manufacturer_Name"
F 6 "C0603X104K5RACTU" H 9550 6400 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "80-C0603X104K5R" H 9550 6400 50  0001 C CNN "Mouser Part Number"
F 8 "Mouser" H 9550 6400 50  0001 C CNN "Supplier"
F 9 "80-C0603X104K5R" H 9550 6400 50  0001 C CNN "SupplierRef"
	1    9550 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 6250 9550 6300
Wire Wire Line
	9550 6500 9550 6550
Wire Wire Line
	10100 4750 10200 4750
Wire Wire Line
	10200 4750 10200 4650
Wire Wire Line
	10100 5850 10200 5850
Wire Wire Line
	10200 5850 10200 5950
Text GLabel 10400 5050 2    50   Output ~ 0
PP_IN9
Wire Wire Line
	10100 4950 10400 4950
Text GLabel 10400 5150 2    50   Output ~ 0
PP_IN10
Wire Wire Line
	10100 5050 10400 5050
Text GLabel 10400 5250 2    50   Output ~ 0
PP_IN11
Wire Wire Line
	10100 5150 10400 5150
Text GLabel 10400 5350 2    50   Output ~ 0
PP_IN12
Wire Wire Line
	10100 5250 10400 5250
Text GLabel 10400 5450 2    50   Output ~ 0
PP_IN13
Wire Wire Line
	10100 5350 10400 5350
Text GLabel 10400 5550 2    50   Output ~ 0
PP_IN14
Wire Wire Line
	10100 5450 10400 5450
Text GLabel 10400 5650 2    50   Output ~ 0
PP_IN15
Wire Wire Line
	10100 5550 10400 5550
Text GLabel 10400 4950 2    50   Output ~ 0
PP_IN8
Wire Wire Line
	10100 5650 10400 5650
Text Notes 5350 4400 0    50   ~ 0
R1-R16 are powered through 24V\nso they should be rated to dissipate this power.\nP = U²/R = 58mW
Text GLabel 12050 2950 0    50   Output ~ 0
DIG_IN_CH1
Text GLabel 12050 3050 0    50   Output ~ 0
DIG_IN_CH2
Text GLabel 12050 3150 0    50   Output ~ 0
DIG_IN_CH3
Text GLabel 12050 3250 0    50   Output ~ 0
DIG_IN_CH4
Text GLabel 12050 3350 0    50   Output ~ 0
DIG_IN_CH5
Text GLabel 12050 3450 0    50   Output ~ 0
DIG_IN_CH6
Text GLabel 12050 3550 0    50   Output ~ 0
DIG_IN_CH7
Text GLabel 12050 3650 0    50   Output ~ 0
DIG_IN_CH8
Text GLabel 12050 3750 0    50   Output ~ 0
DIG_IN_CH9
Text GLabel 12050 3850 0    50   Output ~ 0
DIG_IN_CH10
Text GLabel 12050 3950 0    50   Output ~ 0
DIG_IN_CH11
Text GLabel 12050 4050 0    50   Output ~ 0
DIG_IN_CH12
Text GLabel 12050 4150 0    50   Output ~ 0
DIG_IN_CH13
Text GLabel 12050 4250 0    50   Output ~ 0
DIG_IN_CH14
Text GLabel 12050 4350 0    50   Output ~ 0
DIG_IN_CH15
Text GLabel 12050 4450 0    50   Output ~ 0
DIG_IN_CH16
$Comp
L power:GND #PWR022
U 1 1 5E4A006A
P 12950 4500
F 0 "#PWR022" H 12950 4250 50  0001 C CNN
F 1 "GND" H 12955 4327 50  0000 C CNN
F 2 "" H 12950 4500 50  0001 C CNN
F 3 "" H 12950 4500 50  0001 C CNN
	1    12950 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR021
U 1 1 5E4A155C
P 12950 2900
F 0 "#PWR021" H 12950 2750 50  0001 C CNN
F 1 "+24V" H 12965 3073 50  0000 C CNN
F 2 "" H 12950 2900 50  0001 C CNN
F 3 "" H 12950 2900 50  0001 C CNN
	1    12950 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	12800 2950 12950 2950
Wire Wire Line
	12950 2950 12950 2900
Wire Wire Line
	12050 3250 12300 3250
Wire Wire Line
	12050 3150 12300 3150
Wire Wire Line
	12050 3050 12300 3050
Wire Wire Line
	12050 4450 12300 4450
Wire Wire Line
	12050 4350 12300 4350
Wire Wire Line
	12050 4250 12300 4250
Wire Wire Line
	12050 4150 12300 4150
Wire Wire Line
	12050 4050 12300 4050
Wire Wire Line
	12050 3950 12300 3950
Wire Wire Line
	12050 3850 12300 3850
Wire Wire Line
	12050 2950 12300 2950
Wire Wire Line
	12050 3750 12300 3750
Wire Wire Line
	12050 3650 12300 3650
Wire Wire Line
	12050 3550 12300 3550
Wire Wire Line
	12050 3450 12300 3450
Wire Wire Line
	12050 3350 12300 3350
$Comp
L Connector_Generic:Conn_02x16_Odd_Even J1
U 1 1 5E43C13F
P 12600 3650
F 0 "J1" H 12650 4567 50  0000 C CNN
F 1 "Digital inputs" H 12650 4476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x16_P2.54mm_Vertical" H 12600 3650 50  0001 C CNN
F 3 "~" H 12600 3650 50  0001 C CNN
F 4 ".100\" Mini Mate® Isolated Power Terminal Strip, Cable Mate, double rows, 16 positions" H 12600 3650 50  0001 C CNN "Description"
F 5 "Samtec" H 12600 3650 50  0001 C CNN "Manufacturer_Name"
F 6 "IPL1-116-01-F-D-K" H 12600 3650 50  0001 C CNN "Manufacturer_Part_Number"
F 7 "N/A" H 12600 3650 50  0001 C CNN "Mouser Part Number"
F 8 "Samtec" H 12600 3650 50  0001 C CNN "Supplier"
F 9 "IPL1-116-01-F-D-K" H 12600 3650 50  0001 C CNN "SupplierRef"
	1    12600 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	12800 3750 12950 3750
Wire Wire Line
	12950 3750 12950 3850
Wire Wire Line
	12800 3650 12950 3650
Wire Wire Line
	12950 3650 12950 3550
Connection ~ 12950 2950
Wire Wire Line
	12800 3050 12950 3050
Connection ~ 12950 3050
Wire Wire Line
	12950 3050 12950 2950
Wire Wire Line
	12800 3150 12950 3150
Connection ~ 12950 3150
Wire Wire Line
	12950 3150 12950 3050
Wire Wire Line
	12800 3250 12950 3250
Connection ~ 12950 3250
Wire Wire Line
	12950 3250 12950 3150
Wire Wire Line
	12800 3350 12950 3350
Connection ~ 12950 3350
Wire Wire Line
	12950 3350 12950 3250
Wire Wire Line
	12800 3450 12950 3450
Connection ~ 12950 3450
Wire Wire Line
	12950 3450 12950 3350
Wire Wire Line
	12800 3550 12950 3550
Connection ~ 12950 3550
Wire Wire Line
	12950 3550 12950 3450
Wire Wire Line
	12800 3850 12950 3850
Connection ~ 12950 3850
Wire Wire Line
	12950 3850 12950 3950
Wire Wire Line
	12800 3950 12950 3950
Connection ~ 12950 3950
Wire Wire Line
	12950 3950 12950 4050
Wire Wire Line
	12800 4050 12950 4050
Connection ~ 12950 4050
Wire Wire Line
	12950 4050 12950 4150
Wire Wire Line
	12800 4150 12950 4150
Connection ~ 12950 4150
Wire Wire Line
	12950 4150 12950 4250
Wire Wire Line
	12800 4250 12950 4250
Connection ~ 12950 4250
Wire Wire Line
	12950 4250 12950 4350
Wire Wire Line
	12800 4350 12950 4350
Connection ~ 12950 4350
Wire Wire Line
	12950 4350 12950 4450
Wire Wire Line
	12800 4450 12950 4450
Connection ~ 12950 4450
Wire Wire Line
	12950 4450 12950 4500
$Comp
L power:GND #PWR020
U 1 1 5E638093
P 2750 5200
F 0 "#PWR020" H 2750 4950 50  0001 C CNN
F 1 "GND" V 2755 5072 50  0000 R CNN
F 2 "" H 2750 5200 50  0001 C CNN
F 3 "" H 2750 5200 50  0001 C CNN
	1    2750 5200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR019
U 1 1 5E6393BC
P 2900 2750
F 0 "#PWR019" H 2900 2600 50  0001 C CNN
F 1 "+3.3V" V 2915 2878 50  0000 L CNN
F 2 "" H 2900 2750 50  0001 C CNN
F 3 "" H 2900 2750 50  0001 C CNN
	1    2900 2750
	1    0    0    -1  
$EndComp
Text GLabel 3900 3200 2    50   Output ~ 0
PP_IN0
Text GLabel 3900 3300 2    50   Output ~ 0
PP_IN1
Text GLabel 3900 3400 2    50   Output ~ 0
PP_IN2
Text GLabel 3900 3500 2    50   Output ~ 0
PP_IN3
Text GLabel 3900 3600 2    50   Output ~ 0
PP_IN4
Text GLabel 3900 3700 2    50   Output ~ 0
PP_IN5
Text GLabel 3900 3800 2    50   Output ~ 0
PP_IN6
Text GLabel 3900 3900 2    50   Output ~ 0
PP_IN7
Text GLabel 3900 4000 2    50   Output ~ 0
PP_IN8
Text GLabel 3900 4100 2    50   Output ~ 0
PP_IN9
Text GLabel 3900 4200 2    50   Output ~ 0
PP_IN10
Text GLabel 3900 4300 2    50   Output ~ 0
PP_IN11
Text GLabel 3900 4400 2    50   Output ~ 0
PP_IN12
Text GLabel 3900 4500 2    50   Output ~ 0
PP_IN13
Text GLabel 3900 4600 2    50   Output ~ 0
PP_IN14
Text GLabel 3900 4700 2    50   Output ~ 0
PP_IN15
Wire Wire Line
	3750 3800 3900 3800
Wire Wire Line
	3900 3900 3750 3900
Wire Wire Line
	3750 3600 3900 3600
Wire Wire Line
	3900 3700 3750 3700
Wire Wire Line
	3750 3400 3900 3400
Wire Wire Line
	3750 3500 3900 3500
Wire Wire Line
	3750 3300 3900 3300
Wire Wire Line
	3750 4700 3900 4700
Wire Wire Line
	3900 4600 3750 4600
Wire Wire Line
	3750 4500 3900 4500
Wire Wire Line
	3900 4400 3750 4400
Wire Wire Line
	3750 4300 3900 4300
Wire Wire Line
	3900 4200 3750 4200
Wire Wire Line
	3750 4100 3900 4100
Wire Wire Line
	3900 4000 3750 4000
Text GLabel 1600 3750 0    50   Output ~ 0
CS_DIG_IN
$Comp
L EuroPress:EuroPress_InputsBoard XA1
U 1 1 5E38E749
P 2750 3950
F 0 "XA1" H 2050 4850 50  0000 L CNN
F 1 "EuroPress_InputsBoard" H 3000 3050 50  0000 L CNN
F 2 "EuroPress:EuroPress_InputsBoard" V 2950 3950 50  0001 C CNN
F 3 "" H 3150 4450 50  0001 C CNN
	1    2750 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3750 1750 3750
Wire Wire Line
	3750 3200 3900 3200
Wire Wire Line
	2600 5100 2600 5150
Wire Wire Line
	2600 5150 2750 5150
Wire Wire Line
	2900 5150 2900 5100
Wire Wire Line
	2750 5100 2750 5150
Connection ~ 2750 5150
Wire Wire Line
	2750 5150 2900 5150
Wire Wire Line
	2750 5150 2750 5200
Wire Wire Line
	2900 2750 2900 2800
NoConn ~ 2750 2800
$Comp
L power:+24V #PWR0101
U 1 1 5E5E80C2
P 2600 2750
F 0 "#PWR0101" H 2600 2600 50  0001 C CNN
F 1 "+24V" H 2615 2923 50  0000 C CNN
F 2 "" H 2600 2750 50  0001 C CNN
F 3 "" H 2600 2750 50  0001 C CNN
	1    2600 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2750 2600 2800
NoConn ~ 1750 3850
NoConn ~ 1750 4050
NoConn ~ 1750 4150
NoConn ~ 1750 4250
$EndSCHEMATC
