#include "io_encoder.h"

void setupQDEC0(bool xSwap){
	// Setup Quadrature Encoder (Timer Counter)

	// SAM3X/SAM3A Series Datasheet : http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-11057-32-bit-Cortex-M3-Microcontroller-SAM3X-SAM3A_Datasheet.pdf
	// Section 36 p856
	// Section 36.6.14 p871 Quadrature Decoder
	// Section 36.6.14.4 p876 Position and Rotation Measurement
	// http://forum.arduino.cc/index.php?topic=140205.30

	// Only channel 0 of TC0 is used here to get the position of the quadrature encoder
	// On the Arduino Due, the encoder channel A should be wired to pin 2 (TIOA0) and channel B to pin 13 (TIOB0)

	// The current position value is stored on REG_TC0_CV0
	
	pmc_enable_periph_clk(ID_TC0);

	// TC Channel Mode Register
	REG_TC0_CMR0 = TC_CMR_TCCLKS_XC0; // Select XC0 as clock source

	// TC Block Mode Register
	REG_TC0_BMR = TC_BMR_QDEN     // QDEC mode enabled
	            | TC_BMR_POSEN    // Position measure is enabled
	            | TC_BMR_EDGPHA  // Detect quadrature on both PHA and PHB (4X decoding)
				| (xSwap ? TC_BMR_SWAP : 0x00);  // Swap PHA and PHB internally

	// TC Channel Control Register
	REG_TC0_CCR0 = TC_CCR_CLKEN   // Enable the clock
	             | TC_CCR_SWTRG;  // and reset the counter
}

void resetQDEC0(){
	// Set the Quadrature Encoder position value to 0
	// NOTE : The counter value (REG_TC0_CV0) passes to zero only
	// on the next valid edge of the selected clock. NOT IMMEDIATELY !

	REG_TC0_CCR0 |=  TC_CCR_SWTRG; // Reset the QDEC counter value
}

uint16_t setupMAX14890E(bool xSingleEnded){
	// Setup MAX14890E Incremental Encoder Interface

	uint16_t ui16response;

	// Sending command
	digitalWriteFast(CS_ENCODER, LOW);
	if (xSingleEnded)  // Single-ended HTL input
		SPI.transfer16(0b1011010100000001);
	else// Differential HTL input
		SPI.transfer16(0b1011000000000001);
	digitalWriteFast(CS_ENCODER, HIGH);

	// Read response on SPI follow-on cycle
	// Response is detailled fault status
	digitalWriteFast(CS_ENCODER, LOW);
	ui16response = SPI.transfer16(0x0000);
	digitalWriteFast(CS_ENCODER, HIGH);

	return ui16response;
}

uint16_t readMAX14890EConfig(){
	// Setup MAX14890E Incremental Encoder Interface

	uint16_t ui16response;

	// Sending command
	digitalWriteFast(CS_ENCODER, LOW);
	SPI.transfer16(0b1110000000000000);
	digitalWriteFast(CS_ENCODER, HIGH);

	// Read response on SPI follow-on cycle
	digitalWriteFast(CS_ENCODER, LOW);
	ui16response = SPI.transfer16(0x0000);
	digitalWriteFast(CS_ENCODER, HIGH);

	return ui16response;
}
