#include <digitalWriteFast.h>
#include <DmaSerial.h>
#include <SPI.h>

#include "DueTimer.h"
#include "EuroPressHMI.h"
#include "EuroPressPLC.h"
#include "europress_flash.h"
#include "europress_io.h"
#include "europress_utils.h"
#include "GVL.h"
#include "io_encoder.h"


/*
 * Variables used for cycle timing
 */
typedef struct __attribute__((packed)) {
	bool x1ms;
	bool x100ms;
	volatile bool xInterrupt_1ms;
	volatile bool xInterrupt_100ms;
} T_TICK;

T_TICK stTicks;
GVL_VARS stVars;
DmaSerial dma_serial2 = DmaSerial((Uart*)USART1, ID_USART1);
EuroPressPLC Plc(&stVars);
EuroPressHMI Hmi;

void setup() {
	noInterrupts();

	// Ensure u8CurentRecipe is between 0-9
	stVars.u8CurentRecipe = 0;

	Serial.begin(115200);  // Debug console

	setupInputs();
	setupOutputs();	

	// Configure SPI communication
	// Used for encoder (MAX14890E)
	SPI.begin();
	SPI.setDataMode(SPI_MODE0);

	// Configuring ModbusRTU communication
	Hmi.begin(1, dma_serial2);
	Hmi.idle(ModbusIdle_handler);
	
	setupFlashMemory();

	// Get machine parameters from flash on startup
	readParamsFromFlash(&stVars.param);
	strcpy(stVars.param.sVersionDate, "V1.3");

	setupQDEC0(stVars.param.xEncoderInv);
	setupMAX14890E(stVars.param.xEncoderSE);

	// Call Timer3_handler every ms (1000µs): PLC cycle
	Timer3.attachInterrupt(ISR_Timer3_handler).start(1000);

	// Call Timer4_handler every 100 ms: HMI communication
	Timer4.attachInterrupt(ISR_Timer4_handler).start(100000);

	pinMode(OUT_TPS_CYCLE, OUTPUT); // Pin used to monitor the cycle time
	pinMode(OUT_TPS_CYCLE2, OUTPUT); // Pin used to monitor the cycle time

	interrupts();
	
	// Get recipes from flash on startup
	for(uint8_t i=0; i < 10; i++) {
		 readRecipeFromFlash(&stVars.arRecipes[i], i);
	}

	// Load last used recipe
	stVars.u8CurentRecipe = getLastRecipeId();
	stVars.process.pRecipe = &stVars.arRecipes[stVars.u8CurentRecipe];

	// Load counters values (parts ok, parts nok, cycles) from memory at startup
	getLastCountersValues(&stVars.process);
}

void loop() {
	setTicks();

	Hmi.getRegistersValues(&stVars);
	
	// Read and write data to HMI every 100ms
	if(stTicks.x100ms) {
		stTicks.x100ms = false;
		Hmi.readWrite(&stVars);
	}

	// Run PLC cycle each millisecond
	if(stTicks.x1ms) {
		stTicks.x1ms = false;
		#ifdef DEBUG
			digitalWriteFast(OUT_TPS_CYCLE, HIGH);  // Cycle time monitoring
		#endif
		Plc.run();
		#ifdef DEBUG
			digitalWriteFast(OUT_TPS_CYCLE, LOW);
		#endif
	}
	
	resetTicks();
}

void ISR_Timer3_handler() {
	stTicks.xInterrupt_1ms = true;
}

void ISR_Timer4_handler() {
	stTicks.xInterrupt_100ms = true;
}

void setTicks() {
	noInterrupts();
	if(stTicks.xInterrupt_1ms) {
		stTicks.xInterrupt_1ms = false;
		stTicks.x1ms = true;
	}
	if(stTicks.xInterrupt_100ms) {
		stTicks.xInterrupt_100ms = false;
		stTicks.x100ms = true;
	}
	interrupts();
}

void resetTicks() {
	if(stTicks.x1ms) {
		stTicks.x1ms = false;
	}
	if(stTicks.x100ms) {
		stTicks.x100ms = false;
	}
}

// This callback is called when Modbus master waits for Slave response
void ModbusIdle_handler() {
	// TODO: Delete this handler and directly use ModbusRtu.idle(loop) ?
	setTicks();
	if(stTicks.x1ms) {
		stTicks.x1ms = false;
		#ifdef DEBUG
			digitalWriteFast(OUT_TPS_CYCLE, HIGH);
			digitalWriteFast(OUT_TPS_CYCLE2, HIGH);
		#endif
		Plc.run();
		#ifdef DEBUG
			digitalWriteFast(OUT_TPS_CYCLE, LOW);
			digitalWriteFast(OUT_TPS_CYCLE2, LOW);
		#endif
	}
}
