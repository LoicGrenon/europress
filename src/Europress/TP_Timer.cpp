#include "TP_Timer.h"

/*
	// TP example
	TP_Timer stTP;
	stTP.timer(vars->io.I_xStartCycle, 1000);
	vars->io.O_xGeneratorRun = stTP.xOut;
*/

void TP_Timer::timer(bool xIn, uint32_t u32Time) {
	if(xIn && !this->_xInOld) { // On rising edge
		this->_xInOld = true;
		_u32StartT = millis();
	}

	if(this->_xInOld) {
		this->u32ElapsedTime = millis() - _u32StartT;
		
		if (this->u32ElapsedTime >= u32Time) {
			this->u32ElapsedTime = u32Time;
			this->xOut = false;
			this->_xInOld = false;
		} else {
			this->xOut = true;
		}
	}
}
