#ifndef _EUROPRESS_FLASH_H
#define _EUROPRESS_FLASH_H

#include <Arduino.h>
#include <digitalWriteFast.h>
#include <SPI.h>
#include <SPIMemory.h>
#include "GVL.h"

#include "europress_io.h"


#define FLASH_PARAM_START_ADDR 0
#define FLASH_RCP_START_ADDR 1024
#define FLASH_RCP_SIZE_BYTES 1024
#define FLASH_LAST_RCP_ID_ADDR 11264
#define FLASH_GOOD_PARTS_CNT_ADDR (FLASH_LAST_RCP_ID_ADDR + 1)
#define FLASH_BAD_PARTS_CNT_ADDR (FLASH_GOOD_PARTS_CNT_ADDR + 4)
#define FLASH_CYCLES_CNT_ADDR (FLASH_BAD_PARTS_CNT_ADDR + 4)


void flashFactoryRestore(GVL_VARS * pVars);
void getLastCountersValues(GVL_PROCESS * pProcess);
uint8_t getLastRecipeId(void);
void readParamsFromFlash(GVL_PARAM * pParams);
void readRecipeFromFlash(RCP_DATA * stRcpData, uint8_t u8RecipeId=0xFF);
void setupFlashMemory(void);
void storeCounters(GVL_PROCESS * pProcess);
void writeLastRecipeID(uint8_t u8RecipeId);
void writeParamsToFlash(GVL_PARAM * pParams);
void writeRecipeToFlash(RCP_DATA * stRcpData);
void writeDefaultRecipesToFlash(void);

void _checkRecipeValue(RCP_DATA * stRcpData, uint8_t u8RecipeId);


#endif // #ifndef _EUROPRESS_FLASH_H
