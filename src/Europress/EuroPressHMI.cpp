#include "EuroPressHMI.h"


EuroPressHMI::EuroPressHMI(void)
{}

void EuroPressHMI::begin(uint8_t u8Slave, DmaSerial &serial) {
	serial.begin(115200, DMA_SERIAL_8E1);
	modbus.begin(u8Slave, serial);
}

/*
 * Get the register values previously reads with readRegisters()
 * and store them to the given stRcpData structure
 */
void EuroPressHMI::getRegistersValues(GVL_VARS * stVars) {
	if(isAvailable() && _xRegistersReadDone && _u8RdResult == modbus.ku8MBSuccess) {
		_u8RdResult = 0xFF;
		// Walk through the Modbus registers previously reads with EuroPressHMI::readRegisters()
		// (They are stored into the ResponseBuffer)
		for (uint8_t u8ChunkId=0; u8ChunkId < _ku8RdChunkSize; u8ChunkId++)
		{
			uint8_t u8AddressOffset = _u8RdChunkOffsetPrev + u8ChunkId;
			if (u8AddressOffset >= HMI_RCP_BLOCK_START && u8AddressOffset <= HMI_RCP_BLOCK_END) {
				_getRecipeValues(u8AddressOffset, u8ChunkId, &stVars->hmi.stRecipe);
			} else if (u8AddressOffset >= HMI_PARAMS_BLOCK_START && u8AddressOffset <= HMI_PARAMS_BLOCK_END) {
				_getParamValues(u8AddressOffset, u8ChunkId, &stVars->hmi.stParam);
			} else {
				switch(u8AddressOffset) {
					case BTN_ADDR:
						_getButtonsState(u8AddressOffset, u8ChunkId, stVars);
						break;
					case RCP_SELECTED_ADDR:
						stVars->hmi.u8RecipeSelected = modbus.getResponseBuffer(u8ChunkId);
						break;
					case DO_CMD_ADDR:
						_getOutCmdButtonsState(u8ChunkId, stVars);
						break;
					case AO1_VALUE_ADDR:
						stVars->hmi.u16Aout1Value = modbus.getResponseBuffer(u8ChunkId);
						break;
					case AO2_VALUE_ADDR:
						stVars->hmi.u16Aout2Value = modbus.getResponseBuffer(u8ChunkId);
						break;
					case HMI_CUR_SCREEN_ADDR:
						stVars->hmi.u16CurScreen = modbus.getResponseBuffer(u8ChunkId);
						break;
				}
			}
		}
	}
}

void EuroPressHMI::idle(void (*idle)())
{
	modbus.idle(idle);
}

bool EuroPressHMI::isAvailable() {
	return _u8WrRecipeResult == modbus.ku8MBSuccess && _u8RdResult != modbus.ku8MBResponseTimedOut ? true : false;
}

void EuroPressHMI::readWrite(GVL_VARS * stVars) {
	if(isAvailable()) {
		_readRegisters();

		if(stVars->hmi.xRcpSaveBtn) {  // Save recipe currently edited on HMI to flash
			writeRecipeToFlash(&stVars->hmi.stRecipe);
			// Reload recipe values
			readRecipeFromFlash(&stVars->arRecipes[stVars->u8CurentRecipe], stVars->u8CurentRecipe);
		}
		if(stVars->hmi.xRcpLoadBtn) {  // Send recipe to HMI
			if(stVars->hmi.u8RecipeSelected >= 0 && stVars->hmi.u8RecipeSelected < 10) {
				stVars->hmi.stRecipe = stVars->arRecipes[stVars->hmi.u8RecipeSelected];
				_writeRecipeRegisters(&stVars->hmi.stRecipe);
			}
		}
		if(stVars->hmi.xRcpSelectBtn) {  // Use recipe selected on HMI
			// TODO: Ne charger la recette que si on n'est pas en cycle sinon attendre la fin de cycle
			if(stVars->hmi.u8RecipeSelected >= 0 && stVars->hmi.u8RecipeSelected < 10) {
				stVars->u8CurentRecipe = stVars->hmi.u8RecipeSelected;
				writeLastRecipeID(stVars->u8CurentRecipe);
			}
		}
		if(stVars->hmi.xParamSaveBtn) {
			stVars->param = stVars->hmi.stParam;
			writeParamsToFlash(&stVars->param);
		}
		if(stVars->hmi.xParamLoadBtn) {
			stVars->hmi.stParam = stVars->param;
			_writeParamRegisters(&stVars->hmi.stParam);
		}
		if(stVars->hmi.xParamRestoreBtn) {
			flashFactoryRestore(stVars);
		}
		if(stVars->hmi.xAckBtn) {  // Fault acknowledgement
			stVars->process.u16Faults = 0;
		}
		if(stVars->hmi.xCntRstBtn) {  // Counters reset
			stVars->process.u32PartsOk = 0;
			stVars->process.u32PartsNok = 0;
			storeCounters(&(stVars->process));
		}
	} else {  // Executed once at startup and on communication timeout
		_writeRecipeRegisters(&stVars->arRecipes[stVars->u8CurentRecipe]);  // Send values to HMI
	}
	_clearButtonsState(stVars);
	_writeDefAlmMsg(stVars);
	if(stVars->hmi.u16CurScreen == 2) {
		_writeIOStatus(stVars);
		_writeOutCmdStatus(stVars);
	} else if(stVars->hmi.u16CurScreen == 5) {
		_writeSfcStatus(stVars);
	} else if(stVars->hmi.u16CurScreen == 0) {
		_writeHomePageValues(stVars);
	}
}

void EuroPressHMI::_clearButtonsState(GVL_VARS * stVars) {
	if(stVars->hmi.xAckBtn || stVars->hmi.xCntRstBtn
	   || stVars->hmi.xRcpSaveBtn || stVars->hmi.xRcpLoadBtn || stVars->hmi.xRcpSelectBtn
	   || stVars->hmi.xParamSaveBtn || stVars->hmi.xParamLoadBtn || stVars->hmi.xParamRestoreBtn) {
		modbus.writeSingleRegister(BTN_ADDR, 0);

		stVars->hmi.xAckBtn = false;
		stVars->hmi.xCntRstBtn = false;
		stVars->hmi.xRcpSaveBtn = false;
		stVars->hmi.xRcpLoadBtn = false;
		stVars->hmi.xRcpSelectBtn = false;
		stVars->hmi.xParamSaveBtn = false;
		stVars->hmi.xParamLoadBtn = false;
		stVars->hmi.xParamRestoreBtn = false;
	}
}

/*
 * This method is called by EuroPressHMI::getRegistersValues()
 * and is used to write the machine parameters values from HMI to µC structure
 */
void EuroPressHMI::_getParamValues(uint8_t u8AddressOffset, uint8_t u8BufferIdx, GVL_PARAM * pParams) {
	switch(u8AddressOffset) {
		case HMI_PARAMS_ENCODER_CONFIG:
			pParams->xEncoderInv = modbus.getResponseBuffer(u8BufferIdx) & PARAMS_ENCODER_INV ? true : false;
			pParams->xEncoderSE = modbus.getResponseBuffer(u8BufferIdx) & PARAMS_ENCODER_SE ? true : false;
			break;
		case HMI_PARAMS_ENCODER_RESOLUTION:
			pParams->i16EncoderResolution = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAMS_GENERATOR_POWER:
			pParams->u16GeneratorMaxPower = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAMS_CYCLE_TIMEOUT:
			pParams->u8CycleTimeout = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAM_AO1_XMIN:
			pParams->u16GeneratorAmplitudeMin_Points = (uint16_t)round((float)modbus.getResponseBuffer(u8BufferIdx) * 655.35);
			break;
		case HMI_PARAM_AO1_XMAX:
			pParams->u16GeneratorAmplitudeMax_Points = (uint16_t)round((float)modbus.getResponseBuffer(u8BufferIdx) * 655.35);
			break;
		case HMI_PARAM_AO1_YMIN:
			pParams->u16GeneratorAmplitudeMin_Percent = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAM_AO1_YMAX:
			pParams->u16GeneratorAmplitudeMax_Percent = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAM_AO2_XMIN:
			pParams->u16PressRegulatorOutMin_Points = (uint16_t)round((float)modbus.getResponseBuffer(u8BufferIdx) * 655.35);
			break;
		case HMI_PARAM_AO2_XMAX:
			pParams->u16PressRegulatorOutMax_Points = (uint16_t)round((float)modbus.getResponseBuffer(u8BufferIdx) * 655.35);
			break;
		case HMI_PARAM_AO2_YMIN:
			pParams->u16PressRegulatorOutMin_Bar = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAM_AO2_YMAX:
			pParams->u16PressRegulatorOutMax_Bar = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAM_AI1_XMIN:
			pParams->u16PressRegulatorInMin_Points = (uint16_t)round((float)modbus.getResponseBuffer(u8BufferIdx) * 40.96);
			break;
		case HMI_PARAM_AI1_XMAX:
			pParams->u16PressRegulatorInMax_Points = (uint16_t)round((float)modbus.getResponseBuffer(u8BufferIdx) * 40.96);
			break;
		case HMI_PARAM_AI1_YMIN:
			pParams->u16PressRegulatorInMin_Bar = modbus.getResponseBuffer(u8BufferIdx);
			break;
		case HMI_PARAM_AI1_YMAX:
			pParams->u16PressRegulatorInMax_Bar = modbus.getResponseBuffer(u8BufferIdx);
			break;
	}
}

/*
 * This method is called by EuroPressHMI::getRegistersValues()
 * and is used to write the recipe values from HMI to µC structure
 */
void EuroPressHMI::_getRecipeValues(uint8_t u8AddressOffset, uint8_t u8BufferIdx, RCP_DATA * stRcpData) {
	if (u8AddressOffset >= RCP_NAME_0_1 && u8AddressOffset <= RCP_NAME_14_15) {
		// The recipe name has a maximum length of 16 chararacters
		// and 2 characters can be stored on each 16-bits Modbus register.
		// So the recipe name is stored on 8 registers (RCP_NAME_0_1 to RCP_NAME_14_15).
		uint8_t u8LowCharOffset = (u8AddressOffset - RCP_NAME_0_1) * 2;
		stRcpData->sName[u8LowCharOffset] = lowByte(modbus.getResponseBuffer(u8BufferIdx));
		stRcpData->sName[u8LowCharOffset + 1] = highByte(modbus.getResponseBuffer(u8BufferIdx));
	} else {
		switch(u8AddressOffset) {
			case RCP_ID:
				stRcpData->u8Id = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_MODE:
				stRcpData->u8Mode = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_AMPLITUDE:
				stRcpData->u8Amplitude = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_DELAY:
				stRcpData->u16StartDelayTime = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_START_POS:
				stRcpData->u16StartPos = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_TIME:
				stRcpData->u16WeldingTime = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_POWER:
				stRcpData->u16WeldingPower = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_ENERGY:
				stRcpData->u16WeldingEnergy = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_DISTANCE:
				stRcpData->u16WeldingDistance = (int16_t)modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_DEPTH:
				stRcpData->u16WeldingDepth = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_COOLING:
				stRcpData->u16CoolingTime = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_DESC_PRESSURE:
				stRcpData->u16DescentPressure = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_WELD_PRESSURE:
				stRcpData->u16WeldingPressure = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_COOL_PRESSURE:
				stRcpData->u16CoolingPressure = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_PULSE_DELAY:
				stRcpData->u16PulseDelay = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_PULSE_WIDTH:
				stRcpData->u16PulseWidth = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_LIMITS_MODE:
				stRcpData->xTimeLimit = modbus.getResponseBuffer(u8BufferIdx) & RCP_TIME_LIMIT_MASK ? true : false;
				stRcpData->xPowerLimit = modbus.getResponseBuffer(u8BufferIdx) & RCP_POWER_LIMIT_MASK ? true : false;
				stRcpData->xEnergyLimit = modbus.getResponseBuffer(u8BufferIdx) & RCP_ENERGY_LIMIT_MASK ? true : false;
				stRcpData->xDepthLimit = modbus.getResponseBuffer(u8BufferIdx) & RCP_DEPTH_LIMIT_MASK ? true : false;
				stRcpData->xPartPresence1 = modbus.getResponseBuffer(u8BufferIdx) & RCP_PART_PRES_1 ? true : false;
				stRcpData->xPartPresence2 = modbus.getResponseBuffer(u8BufferIdx) & RCP_PART_PRES_2 ? true : false;
				break;
			case RCP_TIME_MIN:
				stRcpData->u16TimeMinLimit = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_TIME_MAX:
				stRcpData->u16TimeMaxLimit = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_POWER_MIN:
				stRcpData->u16PowerMinLimit = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_POWER_MAX:
				stRcpData->u16PowerMaxLimit = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_ENERGY_MIN:
				stRcpData->u16EnergyMinLimit = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_ENERGY_MAX:
				stRcpData->u16EnergyMaxLimit = modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_DEPTH_MIN:
				stRcpData->i16DepthMinLimit = (int16_t)modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_DEPTH_MAX:
				stRcpData->i16DepthMaxLimit = (int16_t)modbus.getResponseBuffer(u8BufferIdx);
				break;
			case RCP_PART_POSITION:
				stRcpData->u16PartPos = modbus.getResponseBuffer(u8BufferIdx);
				break;
		}
	}
}

/*
 * This method is called by EuroPressHMI::getRegistersValues()
 * and is used to write the button status from HMI to µC structure
 */
void EuroPressHMI::_getButtonsState(uint8_t u8ModbusAddress, uint8_t u8BufferIdx, GVL_VARS * stVars) {
	uint16_t u16ButtonsState;
	if (u8ModbusAddress == BTN_ADDR) {
		u16ButtonsState = modbus.getResponseBuffer(u8BufferIdx);

		stVars->hmi.xAckBtn = u16ButtonsState & (1 << ACK_BTN);
		stVars->hmi.xCntRstBtn = u16ButtonsState & (1 << CNT_RST_BTN);
		stVars->hmi.xRcpSaveBtn = u16ButtonsState & (1 << RCP_SAVE_BTN);
		stVars->hmi.xRcpLoadBtn = u16ButtonsState & (1 << RCP_LOAD_BTN);
		stVars->hmi.xRcpSelectBtn = u16ButtonsState & (1 << RCP_SELECT_BTN);
		stVars->hmi.xParamSaveBtn = u16ButtonsState & (1 << PARAM_SAVE_BTN);
		stVars->hmi.xParamLoadBtn = u16ButtonsState & (1 << PARAM_LOAD_BTN);
		stVars->hmi.xParamRestoreBtn = u16ButtonsState & (1 << PARAM_RESTORE_BTN);
	}
}

void EuroPressHMI::_getOutCmdButtonsState(uint8_t u8BufferIdx, GVL_VARS * stVars) {
	uint16_t u16ButtonsState = modbus.getResponseBuffer(u8BufferIdx);

	stVars->hmi.xOut1Cmd = u16ButtonsState & 0b00000001;
	stVars->hmi.xOut2Cmd = u16ButtonsState & 0b00000010;
	stVars->hmi.xOut3Cmd = u16ButtonsState & 0b00000100;
	stVars->hmi.xOut4Cmd = u16ButtonsState & 0b00001000;
	stVars->hmi.xOut5Cmd = u16ButtonsState & 0b00010000;
	stVars->hmi.xOut6Cmd = u16ButtonsState & 0b00100000;
	stVars->hmi.xOut7Cmd = u16ButtonsState & 0b01000000;
	stVars->hmi.xOut8Cmd = u16ButtonsState & 0b10000000;
}

/*
 * Read Modbus Holding Registers by chunks
 * The chunks size should be set so that the communication time 
 * does not influence the PLC cycle time.
 * 
 * E.g : Transmitting over 100 words of 16-bits using Modbus RTU
 * can take more than 20ms at 115200 bauds.
 * 80 words takes about 16ms.
 */
void EuroPressHMI::_readRegisters() {
	// Read chunks from 0 to 2 * _ku8RdChunkSize + _ku8RdChunkSize - 1 = 0 to 191
	if(_u8RdChunkOffset > 191)  // The limit should be modulo _ku8RdChunkSize - 1 (63, 127, 191, etc...)
		_u8RdChunkOffset = 0;

	_u8RdResult = modbus.readHoldingRegisters(_u8RdChunkOffset, _ku8RdChunkSize);

	_u8RdChunkOffsetPrev = _u8RdChunkOffset;
	_u8RdChunkOffset += _ku8RdChunkSize;

	_xRegistersReadDone = true;
}

/*
 * Send counters values to HMI for displaying
 */
void EuroPressHMI::_writeCounters(GVL_PROCESS * pProcess) {
	modbus.clearTransmitBuffer();
	modbus.setTransmitBuffer(0, lowWord(pProcess->u32PartsOk));
	modbus.setTransmitBuffer(1, highWord(pProcess->u32PartsOk));
	modbus.setTransmitBuffer(2, lowWord(pProcess->u32PartsNok));
	modbus.setTransmitBuffer(3, highWord(pProcess->u32PartsNok));
	modbus.setTransmitBuffer(4, lowWord(pProcess->u32Cycles));
	modbus.setTransmitBuffer(5, highWord(pProcess->u32Cycles));
	modbus.writeMultipleRegisters(CNT_PART_OK_ADDR, 6);
}

/*
 * Write current production recipe values to HMI for displaying
 */
void EuroPressHMI::_writeCurrentRcpProd(GVL_VARS * vars) {
	modbus.clearTransmitBuffer();
	modbus.setTransmitBuffer(0, vars->u8CurentRecipe);
	modbus.setTransmitBuffer(1, vars->arRecipes[vars->u8CurentRecipe].u8Mode);
	modbus.writeMultipleRegisters(RCP_PROD_ID_ADDR, 2);
}

void EuroPressHMI::_writeHomePageValues(GVL_VARS * vars) {
	modbus.clearTransmitBuffer();
	if(vars->mainSFC.i16Step <= 10) {
		/*
		_writeWeldCycleValues(stVars);
		_writeCounters(&stVars->process);
		_writeCurrentRcpProd(stVars);
		*/
		modbus.setTransmitBuffer(0, vars->u8CurentRecipe);
		modbus.setTransmitBuffer(1, vars->arRecipes[vars->u8CurentRecipe].u8Mode);

		// Counters
		modbus.setTransmitBuffer(2, lowWord(vars->process.u32PartsOk));
		modbus.setTransmitBuffer(3, highWord(vars->process.u32PartsOk));
		modbus.setTransmitBuffer(4, lowWord(vars->process.u32PartsNok));
		modbus.setTransmitBuffer(5, highWord(vars->process.u32PartsNok));
		modbus.setTransmitBuffer(6, lowWord(vars->process.u32Cycles));
		modbus.setTransmitBuffer(7, highWord(vars->process.u32Cycles));

		// Temps US
		modbus.setTransmitBuffer(8, vars->stUsGenerator.u32RunningTime);
		// Puissance
		modbus.setTransmitBuffer(9, vars->io.O_xGeneratorRun ? vars->stUsGenerator.dActualPower : vars->stUsGenerator.dMaxPower);
		// Energie
		modbus.setTransmitBuffer(10, vars->stUsGenerator.dAccumulatedEnergy);
		// Profondeur
		modbus.setTransmitBuffer(11, vars->process.u32WeldDepth);
		// Temps de cycle
		modbus.setTransmitBuffer(12, vars->process.u32CycleTime);
		// Pression
		modbus.setTransmitBuffer(13, vars->process.u16PressPressureMeas);

		modbus.writeMultipleRegisters(RCP_PROD_ID_ADDR, 15);
	} else {
		_writeWeldCycleValues(vars);
	}
}

/*
 * Write faults, alarms and messages status to HMI for displaying
 */
void EuroPressHMI::_writeDefAlmMsg(GVL_VARS * vars) {
	modbus.clearTransmitBuffer();
	modbus.setTransmitBuffer(0, vars->hmi.u16DefCode);
	modbus.setTransmitBuffer(1, vars->hmi.u16AlmCode);
	modbus.setTransmitBuffer(2, vars->hmi.ui16MsgCode);
	modbus.writeMultipleRegisters(DEF_CODE_ADDR, 3);
}

/*
 * Write inputs and outputs status to HMI for monitoring by user
 */
void EuroPressHMI::_writeIOStatus(GVL_VARS * vars) {
	uint16_t u16Inputs;
	uint16_t u16Outputs;

	// Guess toolcode related inputs by doing some decimal to binary conversion
	bool xToolCode_bit0 = vars->io.I_u8ToolCode & 0x01;
	bool xToolCode_bit1 = vars->io.I_u8ToolCode & 0x02;
	bool xToolCode_bit2 = vars->io.I_u8ToolCode & 0x04;
	bool xToolCode_bit3 = vars->io.I_u8ToolCode & 0x08;

	// Construct inputs's image word by doing some binary to decimal conversion
	u16Inputs = vars->io.I_xEnergized      << 0  | vars->io.I_xEstop          << 1  | vars->io.I_xStartCycle    << 2
	          | vars->io.I_xGeneratorRun   << 3  | vars->io.I_xGeneratorError << 4  | vars->io.I_xPressAtUpPos  << 5
	          | vars->io.I_xPressAtDownPos << 6  | vars->io.I_xPartPresence1  << 7  | vars->io.I_xPartPresence2 << 8
	          | xToolCode_bit0             << 9  | xToolCode_bit1             << 10 | xToolCode_bit2            << 11
	          | xToolCode_bit3             << 12 | vars->io.I_xInput14        << 13 | vars->io.I_xInput15       << 14
			  | vars->io.I_xInput16        << 15;

	u16Outputs = 0; // TODO: Image des sorties TOR

	modbus.clearTransmitBuffer();
	modbus.setTransmitBuffer(0, u16Inputs);
	modbus.setTransmitBuffer(1, vars->io.I_u16GeneratorPower);
	modbus.setTransmitBuffer(2, vars->io.I_u16PressPressure);
	modbus.setTransmitBuffer(3, lowWord(REG_TC0_CV0));  // Encoder value (LSB)
	modbus.setTransmitBuffer(4, highWord(REG_TC0_CV0)); // Encoder value (MSB)
	// bit array
	uint16_t u16Dummy = vars->process.xManualMode << 0;
	modbus.setTransmitBuffer(5, u16Dummy);
	/*
	modbus.setTransmitBuffer(3, u16Outputs);
	modbus.setTransmitBuffer(4, 0); // TODO: Image de la sortie analogique n°1
	modbus.setTransmitBuffer(5, 0); // TODO: Image de la sortie analogique n°2
	*/
	modbus.writeMultipleRegisters(DI_STATE_ADDR, 6);
}

void EuroPressHMI::_writeOutCmdStatus(GVL_VARS * vars) {
	uint16_t u16ButtonsState;
	u16ButtonsState = vars->hmi.xOut1Cmd << 0 | vars->hmi.xOut2Cmd << 1 | vars->hmi.xOut3Cmd << 2
	                | vars->hmi.xOut4Cmd << 3 | vars->hmi.xOut5Cmd << 4 | vars->hmi.xOut6Cmd << 5
	                | vars->hmi.xOut7Cmd << 6 | vars->hmi.xOut8Cmd << 7;
	if (!vars->process.xManualMode) {
		modbus.writeSingleRegister(DO_CMD_ADDR, 0);
	}
}

void EuroPressHMI::_writeParamRegisters(GVL_PARAM * pParams) {
	modbus.clearTransmitBuffer();
	
	// Firmware version register
	modbus.setTransmitBuffer(0, pParams->u16Version);
	// The version date string has a maximum length of 10 characters
	// and 2 characters are stored on 1 Modbus register.
	// So the version date string is stored on 5 registers
	for(uint8_t i=0; i < 5; i++) {
		modbus.setTransmitBuffer(1+i, pParams->sVersionDate[i*2+1] << 8 | pParams->sVersionDate[i*2]);
	}
	// Encoder configuration register
	modbus.setTransmitBuffer(6, pParams->xEncoderSE << 1
	                          | pParams->xEncoderInv);
	// Encoder resolution register
	modbus.setTransmitBuffer(7, pParams->i16EncoderResolution);
	// Generator max power register
	modbus.setTransmitBuffer(8, pParams->u16GeneratorMaxPower);
	// Cycle timeout
	modbus.setTransmitBuffer(9, pParams->u8CycleTimeout);
	// 
	modbus.setTransmitBuffer(10, map(pParams->u16GeneratorAmplitudeMin_Points, 0, 65535, 0, 100));
	//
	modbus.setTransmitBuffer(11, map(pParams->u16GeneratorAmplitudeMax_Points, 0, 65535, 0, 100));
	//
	modbus.setTransmitBuffer(12, pParams->u16GeneratorAmplitudeMin_Percent);
	//
	modbus.setTransmitBuffer(13, pParams->u16GeneratorAmplitudeMax_Percent);
	//
	modbus.setTransmitBuffer(14, map(pParams->u16PressRegulatorOutMin_Points, 0, 65535, 0, 100));
	//
	modbus.setTransmitBuffer(15, map(pParams->u16PressRegulatorOutMax_Points, 0, 65535, 0, 100));
	//
	modbus.setTransmitBuffer(16, pParams->u16PressRegulatorOutMin_Bar);
	//
	modbus.setTransmitBuffer(17, pParams->u16PressRegulatorOutMax_Bar);
	//
	modbus.setTransmitBuffer(18, map(pParams->u16PressRegulatorInMin_Points, 0, 4095, 0, 100));
	//
	modbus.setTransmitBuffer(19, map(pParams->u16PressRegulatorInMax_Points, 0, 4095, 0, 100));
	//
	modbus.setTransmitBuffer(20, pParams->u16PressRegulatorInMin_Bar);
	//
	modbus.setTransmitBuffer(21, pParams->u16PressRegulatorInMax_Bar);

	_u8WrRecipeResult = modbus.writeMultipleRegisters(HMI_PARAMS_BLOCK_START, 22);
}

void EuroPressHMI::_writeRecipeRegisters(RCP_DATA * stRcpData) {
	_u8RdResult = 0;  // Avoid EuroPressHMI::isAvailable() being stuck when _uiRdResult == modbus.ku8MBResponseTimedOut

	modbus.clearTransmitBuffer();
	modbus.setTransmitBuffer(0, stRcpData->u8Id);
	// Recipe name has a maximum length of 16 chararacters
	// and 2 characters are stored on 1 Modbus register.
	// So the recipe name is stored on 8 registers
	for(uint8_t i=0; i < 8; i++) {
		modbus.setTransmitBuffer(1+i, stRcpData->sName[i*2+1] << 8 | stRcpData->sName[i*2]);
	}
	modbus.setTransmitBuffer(9, stRcpData->u8Mode);
	modbus.setTransmitBuffer(10, stRcpData->u8Amplitude);
	modbus.setTransmitBuffer(11, stRcpData->u16StartDelayTime);
	modbus.setTransmitBuffer(12, stRcpData->u16StartPos);
	modbus.setTransmitBuffer(13, stRcpData->u16WeldingTime);
	modbus.setTransmitBuffer(14, stRcpData->u16WeldingPower);
	modbus.setTransmitBuffer(15, stRcpData->u16WeldingEnergy);
	modbus.setTransmitBuffer(16, stRcpData->u16WeldingDistance);
	modbus.setTransmitBuffer(17, stRcpData->u16WeldingDepth);
	modbus.setTransmitBuffer(18, stRcpData->u16CoolingTime);
	modbus.setTransmitBuffer(19, stRcpData->u16DescentPressure);
	modbus.setTransmitBuffer(20, stRcpData->u16WeldingPressure);
	modbus.setTransmitBuffer(21, stRcpData->u16CoolingPressure);
	modbus.setTransmitBuffer(22, stRcpData->u16PulseDelay);
	modbus.setTransmitBuffer(23, stRcpData->u16PulseWidth);
	modbus.setTransmitBuffer(24, stRcpData->xPartPresence2 << 5
	                           | stRcpData->xPartPresence1 << 4
							   | stRcpData->xDepthLimit << 3
	                           | stRcpData->xEnergyLimit << 2
	                           | stRcpData->xPowerLimit << 1
	                           | stRcpData->xTimeLimit);
	modbus.setTransmitBuffer(25, stRcpData->u16TimeMinLimit);
	modbus.setTransmitBuffer(26, stRcpData->u16TimeMaxLimit);
	modbus.setTransmitBuffer(27, stRcpData->u16PowerMinLimit);
	modbus.setTransmitBuffer(28, stRcpData->u16PowerMaxLimit);
	modbus.setTransmitBuffer(29, stRcpData->u16EnergyMinLimit);
	modbus.setTransmitBuffer(30, stRcpData->u16EnergyMaxLimit);
	modbus.setTransmitBuffer(31, (uint16_t)stRcpData->i16DepthMinLimit);
	modbus.setTransmitBuffer(32, (uint16_t)stRcpData->i16DepthMaxLimit);
	modbus.setTransmitBuffer(33, stRcpData->u16PartPos);

	// Recipe word starts at address HMI_RCP_BLOCK_START (20)
	_u8WrRecipeResult = modbus.writeMultipleRegisters(HMI_RCP_BLOCK_START, 60);
}

/*
 * Ecrit les numéros d'étape et temps d'activation de l'étape actuelle de chaque grafcet
 * sur la page de surveillance
 */
void EuroPressHMI::_writeSfcStatus(GVL_VARS * vars) {
	modbus.clearTransmitBuffer();
	// Numéro d'étape du GEMMA
	modbus.setTransmitBuffer(0, vars->runningSFC.i16Step);
	// Temps d'activation de l'étape du GEMMA
	modbus.setTransmitBuffer(1, vars->runningSFC.u16StepTime);
	// Numéro d'étape du cycle principal
	modbus.setTransmitBuffer(2, vars->mainSFC.i16Step);
	// Temps d'activation de l'étape du cycle principal
	modbus.setTransmitBuffer(3, vars->mainSFC.u16StepTime);
	// Numéro d'étape du cycle de soudure
	modbus.setTransmitBuffer(4, vars->weldSFC.i16Step);
	// Temps d'activation de l'étape du cycle de soudure
	modbus.setTransmitBuffer(5, vars->weldSFC.u16StepTime);
	modbus.writeMultipleRegisters(RUN_SFC_STEP_ADDR, 6);
}

/*
 * Ecrit les valeurs affichées sur la page principale de l'IHM
 */
void EuroPressHMI::_writeWeldCycleValues(GVL_VARS * vars) {
	modbus.clearTransmitBuffer();
	// Temps US
	modbus.setTransmitBuffer(0, vars->stUsGenerator.u32RunningTime);
	// Puissance
	modbus.setTransmitBuffer(1, vars->io.O_xGeneratorRun ? vars->stUsGenerator.dActualPower : vars->stUsGenerator.dMaxPower);
	// Energie
	modbus.setTransmitBuffer(2, vars->stUsGenerator.dAccumulatedEnergy);
	// Profondeur
	modbus.setTransmitBuffer(3, vars->process.u32WeldDepth);
	// Temps de cycle
	modbus.setTransmitBuffer(4, vars->process.u32CycleTime);
	// Pression
	modbus.setTransmitBuffer(5, vars->process.u16PressPressureMeas);
	// bit array
	uint16_t u16Dummy = vars->process.xManualMode << 0;
	modbus.setTransmitBuffer(6, u16Dummy);
	modbus.writeMultipleRegisters(WELD_TIME_ADDR, 7);
}
