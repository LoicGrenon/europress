#include "TOF_Timer.h"

/*
  // TOF example
  TOF_Timer stTOF;
  stTOF.timer(vars->io.I_xStartCycle, 1000);
  vars->io.O_xGeneratorRun = stTOF.xOut;
*/

void TOF_Timer::timer(bool xIn, uint32_t u32Time) {
	if (xIn) {
		this->xOut = false;
		this->u32ElapsedTime = 0;
		_u32StartT = millis();
	}
  
	if(xIn && !this->_xInOld) { // On rising edge
		this->_xInOld = true;
	} else if(!xIn && this->_xInOld) {  // On falling edge
		this->u32ElapsedTime = millis() - _u32StartT;
		
		if (this->u32ElapsedTime >= u32Time) {
			this->u32ElapsedTime = u32Time;
			this->xOut = false;
			this->_xInOld = false;
		} else {
			this->xOut = true;
		}
	}
}
