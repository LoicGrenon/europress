#include <Arduino.h>
#include <digitalWriteFast.h>
#include <SPI.h>
#include "GVL.h"

#ifndef _EUROPRESS_IO_H
#define _EUROPRESS_IO_H

// Use as : NOP; to waste one cycle
#define NOP __asm__ __volatile__ ("nop\n\t")

//#define DIG_INPUT_BOARD_MAX31911

/*
 * Attribution des broches : Circuiterie externe
 */
#define LATCH_DIG_OUT_1	22
#define LATCH_DIG_OUT_2	23
#define CS_DIG_IN_1		27
#define CS_DIG_IN_2		15
#define	CS_FLASH		28
#define	CS_ENCODER		29

#define ADC_RD			26
#define IN_ADC_BUSY		24
#define	OUT_ADC_CONVST	25

/*
 * Attribution des broches: Port parallèle - Entrées
 */
#define PP_IN0		31
#define PP_IN1		30
#define PP_IN2		33
#define PP_IN3		32
#define PP_IN4		35
#define PP_IN5		34
#define PP_IN6		37
#define PP_IN7		36
#define PP_IN8		39
#define PP_IN9		38
#define PP_IN10     41
#define PP_IN11	    40
#define PP_IN12	    43
#define PP_IN13	    42
#define PP_IN14	    45
#define PP_IN15	    44

/*
 * Attribution des broches: Port parallèle - Sorties
 */
#define PP_OUT0		46
#define PP_OUT1		47
#define PP_OUT2		48
#define PP_OUT3		49
#define PP_OUT4		50
#define PP_OUT5		51
#define PP_OUT6		52
#define PP_OUT7		53

/*
 * Attribution des broches: Entrées analogiques
 */
// ANA_IN_CH1: Retour de la pression de descente de la presse
#define IN_PRESS_PRESSURE_PIN	A0	

/*
 * Attribution des broches: Divers
 */
// Pour surveillance temps de cycle
#define OUT_TPS_CYCLE		    11
// Pour surveillance temps de cycle
#define OUT_TPS_CYCLE2		    12

/*
 * Attribution des broches: Sorties analogiques
 */
// ANA_OUT_CH1: Consigne d'amplitude du générateur ultrasons
#define OUT_GENERATOR_AMPLITUDE_PIN	DAC0
// ANA_OUT_CH2: Consigne de pression pour la descente de la presse
#define OUT_PRESS_PRESSURE_PIN		DAC1



/*------------------------------------------------------------------------------
 *         Global functions
 *------------------------------------------------------------------------------*/

void makePPIn();
void makePPOut();

void setupInputs();
void setupOutputs();

void readInputs(GVL_VARS * vars);
void writeOutputs(GVL_VARS * vars);
void _readDigitalInputs(GVL_VARS * vars);
void _readDigitalInputs_MAX31911(GVL_VARS * vars);
void _readAnalogInputs(GVL_VARS * vars);
void _readAD7610(GVL_VARS * vars);
void _writeDigitalOutputs(GVL_VARS * vars);
void _writeAnalogOutputs(GVL_VARS * vars);

#endif // #ifndef _EUROPRESS_IO_H