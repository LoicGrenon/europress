#ifndef EUROPRESS_MODBUS_ADDR_H
#define EUROPRESS_MODBUS_ADDR_H

/*
 * Définition des addressages pour la communication entre l'automate et l'écran
 * via Modbus RTU
 */

/*---------------------*/
/* Echanges PLC -> HMI */
/*---------------------*/


// Boutons divers (voir adressage des bits ci-dessous)
#define BTN_ADDR          0
// Numéro de recette sélectionnée
#define RCP_SELECTED_ADDR 1
// Boutons pour forçage  des sorties TOR sur IHM
#define DO_CMD_ADDR       2
// Image de la sortie analogique n°1
#define AO1_VALUE_ADDR    3
// Image de la sortie analogique n°2
#define AO2_VALUE_ADDR    4
// Numéro de recette en cours de production
#define RCP_PROD_ID_ADDR  5
// Mode de soudure de la recette en cours de production
#define RCP_PROD_WELD_MODE_ADDR  6
// Compteur de pièces conformes
#define CNT_PART_OK_ADDR  7
#define CNT_PART_OK_LSB_ADDR  7
#define CNT_PART_OK_MSB_ADDR  8
// Compteur de pièces non conformes
#define	CNT_PART_NOK_ADDR 9
#define	CNT_PART_NOK_LSB_ADDR 9
#define	CNT_PART_NOK_MSB_ADDR 10
// Compteur de cycles machine
#define CNT_CYCLES_ADDR   11
#define CNT_CYCLES_LSB_ADDR   11
#define CNT_CYCLES_MSB_ADDR   12
// Temps de soudure du dernier cycle de soudure
#define WELD_TIME_ADDR    13
// Puissance du dernier cycle de soudure
#define WELD_POWER_ADDR   14
// Energie du dernier cycle de soudure
#define	WELD_ENERGY_ADDR  15
// Enfoncement du dernier cycle de soudure
#define WELD_DEPTH_ADDR   16
// Temps de cycle global du dernier cycle de soudure
#define	CYCLE_TIME_ADDR   17
// Pression courante appliquée sur le vérin de la presse
#define	CUR_PRESSURE_ADDR   18

// Mot pour affichage des défauts sur IHM
#define DEF_CODE_ADDR     110
// Mot pour affichage des alarmes sur IHM
#define ALM_CODE_ADDR     111
// Mot pour affichage des messages sur IHM
#define MSG_CODE_ADDR     112
// Numéro d'étape du grafcet du GEMMA
#define RUN_SFC_STEP_ADDR 113
// Numéro d'étape du grafcet du cycle principal
#define MAIN_SFC_STEP_ADDR 115
// Numéro d'étape du grafcet du cycle de soudure
#define WELD_SFC_STEP_ADDR 117

// Image des entrées TOR pour affichage sur IHM
#define DI_STATE_ADDR     120
// Image de l'entrée analogique n°1
#define AI1_VALUE_ADDR    121
// Image de l'entrée analogique n°2
#define AI2_VALUE_ADDR    122
// Valeur du codeur en points
#define ENC_LSBVALUE_ADDR 123
// Valeur du codeur en points
#define ENC_MSBVALUE_ADDR 124
// Champ de bits (Ecrits vers LB50200 - LB50215 dans l'IHM : refreshBtnState macro)
#define MISC_BITS_ARRAY_ADDR 125

// Numéro d'écran affiché sur l'IHM
#define HMI_CUR_SCREEN_ADDR 191

/*
 * Selected (current) recipe registers addressing
 */
#define HMI_RCP_BLOCK_START 20
#define HMI_RCP_BLOCK_END 79

// Numéro de la recette
#define RCP_ID              20
// Nom de la recette (Caractères 1 et 2)
#define RCP_NAME_0_1        21
// Nom de la recette (Caractères 3 et 4)
#define RCP_NAME_2_3        22
// Nom de la recette (Caractères 5 et 6)
#define RCP_NAME_4_5        23
// Nom de la recette (Caractères 7 et 8)
#define RCP_NAME_6_7        24
// Nom de la recette (Caractères 9 et 10)
#define RCP_NAME_8_9        25
// Nom de la recette (Caractères 11 et 12)
#define RCP_NAME_10_11      26
// Nom de la recette (Caractères 13 et 14)
#define RCP_NAME_12_13      27
// Nom de la recette (Caractères 15 et 16)
#define RCP_NAME_14_15      28
// Mode de soudure
#define RCP_MODE            29
// Consigine d'amplitude du générateur ultrasons
#define RCP_AMPLITUDE       30
// Temps de retard avant soudure (hors mode distance)
#define RCP_WELD_DELAY      31
// Position de déclenchement des US (mode distance)
#define RCP_WELD_START_POS  32
// Consigne de temps de soudure (mode temps)
#define RCP_WELD_TIME       33
// Consigne de puissance de soudure (mode puissance)
#define RCP_WELD_POWER      34
// Consigne d'énergie de soudure (mode énergie)
#define RCP_WELD_ENERGY     35
// Consigne de distance de soudure (mode distance)
#define RCP_WELD_DISTANCE   36
// Consigne de profondeur de soudure (mode profondeur)
#define RCP_WELD_DEPTH      37
// Temps de refroidissement
#define RCP_COOLING         38
// Pression appliquée au vérin de la presse lors de la phase de descente
#define RCP_DESC_PRESSURE   39
// Pression appliquée au vérin de la presse lors de la phase de soudure
#define RCP_WELD_PRESSURE   40
// Pressoin appliquée au vérin de la presse lors de la phase de refroidissement
#define RCP_COOL_PRESSURE   41
// Retard avant impulsion US à la remontée
#define RCP_PULSE_DELAY     42
// Temps de l'impulsion US à la remontée
#define RCP_PULSE_WIDTH     43
// Limites pour qualification de soudre (voir bit mask ci-dessous)
#define RCP_LIMITS_MODE     44
// Limite mini de temps à atteindre pour soudure OK
#define RCP_TIME_MIN        45
// Limite maxi de temps à atteindre pour soudure OK
#define RCP_TIME_MAX        46
// Limite mini de puissance à atteindre pour soudure OK
#define RCP_POWER_MIN       47
// Limite maxi de puissance à atteindre pour soudure OK
#define RCP_POWER_MAX       48
// Limite mini d'énergie à atteindre pour soudure OK  
#define RCP_ENERGY_MIN      49
// Limite maxi d'énergie à atteindre pour soudure OK
#define RCP_ENERGY_MAX      50
// Limite mini de profondeur à atteindre pour soudure OK
#define RCP_DEPTH_MIN       51
// Limite maxi de profondeur à atteindre pour soudure OK
#define RCP_DEPTH_MAX       52
// Position de la pièce à souder pour le mode distance
#define RCP_PART_POSITION   53



/*
 * Bit mask for RCP_LIMITS_MODE register
 * 
 * Si le bit correspondant est à 1, la limite correspondante est activée.
 */

// Activation de la limite de temps
#define RCP_TIME_LIMIT_MASK     0x01
// Activation de la limite de puissance
#define RCP_POWER_LIMIT_MASK    0x02
// Activation de la limite d'énergie
#define RCP_ENERGY_LIMIT_MASK   0x04
// Activation de la limite de profondeur
#define RCP_DEPTH_LIMIT_MASK    0x08
// Activation de la présence pièce 1
#define RCP_PART_PRES_1    0x10
// Activation de la présence pièce 2
#define RCP_PART_PRES_2    0x20

/*
 * Boutons de l'IHM
 * Adresses de bit du mot défini par BUTTONS_ADDR
 */
 // Acquittement des défauts
#define ACK_BTN			0
// RAZ des compteurs pièces
#define CNT_RST_BTN		1
// Sauvegarde de la recette
#define RCP_SAVE_BTN	2
// Chargement de la recette
#define RCP_LOAD_BTN	3
// Sélection de la recette
#define RCP_SELECT_BTN	4
// Sauvegarde des paramètres machine
#define PARAM_SAVE_BTN  5
// Chargement des paramètres machine
#define PARAM_LOAD_BTN  6
// Restauration flash usine (paramètres, recettes)
#define PARAM_RESTORE_BTN  7


/*
 * Paramètres machine
 */ 
#define HMI_PARAMS_BLOCK_START 80
#define HMI_PARAMS_BLOCK_END 109

// Paramètre machine : Configuration de la règle linéaire
#define HMI_PARAMS_ENCODER_CONFIG 86
// Paramètre machine : Résolution de la règle linéaire
#define HMI_PARAMS_ENCODER_RESOLUTION 87
// Paramètre machine : Puissance maxi du générateur US
#define HMI_PARAMS_GENERATOR_POWER 88
// Paramètre machine : Temps de cycle max
#define HMI_PARAMS_CYCLE_TIMEOUT 89
// Paramètre machine : Mise à l'échelle de la sortie analogique 1 - Xmin (points)
#define HMI_PARAM_AO1_XMIN 90
// Paramètre machine : Mise à l'échelle de la sortie analogique 1 - Xmax (points)
#define HMI_PARAM_AO1_XMAX 91
// Paramètre machine : Mise à l'échelle de la sortie analogique 1 - Ymin (user unit)
#define HMI_PARAM_AO1_YMIN 92
// Paramètre machine : Mise à l'échelle de la sortie analogique 1 - Ymax (user unit)
#define HMI_PARAM_AO1_YMAX 93
// Paramètre machine : Mise à l'échelle de la sortie analogique 2 - Xmin (points)
#define HMI_PARAM_AO2_XMIN 94
// Paramètre machine : Mise à l'échelle de la sortie analogique 2 - Xmax (points)
#define HMI_PARAM_AO2_XMAX 95
// Paramètre machine : Mise à l'échelle de la sortie analogique 2 - Ymin (user unit)
#define HMI_PARAM_AO2_YMIN 96
// Paramètre machine : Mise à l'échelle de la sortie analogique 2 - Ymax (user unit)
#define HMI_PARAM_AO2_YMAX 97
// Paramètre machine : Mise à l'échelle de l'entrée analogique 1 - Xmin (points)
#define HMI_PARAM_AI1_XMIN 98
// Paramètre machine : Mise à l'échelle de l'entrée analogique 1 - Xmax (points)
#define HMI_PARAM_AI1_XMAX 99
// Paramètre machine : Mise à l'échelle de l'entrée analogique 1 - Ymin (user unit)
#define HMI_PARAM_AI1_YMIN 100
// Paramètre machine : Mise à l'échelle de l'entrée analogique 1 - Ymax (user unit)
#define HMI_PARAM_AI1_YMAX 101

/*
 * Bit mask for HMI_PARAMS_ENCODER_CONFIG register
 */
// Inversion du sens de décomptage
#define PARAMS_ENCODER_INV 1
// Type d'entrée (0: différential - 1: single ended)
#define PARAMS_ENCODER_SE 2


/*---------------------*/
/* Echanges PLC -> HMI */
/*---------------------*/


#endif // End of EUROPRESS_MODBUS_ADDR_H
