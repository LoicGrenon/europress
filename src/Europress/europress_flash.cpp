#include "europress_flash.h"
#include "europress_utils.h"
SPIFram flash(CS_FLASH);


void flashFactoryRestore(GVL_VARS * pVars) {
	pVars->param.i16EncoderResolution = 50;
	pVars->param.u16GeneratorMaxPower = 800;
	pVars->param.u8CycleTimeout = 30;
	pVars->param.u16GeneratorAmplitudeMin_Points = 0;
	pVars->param.u16GeneratorAmplitudeMax_Points = 65535;
	pVars->param.u16GeneratorAmplitudeMin_Percent = 50;
	pVars->param.u16GeneratorAmplitudeMax_Percent = 100;
	pVars->param.u16PressRegulatorOutMin_Points = 0;
	pVars->param.u16PressRegulatorOutMax_Points = 65535;
	pVars->param.u16PressRegulatorOutMin_Bar = 0;
	pVars->param.u16PressRegulatorOutMax_Bar = 90;
	pVars->param.u16PressRegulatorInMin_Points = 410;
	pVars->param.u16PressRegulatorInMax_Points = 2048;
	pVars->param.u16PressRegulatorInMin_Bar = 0;
	pVars->param.u16PressRegulatorInMax_Bar = 90;
	pVars->param.xEncoderInv = false;
	pVars->param.xEncoderSE = false;
	writeParamsToFlash(&pVars->param);

	for(uint8_t i=0; i <= 9; i++) {
		String sPrgName = "Programme " + String(i+1);
		strcpy(pVars->arRecipes[i].sName, sPrgName.c_str());
		pVars->arRecipes[i].u8Id = i+1;
		writeRecipeToFlash(&pVars->arRecipes[i]);
	}
}

void getLastCountersValues(GVL_PROCESS * pProcess) {
	pProcess->u32PartsOk = flash.readULong(FLASH_GOOD_PARTS_CNT_ADDR);
	pProcess->u32PartsNok = flash.readULong(FLASH_BAD_PARTS_CNT_ADDR);
	pProcess->u32Cycles = flash.readULong(FLASH_CYCLES_CNT_ADDR);
}

uint8_t getLastRecipeId(void) {
	uint8_t u8LastRcpId;
	u8LastRcpId = flash.readByte(FLASH_LAST_RCP_ID_ADDR);
	if (u8LastRcpId > 9) {
		DPRINTLN("Invalid recipe id from flash");
		return 0;
	} else {
		DPRINTLN("Last recipe id from flash: " + String(u8LastRcpId));
		return u8LastRcpId;
	}
}

void readParamsFromFlash(GVL_PARAM * pParams) {
	flash.readAnything(FLASH_PARAM_START_ADDR, *pParams, true);
	// TODO: Sanitize values with _checkParamValues()
	if (pParams->u8CycleTimeout == 0)
		pParams->u8CycleTimeout = 30;
}

void readRecipeFromFlash(RCP_DATA * stRcpData, uint8_t u8RecipeId) {
	uint32_t u32RecipeAddr;

	if(u8RecipeId == 0xFF) { // Default Id value: use the one from stRcpData
		u8RecipeId = stRcpData->u8Id - 1;
	}
	DPRINTLN("Recipe #" + String(u8RecipeId + 1) + " read");
	u32RecipeAddr = FLASH_RCP_START_ADDR + u8RecipeId * FLASH_RCP_SIZE_BYTES;
	flash.readAnything(u32RecipeAddr, *stRcpData, true);
	_checkRecipeValue(stRcpData, u8RecipeId);
	DPRINTLN("---");
}

void setupFlashMemory() {
	delayMicroseconds(250);  // 250us min are needed to ensure the FM25V02A is accessible after power up (tPU time)
	flash.begin();
}

void storeCounters(GVL_PROCESS * pProcess) {
	flash.writeULong(FLASH_GOOD_PARTS_CNT_ADDR, pProcess->u32PartsOk);
	flash.writeULong(FLASH_BAD_PARTS_CNT_ADDR, pProcess->u32PartsNok);
	flash.writeULong(FLASH_CYCLES_CNT_ADDR, pProcess->u32Cycles);
}

/*
 * This function is used to write the default recipe values to the flash memory
 * It is useful when the IC memory chip is clear (first install)
 */
void writeDefaultRecipesToFlash() {
	uint32_t u32RecipeAddr;
	uint8_t u8OldRecipeId;
	RCP_DATA stRecipe;

	for(uint8_t i=1; i <= 10; i++) {
		stRecipe.u8Id = i;
		u8OldRecipeId = stRecipe.u8Id;  // We store the recipe Id because we will read the value from the flash memory
		readRecipeFromFlash(&stRecipe);
		if(stRecipe.u8Id != u8OldRecipeId) {
			DPRINTLN("Old is : "+String(u8OldRecipeId)+" - Read is : "+String(stRecipe.u8Id));
			stRecipe.u8Id = i;
			writeRecipeToFlash(&stRecipe);
		}
	}
}

void writeLastRecipeID(uint8_t u8RecipeId) {
	flash.writeByte(FLASH_LAST_RCP_ID_ADDR, u8RecipeId);
}

void writeParamsToFlash(GVL_PARAM * pParams) {
	flash.writeAnything(FLASH_PARAM_START_ADDR, *pParams);
}

void writeRecipeToFlash(RCP_DATA * stRcpData) {
	uint32_t u32RecipeAddr;

	DPRINTLN("Recipe save");
	if(stRcpData->u8Id >= 1 && stRcpData->u8Id <= 10) {
		u32RecipeAddr = FLASH_RCP_START_ADDR + (stRcpData->u8Id - 1) * FLASH_RCP_SIZE_BYTES;
		flash.writeAnything(u32RecipeAddr, *stRcpData);
	} else {
		DPRINTLN("Error: Invalid recipe id");
	}

	DPRINTLN("---");
}

void _checkRecipeValue(RCP_DATA * stRcpData, uint8_t u8RecipeId) {
	if(stRcpData->u8Id != u8RecipeId + 1) { // Recipe is invalid
		DPRINTLN("Invalid id: " + String(stRcpData->u8Id));
		// Set default values
		stRcpData->u8Id = u8RecipeId + 1;
		stRcpData->u8Mode = 0;
		stRcpData->u8Amplitude = 50;
		stRcpData->u16DescentPressure = 0;
		stRcpData->u16WeldingPressure = 0;
		stRcpData->u16CoolingPressure = 0;
		stRcpData->u16PulseDelay = 0;
		stRcpData->u16PulseWidth = 0;
		stRcpData->u16CoolingTime = 0;
		stRcpData->u16StartDelayTime = 0;
		stRcpData->u16StartPos = 0;
		stRcpData->u16PartPos = 0;
		stRcpData->u16WeldingTime = 0;
		stRcpData->u16WeldingPower = 0;
		stRcpData->u16WeldingEnergy = 0;
		stRcpData->u16WeldingDistance = 0;
		stRcpData->u16WeldingDepth = 0;
		stRcpData->xTimeLimit = false;
		stRcpData->xPowerLimit = false;
		stRcpData->xEnergyLimit = false;
		stRcpData->xDepthLimit = false;
		stRcpData->xPartPresence1 = false;
		stRcpData->xPartPresence2 = false;
		stRcpData->u16TimeMinLimit = 0;
		stRcpData->u16TimeMaxLimit = 0;
		stRcpData->u16PowerMinLimit = 0;
		stRcpData->u16PowerMaxLimit = 0;
		stRcpData->u16EnergyMinLimit = 0;
		stRcpData->u16EnergyMaxLimit = 0;
		stRcpData->i16DepthMinLimit = 0;
		stRcpData->i16DepthMaxLimit = 0;
	} else { // Recipe is valid
		DPRINTLN("Recipe is valid");
	}
}
