#ifndef EUROPRESS_GVL_H
#define EUROPRESS_GVL_H

typedef enum {
	WELD_DISABLED = 0,
	WELD_MODE_TIME = 1,
	WELD_MODE_POWER = 2,
	WELD_MODE_ENERGY = 3,
	WELD_MODE_DISTANCE = 4,
	WELD_MODE_DEPTH = 5
} WELD_MODE;

/*
 * Paramètres machine
 */
typedef struct __attribute__((packed)) {
	// Numéro de version du programme
	uint16_t u16Version;
	// Date de la version du programme
	char sVersionDate[10];
	// Sens de rotation du codeur inversé
	bool xEncoderInv = false;
	// 0: Entrée différentielle | 1: Entrée single-ended
	bool xEncoderSE = false;
	// Résolution du codeur (1 = 10µm) => Avec une résolution de 10µm, la course max est de 327.67 mm.
	int16_t i16EncoderResolution = 5;
	// Puissance max du générateur ultrasons (pour mise à l'échelle de la mesure)
	uint16_t u16GeneratorMaxPower = 800;
	// Temps max d'un cycle (en secondes)
	uint8_t u8CycleTimeout = 30;
	// Valeur mini de l'amplitude du générateur ultrason (en points, 65535 points = 10V)
	uint16_t u16GeneratorAmplitudeMin_Points = 0;
	// Valeur maxi de l'amplitude du générateur ultrason (en points, 65535 points = 10V)
	uint16_t u16GeneratorAmplitudeMax_Points = 65535;
	// Valeur mini de l'amplitude du générateur ultrason (en %)
	uint16_t u16GeneratorAmplitudeMin_Percent = 50;
	// Valeur maxi de l'amplitude du générateur ultrason (en %)
	uint16_t u16GeneratorAmplitudeMax_Percent = 100;
	// Valeur mini de consigne de pression de la vanne proportionnelle de la presse (en points, 65535 points = 10V)
	uint16_t u16PressRegulatorOutMin_Points = 0;
	// Valeur maxi de consigne de pression de la vanne proportionnelle de la presse (en points, 65535 points = 10V)
	uint16_t u16PressRegulatorOutMax_Points = 65535;
	// Valeur mini de consigne de pression de la vanne proportionnelle de la presse (en 0.1 bars)
	uint16_t u16PressRegulatorOutMin_Bar = 0;
	// Valeur maxi de consigne de pression de la vanne proportionnelle de la presse (en 0.1 bars)
	uint16_t u16PressRegulatorOutMax_Bar = 90;
	// Valeur mini du retour de pression de la vanne proportionnelle de la presse (en points, 4095 points = 10V)
	uint16_t u16PressRegulatorInMin_Points = 410;
	// Valeur maxi du retour de pression de la vanne proportionnelle de la presse (en points, 4095 points = 10V)
	uint16_t u16PressRegulatorInMax_Points = 2048;
	// Valeur mini du retour de pression de la vanne proportionnelle de la presse (en 0.1 bars)
	uint16_t u16PressRegulatorInMin_Bar = 0;
	// Valeur maxi du retour de pression de la vanne proportionnelle de la presse (en 0.1 bars)
	uint16_t u16PressRegulatorInMax_Bar = 90;
} GVL_PARAM;  // 35 octets

/*
 * Entrées/Sorties
 */
typedef struct __attribute__((packed)) {
	// Entrée: Arrêt d'urgence
	bool I_xEstop;
	// Entrée: Machine en énergie
	bool I_xEnergized;
	// Entrée: Départ cycle
	bool I_xStartCycle;
	// Entrée: Vérin de la presse en position haute
	bool I_xPressAtUpPos;
	// Entrée: Vérin de la presse en position basse
	bool I_xPressAtDownPos;
	// Entrée: Générateur ultrasons en défaut
	bool I_xGeneratorError;
	// Entrée: Générateur ultrasons en marche
	bool I_xGeneratorRun;
	// Entrée: Présence pièce n°1
	bool I_xPartPresence1;
	// Entrée: Présence pièce n°2
	bool I_xPartPresence2;
	// Entrée: Numéro de codage de l'outillage
	uint8_t I_u8ToolCode;
	// Entrée réserve n°14
	bool I_xInput14;
	// Entrée réserve n°15
	bool I_xInput15;
	// Entrée réserve n°16
	bool I_xInput16;
	// Entrée: Retour de puissance du générateur ultrasons
	uint16_t I_u16GeneratorPower;
	// Entrée: Retour de pression de la vanne proportionnelle de la presse
	uint16_t I_u16PressPressure;
	// Sortie: Mise en marche du générateur ultrasons
	bool O_xGeneratorRun;
	// Sortie: Commande de descente de la presse
	bool O_xPressDown;
	// Sortie: Commande de refroidissement
	bool O_xCooling;
	// Sortie réserve n°4
	bool O_xOutput4;
	// Sortie réserve n°5
	bool O_xOutput5;
	// Sortie réserve n°6
	bool O_xOutput6;
	// Sortie réserve n°7
	bool O_xOutput7;
	// Sortie réserve n°8
	bool O_xOutput8;
	// Sortie: Consigne d'amplitude du générateur ultrasons
	uint16_t O_u16GeneratorAmplitude;
	// Sortie: Consigne de pression pour la descente de la presse
	uint16_t O_u16PressPressure;
} GVL_IO;  // 29 octets

/*
 * Données de recette
 */
typedef struct __attribute__((packed)) {
	// Numéro du programme
	uint8_t u8Id = 0;
	// Nom du programme
	char sName[16] = "               ";
	// Mode de soudure. 0: Aucun | 1: Temps | 2: Puissance | 3: Energie | 4: Distance | 5: Enfoncement
	uint8_t u8Mode = 0;
	// Consigne d'amplitude du générateur ultrasons en %
	uint8_t  u8Amplitude = 50;
	// Consigne de pression lors de la descente de la presse
	uint16_t u16DescentPressure = 0;
	// Consigne de pression lors de la soudure
	uint16_t u16WeldingPressure = 0;
	// Consigne de pression lors du refroidissement
	uint16_t u16CoolingPressure = 0;
	// Retard pour impulsion de soudure à la fin du cycle (Unité: 0.1s)
	uint16_t u16PulseDelay = 0;
	// Temps d'impulsion de soudure à la fin du cycle (Unité: 0.1s)
	uint16_t u16PulseWidth = 0;
	// Temps de refroidissement (Unité: 0.1s)
	uint16_t u16CoolingTime = 0;
	// Temps de retard pour déclenchement de la soudure (hors mode distance) (Unité: 0.1s)
	uint16_t u16StartDelayTime = 0;
	// Position de démarrage des US en mode distance
	uint16_t u16StartPos = 0;
	// Position de la pièce à souder pour le mode distance
	uint16_t u16PartPos = 0;
	// Consigne de soudure en mode temps (Unité: 0.1s)
	uint16_t u16WeldingTime = 0;
	// Consigne de soudure en mode puissance
	uint16_t u16WeldingPower = 0;
	// Consigne de soudure en mode énergie
	uint16_t u16WeldingEnergy = 0;
	// Consigne de soudure en mode distance
	uint16_t u16WeldingDistance = 0;
	// Consigne de soudure en mode enfoncement
	uint16_t u16WeldingDepth = 0;
	// Fenêtre de temps (false: inactive, true: active)
	bool xTimeLimit = false;
	// Fenêtre de puissance (false: inactive, true: active)
	bool xPowerLimit = false;
	// Fenêtre d'énergie (false: inactive, true: active)
	bool xEnergyLimit = false;
	// Fenêtre de distance (false: inactive, true: active)
	bool xDepthLimit = false;
	// Présence pièce 1 active
	bool xPartPresence1 = false;
	// Présence pièce 2 active
	bool xPartPresence2 = false;
	// Limite mini de la fenêtre de temps (Unité: 0.1s)
	uint16_t u16TimeMinLimit = 0;
	// Limite maxi de la fenêtre de temps (Unité: 0.1s)
	uint16_t u16TimeMaxLimit = 0;
	// Limite mini de la fenêtre de puissance
	uint16_t u16PowerMinLimit = 0;
	// Limite maxi de la fenêtre de puissance
	uint16_t u16PowerMaxLimit = 0;
	// Limite mini de la fenêtre d'énergie
	uint16_t u16EnergyMinLimit = 0;
	// Limite maxi de la fenêtre d'énergie
	uint16_t u16EnergyMaxLimit = 0;
	// Limite mini de la fenêtre de distance
	int16_t i16DepthMinLimit = 0;
	// Limite maxi de la fenêtre de distance
	int16_t i16DepthMaxLimit = 0;
} RCP_DATA;  // 67 octets

/*
 * Structure actionneur
 */
typedef struct __attribute__((packed)) {
	// Présence capteurs repos
	bool xPrSens0 = false;
	// Présence capteurs travail
	bool xPrSens1 = false;
	// Non présence capteurs repos
	bool xNpSens0 = false;
	// Non présence capteurs travail
	bool xNpSens1 = false;
	// Commande repos en mode manuel
	bool xCmdOp0 = false;
	// Commande travail en mode manuel
	bool xCmdOp1 = false;
	// Commande repos en mode automatique
	bool xCmd0 = false;
	// Commande travail en mode automatique
	bool xCmd1 = false;
	// Etat repos
	bool xState0 = false;
	// Etat travail
	bool xState1 = false;
	// Mémoire état repos
	bool xM_State0 = false;
	// Mémoire état travail
	bool xM_State1 = false;
	// Sécurité mouvement repos
	bool xSM0 = false;
	// Sécurité mouvement travail
	bool xSM1 = false;
	// Défaut: capteur repos non attendu
	bool xHomeSensorUnexpected = false;
	// Défaut: capteur repos perdu
	bool xHomeSensorLost = false;
	// Défaut: capteur repos tardif
	bool xHomeSensorLate = false;
	// Défaut: capteur travail non attendu
	bool xWorkSensorUnexpected = false;
	// Défaut: capteur travail perdu
	bool xWorkSensorLost = false;
	// Défaut: capteur travail tardif
	bool xWorkSensorLate = false;
	// Failure synthesis
	bool xFailure = false;
} T_ACTUATOR;  // 21 octets

typedef struct __attribute__((packed)) {
	// Commande marche en mode automatique
	bool xCmd1 = false;
	// Commande marche en mode manuel
	bool xCmdOp1 = false;
	// Temps de soudure effectif (ms)
	uint32_t u32RunningTime = 0;
	// Nombre d'échantillons de puissance mesurés pour le calcul de l'énergie
	uint32_t u32PowerSamples = 1;
	// Puissance actuelle en sortie du générateur
	double dActualPower = 0;
	// Puissance moyenne pendant la soudure
	double dAveragePower = 0;
	// Puissance max. fournie pendant la soudure
	double dMaxPower = 0;
	// Energie transmise pendant la soudure
	double dAccumulatedEnergy = 0;
} T_US_GENERATOR;  // 42 octets

typedef struct __attribute__((packed)) {
	// Numéro d'étape courante du cycle
	int16_t i16Step = 0;
	// Numéro d'étape précédente du cycle
	int16_t i16PrevStep = 0;
	// Temps passé à l'étape courante (ms)
	uint16_t u16StepTime = 0;
	// Temps passé à l'étape précédente (ms)
	uint16_t u16PrevStepTime = 0;
} GVL_CYCLE;  // 4 octets

typedef struct __attribute__((packed)) {
	// Recette utilisée pour le cycle en cours
	RCP_DATA *pRecipe;
	// Consigne de position de démarrage de la soudure (en impulsions codeur)
	uint32_t u32WeldStartPos = 0;
	// Consigne de position d'arrêt de la soudure (en impulsions codeur)
	uint32_t u32WeldStopPos = 0;
	// Temps de cycle (0.1s)
	uint32_t u32CycleTime = 0;
	// Profondeur atteinte (en 0.01 mm)
	uint32_t u32WeldDepth = 0;
	// 
	uint16_t u16Faults = 0;
	//
	uint16_t u16Alarms = 0;
	// Consigne de pression de la presse (0.1 bar)
	uint16_t u16PressPressure = 0;
	// Retour de pression de la presse (0.1 bar)
	uint16_t u16PressPressureMeas = 0;
	// Compteur pièces bonnes
	uint32_t u32PartsOk = 0;
	// Compteur pièces mauvaises
	uint32_t u32PartsNok = 0;
	// Compteur de cycles machine
	uint32_t u32Cycles = 0;
	// Bit de démarrage du cycle de soudure (=1 pendant le cycle de soudure)
	bool xWeldCycleStart = false;
	// Drapeau mode manuel actif
	bool xManualMode = false;
	// Bit de commande du refroidissement en mode auto
	bool xCoolingCmd = false;
} GVL_PROCESS;  // 43 octets

typedef struct __attribute__((packed)) {
	// Code du défaut
	uint16_t u16DefCode = 0;
	// Code de l'alarme
	uint16_t u16AlmCode = 0;
	// Code du message
	uint16_t ui16MsgCode = 0;
	// Bouton d'acquitement des défauts
	bool xAckBtn = false;
	// Bouton de remise à zéro des compteurs
	bool xCntRstBtn = false;
	// Bouton d'enregistrement de la recette
	bool xRcpSaveBtn = false;
	// Bouton de chargement de la recette
	bool xRcpLoadBtn = false;
	// Bouton de sélection de la recette
	bool xRcpSelectBtn = false;
	// Bouton d'enregistrement des paramètres machine
	bool xParamSaveBtn = false;
	// Bouton de chargement des paramètres machine
	bool xParamLoadBtn = false;
	// Bouton de restauration flash usine (paramètres, recettes)
	bool xParamRestoreBtn = false;
	// Id de la recette sélectionnée sur l'IHM
	uint8_t u8RecipeSelected;
	// Valeurs de la recette affichée sur l'IHM
	RCP_DATA stRecipe;
	// Valeurs des paramètres machine affichés sur l'IHM
	GVL_PARAM stParam;
	// Bouton de commande manuelle de la sortie 1
	bool xOut1Cmd = false;
	// Bouton de commande manuelle de la sortie 2
	bool xOut2Cmd = false;
	// Bouton de commande manuelle de la sortie 3
	bool xOut3Cmd = false;
	// Bouton de commande manuelle de la sortie 4
	bool xOut4Cmd = false;
	// Bouton de commande manuelle de la sortie 5
	bool xOut5Cmd = false;
	// Bouton de commande manuelle de la sortie 6
	bool xOut6Cmd = false;
	// Bouton de commande manuelle de la sortie 7
	bool xOut7Cmd = false;
	// Bouton de commande manuelle de la sortie 8
	bool xOut8Cmd = false;
	// Valeur de la sortie analogique 1 en mode manuel
	uint16_t u16Aout1Value = 0;
	// Valeur de la sortie analogique 1 en mode manuel
	uint16_t u16Aout2Value = 0;
	// Numéro d'écran actuelle
	uint16_t u16CurScreen = 0;
} GVL_HMI;  // 115 octets

typedef struct __attribute__((packed)) {
	GVL_PARAM       param;
	GVL_IO          io;
	GVL_CYCLE       runningSFC;
	GVL_CYCLE       mainSFC;
	GVL_CYCLE       weldSFC;
	GVL_PROCESS     process;
	GVL_HMI         hmi;
	T_ACTUATOR      stPress;
	T_US_GENERATOR  stUsGenerator;
	uint8_t         u8CurentRecipe = 0;
	RCP_DATA        arRecipes[10];
} GVL_VARS;

#endif // end of EUROPRESS_GVL_H
