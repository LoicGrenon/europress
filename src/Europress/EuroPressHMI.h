#ifndef EUROPRESS_HMI_H
#define EUROPRESS_HMI_H

#include <Arduino.h>
#include <DmaSerial.h>
#include <ModbusMasterDMA.h>  // http://4-20ma.io/ModbusMaster/index.html

#include "GVL.h"
#include "europress_flash.h"
#include "europress_utils.h"
#include "ModbusAddr.h"


class EuroPressHMI
{
    public:
        EuroPressHMI();

        ModbusMasterDMA modbus;

        void begin(uint8_t, DmaSerial &serial);
        void getRegistersValues(GVL_VARS * stVars);
        void idle(void (*)());
        bool isAvailable();
        void readWrite(GVL_VARS * stVars);

    private:
        uint8_t _u8RdChunkOffset = 0;
        uint8_t _u8RdChunkOffsetPrev = 0;
        // Data size to read with readRegisters()
        // Be careful: Do not exceed 64 bytes since it's the max buffer length in ModbusMaster library (ku8MaxBufferSize)
        static const uint8_t _ku8RdChunkSize = 64;
        uint8_t _u8RdResult = 0xFF;
        uint8_t _u8WrRecipeResult = 0xFF;
        bool _xRegistersReadDone = false;

        void _clearButtonsState(GVL_VARS * stVars);
        void _getParamValues(uint8_t u8AddressOffset, uint8_t u8BufferIdx, GVL_PARAM * pParams);
        void _getRecipeValues(uint8_t u8AddressOffset, uint8_t u8BufferIdx, RCP_DATA * stRcpData);
        void _getButtonsState(uint8_t u8ModbusAddress, uint8_t u8BufferIdx, GVL_VARS * stVars);
		void _getOutCmdButtonsState(uint8_t u8BufferIdx, GVL_VARS * stVars);
        void _readRegisters();
        void _writeCounters(GVL_PROCESS * pProcess);
        void _writeCurrentRcpProd(GVL_VARS * vars);
        void _writeDefAlmMsg(GVL_VARS * vars);
        void _writeHomePageValues(GVL_VARS * vars);
        void _writeIOStatus(GVL_VARS * vars);
        void _writeOutCmdStatus(GVL_VARS * vars);
        void _writeParamRegisters(GVL_PARAM * pParams);
        void _writeRecipeRegisters(RCP_DATA * stRcpData);
		void _writeSfcStatus(GVL_VARS * vars);
        void _writeWeldCycleValues(GVL_VARS * vars);

};

#endif // End of EUROPRESS_HMI_H
