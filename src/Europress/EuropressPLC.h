#ifndef EUROPRESS_PLC_H
#define EUROPRESS_PLC_H

#include <Arduino.h>
#include "GVL.h"
#include "europress_io.h"
#include "europress_utils.h"
#include "Screens.h"
#include "TON_Timer.h"
#include "TP_Timer.h"

// Unit: ms
#define PLC_CYCLE_TIME		1  

#define RUNNING_STEP_INIT	0
#define RUNNING_STEP_ESTOP	10
#define RUNNING_STEP_READY	20
#define RUNNING_STEP_MANUAL	30

#define MAIN_STEP_INIT		0

#define WELD_STEP_INIT		0


struct __attribute__((packed)) PosRd {
	uint32_t u32WeldStart;
	uint32_t u32WeldStop;
	uint32_t u32CoolEnd;
};


class EuroPressPLC
{
public:
	EuroPressPLC(GVL_VARS * pVars);

	void run(void);
	void runningSFC(void);
	void mainSFC(void);
	void weldingSFC(void);
	void alarmsMgmt(void);
	void faultsMgmt(void);
	void outputsMgmt(void);

private:
	bool _checkPartPresence();
	bool _checkQuality();
	void _computePowerEnergy();
	void _incMainSFCstepTime(void);
	void _incRunningSFCstepTime(void);
	void _incWeldSFCstepTime(void);
	void _inputMgmt();
	void _inputMgmt_Press();
	bool _isWeldSetpointReached();
	void _setSFCStep(GVL_CYCLE * pSFC ,int16_t i16Step);
	void _setMainSFCstep(int16_t i16Step);
	void _setRunningSFCstep(int16_t i16Step);
	void _setWeldSFCstep(int16_t i16Step);
	
	
	
	GVL_VARS *_pVars;
	// Start cycle button memory for rising edge detection
	bool xM_StartCycle;
	bool _bGotoNextStep;
	uint8_t _u8StartGenerator_RE;
	uint8_t _u8PressAtUpPosState;
	uint8_t _u8PressAtDownPosState;
	uint32_t _32EncoderResetPos;
	PosRd _stPositionAt;
	TON_Timer _tonPressHomeSensorLate;
	TON_Timer _tonPressWorkSensorLate;
	TP_Timer _tpClearGeneratorError;
};
#endif // end of EUROPRESS_PLC_H
