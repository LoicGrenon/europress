
#ifndef _EUROPRESS_UTILS_H
#define _EUROPRESS_UTILS_H

/*
 * Debug macros
 */
//#define DEBUG
#ifdef DEBUG
#define DPRINT(...)    Serial.print(__VA_ARGS__)     // debug print
#define DPRINTLN(...)  Serial.println(__VA_ARGS__)   // debug print with new line
#else
#define DPRINT(...)
#define DPRINTLN(...)
#endif


/*
 * Macro for detecting rising and falling edges
 * signal must be either 0x0 or 0x1 (digitalRead() output)
 * state must be uint8_t type
 */
// Macro for rising edge detection
#define RE(signal, state) (state=(state<<1)|(signal&1)&3)==1
// Macro for falling edge detection
#define FE(signal, state) (state=(state<<1)|(signal&1)&3)==2


static inline void set_bit(uint16_t *x, int bitNum) {
    *x |= (1U << bitNum);
}
static inline void set_bit(uint32_t *x, int bitNum) {
    *x |= (1UL << bitNum);
}

static inline void clear_bit(uint16_t *x, int bitNum) {
    *x &= ~(1U << bitNum);
}
static inline void clear_bit(uint32_t *x, int bitNum) {
    *x &= ~(1UL << bitNum);
}

static inline void toggle_bit(uint16_t *x, int bitNum) {
    *x ^= (1U << bitNum);
}
static inline void toggle_bit(uint32_t *x, int bitNum) {
    *x ^= (1UL << bitNum);
}

#endif // end of _EUROPRESS_UTILS_H
