#include "TON_Timer.h"
#include "europress_utils.h"

/*
	// TON example
	TON_Timer stTON;
	stTON.timer(vars->io.I_xStartCycle, 1000);
	vars->io.O_xGeneratorRun = stTON.xOut;
*/

void TON_Timer::timer(bool xIn, uint32_t u32Time) {
	if(xIn && !this->_xInOld) { // On rising edge
		this->_u32StartT = millis();
		this->_xInOld = true;
	} else if (!xIn) {
		this->_xInOld = false;
		this->xOut = false;
	}

	if(this->_xInOld && xIn) {
		this->u32ElapsedTime = millis() - this->_u32StartT;
		if (this->u32ElapsedTime >= u32Time) {
			this->u32ElapsedTime = u32Time;
			this->xOut = true;
		}
	}
}
