#ifndef TP_TIMER_H
#define TP_TIMER_H

#include <Arduino.h>

class TP_Timer {
	public:
		uint32_t  u32ElapsedTime=0;
		bool      xOut=false;

		void timer(bool xIn, uint32_t u32Time);

	private:
		bool      _xInOld=false;
		uint32_t  _u32StartT=0;
};

#endif // end of TP_TIMER_H
