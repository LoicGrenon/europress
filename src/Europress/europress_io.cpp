#include "europress_io.h"


void makePPIn() {
	pinModeFast(PP_IN0, INPUT);
	pinModeFast(PP_IN1, INPUT);
	pinModeFast(PP_IN2, INPUT);
	pinModeFast(PP_IN3, INPUT);
	pinModeFast(PP_IN4, INPUT);
	pinModeFast(PP_IN5, INPUT);
	pinModeFast(PP_IN6, INPUT);
	pinModeFast(PP_IN7, INPUT);
	pinModeFast(PP_IN8, INPUT);
	pinModeFast(PP_IN9, INPUT);
	pinModeFast(PP_IN10, INPUT);
	pinModeFast(PP_IN11, INPUT);
	pinModeFast(PP_IN12, INPUT);
	pinModeFast(PP_IN13, INPUT);
	pinModeFast(PP_IN14, INPUT);
	pinModeFast(PP_IN15, INPUT);
}

void makePPOut() {
	pinModeFast(PP_OUT0, OUTPUT);
	pinModeFast(PP_OUT1, OUTPUT);
	pinModeFast(PP_OUT2, OUTPUT);
	pinModeFast(PP_OUT3, OUTPUT);
	pinModeFast(PP_OUT4, OUTPUT);
	pinModeFast(PP_OUT5, OUTPUT);
	pinModeFast(PP_OUT6, OUTPUT);
	pinModeFast(PP_OUT7, OUTPUT);
}

void setupInputs() {
	pinMode(IN_ADC_BUSY, INPUT);
}

void setupOutputs() {
	pinMode(LATCH_DIG_OUT_1, OUTPUT);
	digitalWriteFast(LATCH_DIG_OUT_1, LOW);

	pinMode(LATCH_DIG_OUT_2, OUTPUT);
	digitalWriteFast(LATCH_DIG_OUT_2, LOW);

	pinMode(CS_DIG_IN_1, OUTPUT);
	digitalWriteFast(CS_DIG_IN_1, HIGH);

	pinMode(CS_DIG_IN_2, OUTPUT);
	digitalWriteFast(CS_DIG_IN_2, HIGH);

	pinMode(CS_FLASH, OUTPUT);
	digitalWriteFast(CS_FLASH, HIGH);
	
	pinMode(CS_ENCODER, OUTPUT);
	digitalWriteFast(CS_ENCODER, HIGH);

	pinMode(ADC_RD, OUTPUT);
	digitalWriteFast(ADC_RD, HIGH);

	pinMode(OUT_ADC_CONVST, OUTPUT);
	digitalWriteFast(OUT_ADC_CONVST, HIGH);	
}

void readInputs(GVL_VARS * vars) {
	makePPIn();
#ifdef DIG_INPUT_BOARD_MAX31911
	_readDigitalInputs_MAX31911(vars);
#else
	_readDigitalInputs(vars);
#endif
	_readAnalogInputs(vars);
}

void writeOutputs(GVL_VARS * vars) {
	makePPOut();
	_writeDigitalOutputs(vars);
	_writeAnalogOutputs(vars);
}

void _readDigitalInputs(GVL_VARS * vars) {
	digitalWriteFast(CS_DIG_IN_1, LOW);
	delayMicroseconds(1);  // 250ns max are needed to enable the buffer outputs
	                       // SN74HC540 datasheet pg 4/19 : Ten = 250ns max

	vars->io.I_xEnergized = digitalReadFast(PP_IN0) == HIGH;
	vars->io.I_xEstop = digitalReadFast(PP_IN1) == HIGH;
	vars->io.I_xStartCycle = digitalReadFast(PP_IN2) == HIGH;
	vars->io.I_xGeneratorRun = digitalReadFast(PP_IN3) == HIGH;
	vars->io.I_xGeneratorError = digitalReadFast(PP_IN4) == HIGH;
	vars->io.I_xPressAtUpPos = digitalReadFast(PP_IN5) == HIGH;
	vars->io.I_xPressAtDownPos = digitalReadFast(PP_IN6) == HIGH;
	vars->io.I_xPartPresence2 = digitalReadFast(PP_IN7) == HIGH;
	vars->io.I_xPartPresence1 = digitalReadFast(PP_IN8) == HIGH;
	vars->io.I_u8ToolCode = digitalReadFast(PP_IN9)       | digitalReadFast(PP_IN10) << 1
	                      | digitalReadFast(PP_IN11) << 2 | digitalReadFast(PP_IN12) << 3;
	vars->io.I_xInput14 = digitalReadFast(PP_IN13) == HIGH;
	vars->io.I_xInput15 = digitalReadFast(PP_IN14) == HIGH;
	vars->io.I_xInput16 = digitalReadFast(PP_IN15) == HIGH;

	digitalWriteFast(CS_DIG_IN_1, HIGH);
	delayMicroseconds(1);  // 250ns max are needed to disable the buffer outputs
	                       // SN74HC540 datasheet pg 4/19 : Tdis = 250ns max
}

/*
 *
 */
void _readDigitalInputs_MAX31911(GVL_VARS * vars) {
	uint16_t ui16inputs = 0;

	delayMicroseconds(1); 
	ui16inputs = SPI.transfer16(0x0000);
	digitalWriteFast(CS_DIG_IN_1, HIGH);

	vars->io.I_xEnergized = ui16inputs & 0x100 ? true : false;
	vars->io.I_xEstop = ui16inputs & 0x200 ? true : false;
	vars->io.I_xStartCycle = ui16inputs & 0x400 ? true : false;
	vars->io.I_xGeneratorRun = ui16inputs & 0x800 ? true : false;
	vars->io.I_xGeneratorError = ui16inputs & 0x1000 ? true : false;
	vars->io.I_xPressAtUpPos = ui16inputs & 0x2000 ? true : false;
	vars->io.I_xPressAtDownPos = ui16inputs & 0x4000 ? true : false;
	vars->io.I_xPartPresence2 = ui16inputs & 0x8000 ? true : false;
	vars->io.I_xPartPresence1 = ui16inputs & 0x01 ? true : false;
	vars->io.I_u8ToolCode = (ui16inputs & 0x02 ? true : false)      | (ui16inputs & 0x04 ? true : false) << 1
	                      | (ui16inputs & 0x08 ? true : false) << 2 | (ui16inputs & 0x10 ? true : false) << 3;
	vars->io.I_xInput14 = ui16inputs & 0x20 ? true : false;
	vars->io.I_xInput15 = ui16inputs & 0x40 ? true : false;
	vars->io.I_xInput16 = ui16inputs & 0x80 ? true : false;
}

void _readAnalogInputs(GVL_VARS * vars) {
		_readAD7610(vars);

		analogReadResolution(12);	// TODO: do this at setup phase ?
		vars->io.I_u16PressPressure = analogRead(IN_PRESS_PRESSURE_PIN);
}

/*
 * AD7610 is a 250SPS, 16 bits CAN used to read ultrasonic generator power
 * This function is used to initiate a conversion and read back the result
 */
void _readAD7610(GVL_VARS * vars) {
	uint8_t u8AdcConvertTimeout = 0;
	uint16_t ui16AdcResult = 0;

	/* Initiate conversion */
	digitalWriteFast(OUT_ADC_CONVST, HIGH);  // To make sure a falling edge were triggered on ADC input
	NOP;
	digitalWriteFast(OUT_ADC_CONVST, LOW);
	// CNVST low to Busy high delay is 35ns max (AD7610 datasheet pg 5 : t3)
	// So we need to wait for at least 3 CPU cycles
	NOP;NOP;NOP;NOP;

	// CAN conversion time : 1.45µs (AD7610 datasheet pg 5 : t7)
	// Due CPU frequency : 84Mhz = 11.9ns => 1450/11.9 = 121.8
	// So we need to wait for at least 122 cycles
	while((digitalReadFast(IN_ADC_BUSY) == HIGH) && (u8AdcConvertTimeout < 150)){
		u8AdcConvertTimeout++;
	}

	/* Read results */
	digitalWriteFast(ADC_RD, LOW);
	// CAN data valid on parallel port delay is 40ns max (AD7610 datasheet pg 5 and 23 : t12)
	// Due CPU frequency : 84Mhz = 11.9ns => 40 / 11.9 = 3.36
	// So we need to wait for at least 4 CPU cycles.
	NOP;NOP;NOP;NOP;NOP;

	ui16AdcResult = digitalReadFast(PP_IN0)        | digitalReadFast(PP_IN1) << 1
	              | digitalReadFast(PP_IN2) << 2   | digitalReadFast(PP_IN3) << 3
	              | digitalReadFast(PP_IN4) << 4   | digitalReadFast(PP_IN5) << 5
	              | digitalReadFast(PP_IN6) << 6   | digitalReadFast(PP_IN7) << 7
	              | digitalReadFast(PP_IN8) << 8   | digitalReadFast(PP_IN9) << 9
	              | digitalReadFast(PP_IN10) << 10 | digitalReadFast(PP_IN11) << 11
	              | digitalReadFast(PP_IN12) << 12 | digitalReadFast(PP_IN13) << 13
	              | digitalReadFast(PP_IN14) << 14 | digitalReadFast(PP_IN15) << 15;
	vars->io.I_u16GeneratorPower = ui16AdcResult;

	digitalWriteFast(ADC_RD, HIGH);
	digitalWriteFast(OUT_ADC_CONVST, HIGH);
}

/*
 * Write digital outputs on SN74LVC573A
 *
 * SN74LVC573A is a octal transparent D-type latches with 3-state outputs.
 * It use the shared parallel port (PP_BIT0-15) and depending the latch enable state (LATCH_DIG_OUT),
 * the digital outputs follow the parallel port state (LATCH_DIG_OUT = high)
 * or the state latched (memorized) on the SN74LVC573A (LATCH_DIG_OUT = low)
 */
void _writeDigitalOutputs(GVL_VARS * vars) {
	// WARNING: Outputs logic is reversed !

	// Set latch-enable high : the Q outputs follow the data D inputs
	digitalWriteFast(LATCH_DIG_OUT_1, HIGH);
	delayMicroseconds(1);  // 6.9ns max are needed to switch from D input to Q output
	                       // SN74LVC573A datasheet pg 7/36

	digitalWriteFast(PP_OUT0, vars->io.O_xGeneratorRun ? LOW : HIGH);
	digitalWriteFast(PP_OUT1, vars->io.O_xPressDown ? LOW : HIGH);
	digitalWriteFast(PP_OUT2, vars->io.O_xCooling ? LOW : HIGH);
	digitalWriteFast(PP_OUT3, vars->io.O_xOutput4 ? LOW : HIGH);
	digitalWriteFast(PP_OUT4, vars->io.O_xOutput5 ? LOW : HIGH);
	digitalWriteFast(PP_OUT5, vars->io.O_xOutput6 ? LOW : HIGH);
	digitalWriteFast(PP_OUT6, vars->io.O_xOutput7 ? LOW : HIGH);
	digitalWriteFast(PP_OUT7, vars->io.O_xOutput8 ? LOW : HIGH);
	
	// Set latch-enable low : the Q outputs are latched at the last the data D inputs logic level
	digitalWriteFast(LATCH_DIG_OUT_1, LOW);
	delayMicroseconds(1);  // 7.7ns max are needed to switch from LE input to Q output
	                       // SN74LVC573A datasheet pg 7/36

	// Set latch-enable high : the Q outputs follow the data D inputs
	digitalWriteFast(LATCH_DIG_OUT_2, HIGH);
	delayMicroseconds(1);  // 6.9ns max are needed to switch from D input to Q output
	                       // SN74LVC573A datasheet pg 7/36

	digitalWriteFast(PP_OUT0, HIGH);
	digitalWriteFast(PP_OUT1, HIGH);
	digitalWriteFast(PP_OUT2, HIGH);
	digitalWriteFast(PP_OUT3, HIGH);
	digitalWriteFast(PP_OUT4, HIGH);
	digitalWriteFast(PP_OUT5, HIGH);
	digitalWriteFast(PP_OUT6, HIGH);
	digitalWriteFast(PP_OUT7, HIGH);
	
	// Set latch-enable low : the Q outputs are latched at the last the data D inputs logic level
	digitalWriteFast(LATCH_DIG_OUT_2, LOW);
	delayMicroseconds(1);  // 7.7ns max are needed to switch from LE input to Q output
	                       // SN74LVC573A datasheet pg 7/36
}

void _writeAnalogOutputs(GVL_VARS * vars) {
	// vars->param.u16GeneratorAmplitudeMin_Points is defined in range [0; 65535] for 0 - 10V
	// ANA_OUT is 8 bits so it's defined in range [0; 255] for 0 - 10V => So we need to remap parameters range from [0; 65535] to [0; 255]
	analogWrite(OUT_GENERATOR_AMPLITUDE_PIN, map(vars->io.O_u16GeneratorAmplitude,
	                                             vars->param.u16GeneratorAmplitudeMin_Percent,
												 vars->param.u16GeneratorAmplitudeMax_Percent,
												 map(vars->param.u16GeneratorAmplitudeMin_Points, 0, 65535, 0, 255),
												 map(vars->param.u16GeneratorAmplitudeMax_Points, 0, 65535, 0, 255)));
	analogWrite(OUT_PRESS_PRESSURE_PIN, map(vars->io.O_u16PressPressure,
	                                        vars->param.u16PressRegulatorOutMin_Bar,
											vars->param.u16PressRegulatorOutMax_Bar,
											map(vars->param.u16PressRegulatorOutMin_Points, 0, 65535, 0, 255),
											map(vars->param.u16PressRegulatorOutMax_Points, 0, 65535, 0, 255)));
}
