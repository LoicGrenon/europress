#include "EuroPressPLC.h"
#include "europress_flash.h"
#include "io_encoder.h"
#include "TON_Timer.h"
#include "TOF_Timer.h"
#include "TP_Timer.h"


EuroPressPLC::EuroPressPLC(GVL_VARS * pVars)
{
	_pVars = pVars;
}

void EuroPressPLC::run(void) {
	readInputs(_pVars);
	
	_pVars->process.xManualMode = (_pVars->mainSFC.i16Step == 0 || _pVars->mainSFC.i16Step == 10)
	                             && _pVars->weldSFC.i16Step == 0
								 && _pVars->process.u16Faults == 0;
	if(_pVars->process.u16Faults != 0) {
		_pVars->mainSFC.i16Step = 0;
		_pVars->weldSFC.i16Step = 0;
	}
	_inputMgmt();
	runningSFC();
	mainSFC();
	weldingSFC();
	alarmsMgmt();
	faultsMgmt();
	outputsMgmt();

	if(_pVars->mainSFC.i16Step < 50) // Do not update generator related values during welding pulse
		_computePowerEnergy();

	writeOutputs(_pVars);
}

bool EuroPressPLC::_checkPartPresence() {
	// Check parts presence according to recipe values
	bool xPartPres1 = !_pVars->process.pRecipe->xPartPresence1 || _pVars->io.I_xPartPresence1;
	bool xPartPres2 = !_pVars->process.pRecipe->xPartPresence2 || _pVars->io.I_xPartPresence2;
	
	return xPartPres1 && xPartPres2;
}

bool EuroPressPLC::_checkQuality() {
	// Time limits
	// Generator running time unit : 0.001s
	// Time limits units : 0.1s
	if( _pVars->process.pRecipe->xTimeLimit && (_pVars->stUsGenerator.u32RunningTime < 100 * (uint32_t)_pVars->process.pRecipe->u16TimeMinLimit
	                                         || _pVars->stUsGenerator.u32RunningTime > 100 * (uint32_t)_pVars->process.pRecipe->u16TimeMaxLimit)) {
		set_bit(&(_pVars->process.u16Alarms), 0);
	}

	// Power limits
	if( _pVars->process.pRecipe->xPowerLimit && (_pVars->stUsGenerator.dMaxPower < (double)_pVars->process.pRecipe->u16PowerMinLimit
	                                          || _pVars->stUsGenerator.dMaxPower > (double)_pVars->process.pRecipe->u16PowerMaxLimit)) {
		set_bit(&(_pVars->process.u16Alarms), 1);
	}

	// Energy limits
	if( _pVars->process.pRecipe->xEnergyLimit && (_pVars->stUsGenerator.dAccumulatedEnergy < (double)_pVars->process.pRecipe->u16EnergyMinLimit
	                                           || _pVars->stUsGenerator.dAccumulatedEnergy > (double)_pVars->process.pRecipe->u16EnergyMaxLimit)) {
		set_bit(&(_pVars->process.u16Alarms), 2);
	}

	// Depth limits
	uint32_t u32Depth = (_stPositionAt.u32WeldStop - _stPositionAt.u32WeldStart) * _pVars->param.i16EncoderResolution / 10;
	if( _pVars->process.pRecipe->xDepthLimit && (u32Depth < (uint32_t)_pVars->process.pRecipe->i16DepthMinLimit
	                                          || u32Depth > (uint32_t)_pVars->process.pRecipe->i16DepthMaxLimit)) {
		set_bit(&(_pVars->process.u16Alarms), 3);
	}
}

void EuroPressPLC::_computePowerEnergy() {
	// Analog input resolution for generator power measurement = 16 bits (=65535)
	_pVars->stUsGenerator.dActualPower = (double)(_pVars->io.I_u16GeneratorPower) * (double)(_pVars->param.u16GeneratorMaxPower) / 65535.0;
	if(RE(_pVars->io.O_xGeneratorRun, _u8StartGenerator_RE)){
		_pVars->stUsGenerator.u32PowerSamples = 1;
		_pVars->stUsGenerator.u32RunningTime = 0;
		_pVars->stUsGenerator.dAveragePower = 0;
		_pVars->stUsGenerator.dMaxPower = 0;
	} else if(_pVars->io.O_xGeneratorRun) {
		_pVars->stUsGenerator.u32RunningTime += PLC_CYCLE_TIME;
		_pVars->stUsGenerator.u32PowerSamples++;
		if(_pVars->stUsGenerator.u32PowerSamples == 0){ _pVars->stUsGenerator.u32PowerSamples = 1; }  // Avoid division per zero
		// Compute running mean (moving average)
		_pVars->stUsGenerator.dAveragePower += (_pVars->stUsGenerator.dActualPower - _pVars->stUsGenerator.dAveragePower) / (double)(_pVars->stUsGenerator.u32PowerSamples);
		
		if(_pVars->stUsGenerator.dActualPower > _pVars->stUsGenerator.dMaxPower)
			_pVars->stUsGenerator.dMaxPower = _pVars->stUsGenerator.dActualPower;
	}
	_pVars->stUsGenerator.dAccumulatedEnergy = _pVars->stUsGenerator.dAveragePower * (double)(_pVars->stUsGenerator.u32RunningTime) / 1000;
}

void EuroPressPLC::_inputMgmt() {
	// Press cylinder related inputs management
	_inputMgmt_Press();
	// Analog input scaling : 0-10V = 0-4095 points (12 bits)
	_pVars->process.u16PressPressureMeas = map(_pVars->io.I_u16PressPressure,
	                                           _pVars->param.u16PressRegulatorInMin_Points,
											   _pVars->param.u16PressRegulatorInMax_Points,
											   _pVars->param.u16PressRegulatorInMin_Bar,
											   _pVars->param.u16PressRegulatorInMax_Bar);
	
}

bool EuroPressPLC::_isWeldSetpointReached() {
	if (_pVars->process.pRecipe->u8Mode == WELD_DISABLED)
		return true;
	else if (_pVars->process.pRecipe->u8Mode == WELD_MODE_TIME)
		return _pVars->stUsGenerator.u32RunningTime >= (uint32_t)_pVars->process.pRecipe->u16WeldingTime * 100;
	else if (_pVars->process.pRecipe->u8Mode == WELD_MODE_POWER)
		return _pVars->stUsGenerator.dActualPower >= (double)_pVars->process.pRecipe->u16WeldingPower;
	else if (_pVars->process.pRecipe->u8Mode == WELD_MODE_ENERGY)
		return _pVars->stUsGenerator.dAccumulatedEnergy >= (double)_pVars->process.pRecipe->u16WeldingEnergy;
	else if (_pVars->process.pRecipe->u8Mode == WELD_MODE_DISTANCE || _pVars->process.pRecipe->u8Mode == WELD_MODE_DEPTH)
		return REG_TC0_CV0 >= _pVars->process.u32WeldStopPos;
	return false;
}

void EuroPressPLC::_inputMgmt_Press() {
	// Vérin presse - Présence capteurs repos
	_pVars->stPress.xPrSens0 = _pVars->io.I_xPressAtUpPos;
	// Vérin presse - Présence capteurs travail
	_pVars->stPress.xPrSens1 = _pVars->io.I_xPressAtDownPos;
	// Vérin presse - Non présence capteurs repos
	_pVars->stPress.xNpSens0 = !_pVars->io.I_xPressAtUpPos;
	// Vérin presse - Non présence capteurs travail
	_pVars->stPress.xNpSens1 = !_pVars->io.I_xPressAtDownPos;
	// Vérin presse - Etat repos
	_pVars->stPress.xState0 = _pVars->stPress.xPrSens0 && _pVars->stPress.xNpSens1;
	// Vérin presse - Etat travail
	_pVars->stPress.xState1 = _pVars->stPress.xPrSens1 && _pVars->stPress.xNpSens0;
	// Vérin presse - Mémoire état repos
	_pVars->stPress.xM_State0 = _pVars->stPress.xState0
	                           || (_pVars->stPress.xM_State0 && !_pVars->stPress.xState1);
	// Vérin presse - Mémoire état travail
	_pVars->stPress.xM_State1 = _pVars->stPress.xState1
	                           || (_pVars->stPress.xM_State1 && !_pVars->stPress.xState0 && !_pVars->io.O_xPressDown);
	// Vérin presse - Sécurité mouvement repos
	_pVars->stPress.xSM0 = true;
	// Vérin presse - Sécurité mouvement travail
	_pVars->stPress.xSM1 = _pVars->io.I_xStartCycle || _pVars->io.I_xPressAtDownPos;
}

void EuroPressPLC::_setSFCStep(GVL_CYCLE * pSFC ,int16_t i16Step) {
	pSFC->i16PrevStep = pSFC->i16Step;
	pSFC->u16PrevStepTime = pSFC->u16StepTime;
	pSFC->i16Step = i16Step;
	pSFC->u16StepTime = 0;
}

/*
 * Set the current step number for the running mode SFC
 * and reset the step time counter
 */
void EuroPressPLC::_setRunningSFCstep(int16_t i16Step) {
	_setSFCStep(&_pVars->runningSFC, i16Step);
}

/*
 * Increments the current step time for the running mode SFC
 */
void EuroPressPLC::_incRunningSFCstepTime(void) {
	_pVars->runningSFC.u16StepTime += PLC_CYCLE_TIME;
}

/*
 * Set the current step number for the main SFC
 * and reset the step time counter
 */
void EuroPressPLC::_setMainSFCstep(int16_t i16Step) {
	_setSFCStep(&_pVars->mainSFC, i16Step);
}

/*
 * Increments the current step time (in ms) for the main SFC
 */
void EuroPressPLC::_incMainSFCstepTime(void) {
	_pVars->mainSFC.u16StepTime += PLC_CYCLE_TIME;
	if(_pVars->mainSFC.i16Step > 10) {
		_pVars->process.u32CycleTime += PLC_CYCLE_TIME;
	}
}

/*
 * Set the current step number for the welding SFC
 * and reset the step time counter
 */
void EuroPressPLC::_setWeldSFCstep(int16_t i16Step) {
	_setSFCStep(&_pVars->weldSFC, i16Step);
}

/*
 * Increments the current step time for the welding SFC
 */
void EuroPressPLC::_incWeldSFCstepTime(void) {
	_pVars->weldSFC.u16StepTime += PLC_CYCLE_TIME;
}

void EuroPressPLC::runningSFC(void) {
	// Force init step in case of emergency stop
	if (!_pVars->io.I_xEstop || !_pVars->io.I_xEnergized) {
		_setRunningSFCstep(RUNNING_STEP_INIT);
	}

	_incRunningSFCstepTime();

	switch(_pVars->runningSFC.i16Step) {
		case RUNNING_STEP_INIT:  // Initialization
			_setMainSFCstep(MAIN_STEP_INIT);
			_setWeldSFCstep(WELD_STEP_INIT);
			_setRunningSFCstep(RUNNING_STEP_ESTOP);
			// TODO: Initialiser les variables
			_pVars->process.xWeldCycleStart = false;
			_pVars->process.xCoolingCmd = false;
			break;

		case RUNNING_STEP_ESTOP:  // Initial step
			if (_pVars->io.I_xEstop && _pVars->io.I_xEnergized) {
				_setRunningSFCstep(RUNNING_STEP_READY);
			}
			break;

		case RUNNING_STEP_READY: // Ready
			if (_pVars->mainSFC.i16Step == 0 && _pVars->weldSFC.i16Step == 0 && false) {
				_setRunningSFCstep(RUNNING_STEP_MANUAL);
			}
			break;
			
		case RUNNING_STEP_MANUAL: // Manual mode
			if (false) {
				_setRunningSFCstep(RUNNING_STEP_READY);
			}

		default:  // Unsuitable case
			_setRunningSFCstep(RUNNING_STEP_INIT);
			break;
	}
}

void EuroPressPLC::mainSFC(void) {
	uint16_t u16StartPosSetpoint;

	_incMainSFCstepTime();
	switch(_pVars->mainSFC.i16Step) {
		case MAIN_STEP_INIT:
			_pVars->process.xWeldCycleStart = false;
			_pVars->stUsGenerator.xCmd1 = false;
			_pVars->stPress.xCmd0 = true;
			_pVars->stPress.xCmd1 = false;
			_pVars->process.u16PressPressure = 0;

			if (_pVars->runningSFC.i16Step == RUNNING_STEP_READY && _pVars->process.u16Faults == 0) {
				_setMainSFCstep(10);
			}
			break;

		case 10:  // Wait for cycle start
			if (_pVars->stPress.xState0 && _checkPartPresence()
			                            && _pVars->mainSFC.u16StepTime >= 500
										&& _pVars->process.u16Alarms == 0
										&& !xM_StartCycle && _pVars->io.I_xStartCycle) {
				resetQDEC0();  // Reset encoder position
				_32EncoderResetPos = REG_TC0_CV0;
				_pVars->process.pRecipe = &_pVars->arRecipes[_pVars->u8CurentRecipe];
				// Welding start position for distance welding mode
				u16StartPosSetpoint = _pVars->process.pRecipe->u16PartPos - _pVars->process.pRecipe->u16StartPos;
				_pVars->process.u32WeldStartPos = (uint32_t)u16StartPosSetpoint * 10 / (uint32_t)_pVars->param.i16EncoderResolution;
				_pVars->process.u16PressPressure = _pVars->process.pRecipe->u16DescentPressure;		
				_pVars->stPress.xCmd0 = false;
				_pVars->stPress.xCmd1 = true;
				_pVars->process.u32CycleTime = 0;
				DPRINTLN((String)"Recipe: " + _pVars->process.pRecipe->u8Id);
				DPRINTLN((String)"StartPos: " + _pVars->process.u32WeldStartPos);
				_setMainSFCstep(15);
			}
			break;

		case 15:  // Wait for encoder reset done
			// TODO: Ajouter timeout (en cas de problème avec le codeur, on reste bloqué à cette étape ...)
			// The encoder value is reset only on signal edge so we need to wait the press moves for the reset
			if(REG_TC0_CV0 < _32EncoderResetPos || _32EncoderResetPos < 10)
				_setMainSFCstep(_pVars->hmi.u16CurScreen == SCREEN_RECIPE_EDIT ? 100 : 20);
			break;

		case 20:  // Wait for press cylinder down position or welding cycle end
			// On distance welding mode, the US generator is started when a position is reached during the descent
			if (_pVars->process.pRecipe->u8Mode == WELD_MODE_DISTANCE && _pVars->io.I_xStartCycle
			                                                          && REG_TC0_CV0 >= _pVars->process.u32WeldStartPos) {
				_pVars->stUsGenerator.xCmd1 = true;
			}

			if (_pVars->stPress.xState1 && _pVars->io.I_xStartCycle) {
				_setMainSFCstep(30);
			}
			// The start cycle button must be held until the cylinder reaches it's down position
			else if (!_pVars->io.I_xStartCycle) {
				// Défaut bimanuelle relâchée
				set_bit(&(_pVars->process.u16Faults), 9);
			}
			break;

		case 30:  // Start welding cycle
			_pVars->process.xWeldCycleStart = true;
			_setMainSFCstep(40);
			break;
		
		case 40:  // Wait for welding cycle end
			if (!_pVars->process.xWeldCycleStart) {
				// Update counters
				if (_pVars->process.u16Alarms == 0)
					_pVars->process.u32PartsOk++;
				else
					_pVars->process.u32PartsNok++;
				_pVars->process.u32Cycles++;
				storeCounters(&(_pVars->process));

				_setMainSFCstep(50);
			}
			// Timeout
			else if(_pVars->mainSFC.u16StepTime > (uint16_t)_pVars->param.u8CycleTimeout * 1000) {
				set_bit(&(_pVars->process.u16Faults), 11);
			}
			break;

		case 50:
			_pVars->stPress.xCmd0 = true;
			_pVars->stPress.xCmd1 = false;

			// Welding pulse during press ascent
			_pVars->stUsGenerator.xCmd1 = _pVars->mainSFC.u16StepTime >= _pVars->process.pRecipe->u16PulseDelay * 100
			                            && _pVars->mainSFC.u16StepTime < (_pVars->process.pRecipe->u16PulseDelay + _pVars->process.pRecipe->u16PulseWidth) * 100;
			
			if (_pVars->stPress.xState0) {
				_pVars->stUsGenerator.xCmd1 = false;
				_setMainSFCstep(60);
			}
			break;

		case 60:  // Wait for parts removal by operator
			if((!_pVars->process.pRecipe->xPartPresence1 || !_pVars->io.I_xPartPresence1) && (!_pVars->process.pRecipe->xPartPresence2 || !_pVars->io.I_xPartPresence2)) {
				_setMainSFCstep(0);
			}
			break;

		case 100:  // Distance setup mode : Wait for press cylinder work position 
			if (_pVars->stPress.xState1 && _pVars->io.I_xStartCycle) {
				_setMainSFCstep(110);
			}
			// The start cycle button must be held until the cylinder reaches it's work position
			else if (!_pVars->io.I_xStartCycle) {
				// Défaut bimanuelle relâchée
				set_bit(&(_pVars->process.u16Faults), 9);
			}
			break;

		case 110:  // Distance setup mode : Wait for work position reached for 3s then store position value
			if (!_pVars->stPress.xState1) {
				_setMainSFCstep(110);
			}
			else if (_pVars->mainSFC.u16StepTime >= 3000) {
				// TODO: store position value
				_pVars->process.pRecipe->u16PartPos = (uint16_t) REG_TC0_CV0 * _pVars->param.i16EncoderResolution / 10;
				_pVars->hmi.xRcpLoadBtn = true;
				// Set pressure to the max as the pressure regulator can be mounted on the supply pipe of the solenoid valve
				_pVars->process.u16PressPressure = _pVars->param.u16PressRegulatorOutMax_Bar;
				_pVars->stPress.xCmd0 = true;
				_pVars->stPress.xCmd1 = false;
				_setMainSFCstep(120);
			}
			break;
		
		case 120:  // Distance setup mode : Wait for press cylinder home position 
			if (_pVars->stPress.xState0) {
				_setMainSFCstep(0);
			}
			break;
			
		case 500: // Error step
			if (_pVars->hmi.xAckBtn) {
				_setMainSFCstep(0);
			}
			break;
			
		default:  // Unsuitable case
			_setMainSFCstep(MAIN_STEP_INIT);
			break;
	}
	
	xM_StartCycle = _pVars->io.I_xStartCycle;
}

void EuroPressPLC::weldingSFC(void) {
	uint16_t u16StopPosSetpoint;

	_incWeldSFCstepTime();
	switch(_pVars->weldSFC.i16Step) {
		case WELD_STEP_INIT:
			if (_pVars->runningSFC.i16Step == RUNNING_STEP_READY && _pVars->process.xWeldCycleStart) {
				if(_pVars->process.pRecipe->u8Mode != WELD_DISABLED) {
					_pVars->process.u16PressPressure = _pVars->process.pRecipe->u16WeldingPressure;

					// Clear welding process values
					_pVars->stUsGenerator.u32RunningTime = 0;
					_pVars->stUsGenerator.dActualPower = 0;
					_pVars->stUsGenerator.dAveragePower = 0;
					_pVars->stUsGenerator.dMaxPower = 0;
					_pVars->stUsGenerator.u32PowerSamples = 1;
					_pVars->stUsGenerator.dAccumulatedEnergy = 0;

					_setWeldSFCstep(10);
				} else {
					_setWeldSFCstep(200);  // Fin de cycle
				}
			}
			break;

		case 10:  // Wait for start delay elapsed
			_bGotoNextStep = false;
			if (_pVars->process.pRecipe->u8Mode == WELD_MODE_DISTANCE) {
				u16StopPosSetpoint = _pVars->process.pRecipe->u16PartPos + _pVars->process.pRecipe->u16WeldingDistance;
				_pVars->process.u32WeldStopPos = (uint32_t)u16StopPosSetpoint * 10 / (uint32_t)_pVars->param.i16EncoderResolution;

				if (REG_TC0_CV0 >= _pVars->process.u32WeldStartPos) {
					_bGotoNextStep = true;
				}
			} else { // _pVars->process.pRecipe->u8Mode != WELD_MODE_DISTANCE
				if (_pVars->weldSFC.u16StepTime >= _pVars->process.pRecipe->u16StartDelayTime * 100) {
					_pVars->process.u32WeldStopPos = REG_TC0_CV0;
					_pVars->process.u32WeldStopPos += (uint32_t)_pVars->process.pRecipe->u16WeldingDepth * 10 / (uint32_t)_pVars->param.i16EncoderResolution;

					_bGotoNextStep = true;
				}
			}

			if (_bGotoNextStep) {
				// Memorize welding start position for welding depth computing
				_stPositionAt.u32WeldStart = REG_TC0_CV0;
				_pVars->stUsGenerator.xCmd1 = true;
				DPRINTLN((String)"StopPos: " + _pVars->process.u32WeldStopPos + " current: " + REG_TC0_CV0);
				_setWeldSFCstep(20);
			}
			break;

		case 20:  // Welding
			_pVars->process.u16PressPressure = _pVars->process.pRecipe->u16WeldingPressure;
			if (_isWeldSetpointReached()) {
				_pVars->stUsGenerator.xCmd1 = false;
				// Memorize welding stop position for welding depth computing
				_stPositionAt.u32WeldStop = REG_TC0_CV0;
				_setWeldSFCstep(30);
			}
			break;

		case 30:  // Cooling and welding quality check
			_pVars->process.u16PressPressure = _pVars->process.pRecipe->u16CoolingPressure;
			_pVars->process.xCoolingCmd = true;
			// Contrôle de la qualité de la soudure
			_checkQuality();
			if (_pVars->weldSFC.u16StepTime >= _pVars->process.pRecipe->u16CoolingTime * 100) {
				// Memorize position at end of cooling for welding depth computing
				_stPositionAt.u32CoolEnd = REG_TC0_CV0;
				// Effective welding depth value for displaying on HMI
				_pVars->process.u32WeldDepth = (_stPositionAt.u32CoolEnd - _stPositionAt.u32WeldStart) * _pVars->param.i16EncoderResolution / 10;
				_setWeldSFCstep(200);
			}
			break;

		case 200:  // End of welding cycle
			_pVars->process.xWeldCycleStart = false;
			_pVars->process.xCoolingCmd = false;
			_setWeldSFCstep(0);
			break;
			
		default:  // Unsuitable case
			_setWeldSFCstep(WELD_STEP_INIT);
			break;
	}
}

void EuroPressPLC::faultsMgmt() {
	// Défaut 1: "Arrêt d'urgence"
	if (!_pVars->io.I_xEstop)
		set_bit(&(_pVars->process.u16Faults), 0);

	// Défaut 2: "Machine non réarmée"
	if (_pVars->io.I_xEstop && !_pVars->io.I_xEnergized) {
		set_bit(&(_pVars->process.u16Faults), 1);
	} else {
		clear_bit(&(_pVars->process.u16Faults), 1);
	}

	// Défaut 3: "Incohérence de l'état des capteurs de la presse"
	if (_pVars->io.I_xPressAtUpPos && _pVars->io.I_xPressAtDownPos)
		set_bit(&(_pVars->process.u16Faults), 2);

	// Défaut 4: "Vérin presse: Capteur repos non attendu"
	if (_pVars->io.O_xPressDown && !_pVars->stPress.xM_State0 && RE(_pVars->io.I_xPressAtUpPos, _u8PressAtUpPosState))
		set_bit(&(_pVars->process.u16Faults), 3);

	// Défaut 5: "Vérin presse: Capteur repos perdu"
	if (!_pVars->io.O_xPressDown && _pVars->stPress.xM_State0 && FE(_pVars->io.I_xPressAtUpPos, _u8PressAtUpPosState))
		set_bit(&(_pVars->process.u16Faults), 4);

	// Défaut 6: "Vérin presse: Capteur repos tardif"
	_tonPressHomeSensorLate.timer(!_pVars->io.O_xPressDown && !_pVars->io.I_xPressAtUpPos, 10000);
	if (_tonPressHomeSensorLate.xOut)
		set_bit(&(_pVars->process.u16Faults), 5);

	// Défaut 7: "Vérin presse: Capteur travail non attendu"
	if (!_pVars->io.O_xPressDown && !_pVars->stPress.xM_State1 && RE(_pVars->io.I_xPressAtDownPos, _u8PressAtDownPosState))
		set_bit(&(_pVars->process.u16Faults), 6);

	// Défaut 8: "Vérin presse: Capteur travail perdu"
	if (_pVars->io.O_xPressDown && _pVars->stPress.xM_State0 && FE(_pVars->io.I_xPressAtDownPos, _u8PressAtDownPosState))
		set_bit(&(_pVars->process.u16Faults), 7);

	// Défaut 9: "Vérin presse: Capteur travail tardif"
	_tonPressWorkSensorLate.timer(_pVars->io.O_xPressDown && !_pVars->io.I_xPressAtDownPos, 10000);
	if (_tonPressWorkSensorLate.xOut)
		set_bit(&(_pVars->process.u16Faults), 8);
	
	// Défaut 10: "Commande bimanuelle relâchée"
	if (false)  // Utilisé à l'étape 20 de mainSFC
		set_bit(&(_pVars->process.u16Faults), 9);
	
	// Défaut 11: "Défaut générateur"
	if (_pVars->io.I_xGeneratorError)
		set_bit(&(_pVars->process.u16Faults), 10);

	// Défaut 12: "Défaut timeout cycle soudure"
	if (false)  // Utilisé à l'étape 40 de mainSFC
		set_bit(&(_pVars->process.u16Faults), 11);
	
	// Défaut 13: Réserve
	if (false)
		set_bit(&(_pVars->process.u16Faults), 12);
	// Défaut 14: Réserve
	if (false)
		set_bit(&(_pVars->process.u16Faults), 13);
	// Défaut 15: Réserve
	if (false)
		set_bit(&(_pVars->process.u16Faults), 14);
	// Défaut 16: Réserve
	if (false)
		set_bit(&(_pVars->process.u16Faults), 15);

	_pVars->hmi.u16DefCode = _pVars->process.u16Faults;
}

void EuroPressPLC::alarmsMgmt() {
	// Alarme 1: "Hors limite temps"
	if (false)  // Utilisé dans _checkQuality()
		set_bit(&(_pVars->process.u16Alarms), 0);
	else if(_pVars->hmi.xAckBtn)
		clear_bit(&(_pVars->process.u16Alarms), 0);

	// Alarme 2: "Hors limite puissance"
	if (false)  // Utilisé dans _checkQuality()
		set_bit(&(_pVars->process.u16Alarms), 1);
	else if(_pVars->hmi.xAckBtn)
		clear_bit(&(_pVars->process.u16Alarms), 1);

	// Alarme 3: "Hors limite énergie"
	if (false)  // Utilisé dans _checkQuality()
		set_bit(&(_pVars->process.u16Alarms), 2);
	else if(_pVars->hmi.xAckBtn)
		clear_bit(&(_pVars->process.u16Alarms), 2);

	// Alarme 4: "Hors limite profondeur"
	if (false)  // Utilisé dans _checkQuality()
		set_bit(&(_pVars->process.u16Alarms), 3);
	else if(_pVars->hmi.xAckBtn)
		clear_bit(&(_pVars->process.u16Alarms), 3);

	// Alarme 5: "Manque pièce 1"
	if (_pVars->mainSFC.i16Step == 10 && _pVars->process.pRecipe->xPartPresence1 && !_pVars->io.I_xPartPresence1)
		set_bit(&(_pVars->process.u16Alarms), 4);
	else
		clear_bit(&(_pVars->process.u16Alarms), 4);

	// Alarme 6: "Manque pièce 2"
	if (_pVars->mainSFC.i16Step == 10 && _pVars->process.pRecipe->xPartPresence2 && !_pVars->io.I_xPartPresence2)
		set_bit(&(_pVars->process.u16Alarms), 5);
	else
		clear_bit(&(_pVars->process.u16Alarms), 5);

	// Alarme 7: "Retirer les pièces"
	if (_pVars->mainSFC.i16Step == 60 && ((_pVars->process.pRecipe->xPartPresence1 && _pVars->io.I_xPartPresence1)
	                                   || (_pVars->process.pRecipe->xPartPresence2 && _pVars->io.I_xPartPresence2)))
		set_bit(&(_pVars->process.u16Alarms), 6);
	else
		clear_bit(&(_pVars->process.u16Alarms), 6);

	_pVars->hmi.u16AlmCode = _pVars->process.u16Alarms;
}

void EuroPressPLC::outputsMgmt() {
	_tpClearGeneratorError.timer(_pVars->io.I_xGeneratorError && _pVars->hmi.xAckBtn, 50);

	if (_pVars->process.xManualMode) {  // Outputs state in manual mode
		_pVars->io.O_xGeneratorRun = _pVars->hmi.xOut1Cmd;
		_pVars->io.O_xPressDown = _pVars->hmi.xOut2Cmd;
		_pVars->io.O_xCooling = _pVars->hmi.xOut3Cmd;
		_pVars->io.O_xOutput4 = _pVars->hmi.xOut4Cmd;
		_pVars->io.O_xOutput5 = _pVars->hmi.xOut5Cmd;
		_pVars->io.O_xOutput6 = _pVars->hmi.xOut6Cmd;
		_pVars->io.O_xOutput7 = _pVars->hmi.xOut7Cmd;
		_pVars->io.O_xOutput8 = _pVars->hmi.xOut8Cmd;

		_pVars->io.O_u16GeneratorAmplitude = map(_pVars->hmi.u16Aout1Value, 0, 65535, 50, 100);
		_pVars->io.O_u16PressPressure= _pVars->io.O_xPressDown ? map(_pVars->hmi.u16Aout2Value, 0, 65535, 0, 60) : 0;
	} else {  // Outputs state in automatic mode
		_pVars->io.O_xGeneratorRun = _pVars->stUsGenerator.xCmd1 || _tpClearGeneratorError.xOut;
		_pVars->io.O_xPressDown = _pVars->stPress.xSM1 && _pVars->stPress.xCmd1 && !_pVars->stPress.xCmd0;
		_pVars->io.O_xCooling = _pVars->process.xCoolingCmd;
		_pVars->io.O_xOutput4 = false;
		_pVars->io.O_xOutput5 = false;
		_pVars->io.O_xOutput6 = false;
		_pVars->io.O_xOutput7 = false;
		_pVars->io.O_xOutput8 = false;
		
		_pVars->io.O_u16PressPressure = _pVars->io.O_xPressDown ? _pVars->process.u16PressPressure : 0;
		_pVars->io.O_u16GeneratorAmplitude = (uint16_t)_pVars->process.pRecipe->u8Amplitude;
	}
}
