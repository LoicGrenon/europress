#include <Arduino.h>
#include <SPI.h>
#include "europress_io.h"

#ifndef _IO_ENCODER_H
#define _IO_ENCODER_H

/*
 * Attribution des broches : Entrées rapides
 * /!\ Les numéros de broches sont fixées par le module QDEC (TIOA0 et TIOB0)
 */
// Régle magnétique de mesure de position - Piste A
#define IN_ENCODER_A_PIN 2
// Régle magnétique de mesure de position - Piste B
#define IN_ENCODER_B_PIN 13



/*------------------------------------------------------------------------------
 *         Global functions
 *------------------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

void setupQDEC0(bool xSwap);

void resetQDEC0();

uint16_t setupMAX14890E(bool xSingleEnded);

uint16_t readMAX14890EConfig();

#ifdef __cplusplus
}
#endif

#endif // endif of _IO_ENCODER_H
